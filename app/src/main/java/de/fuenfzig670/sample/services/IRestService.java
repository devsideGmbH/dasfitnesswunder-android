package de.fuenfzig670.sample.services;

import org.json.JSONObject;

public interface IRestService {
    void getCall(String collectionQuery, final IStringCallback callback);

    void postCall(String collectionQuery, JSONObject jsonBody, final IStringCallback callback);

    void putCall(String collectionQuery, JSONObject jsonBody, final IStringCallback callback);

//    static RestService getInstance(Context context);


}
