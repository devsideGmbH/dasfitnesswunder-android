package de.fuenfzig670.sample.services;

public interface IHtmlCallback {
    void onSuccess(String response);
    void onError(String response);
}
