package de.fuenfzig670.sample.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import de.fuenfzig670.dasfitnesswunder.R;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class RestService /*implements IRestService*/ {
    private static RestService instance;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static Context ctx;

    private RestService(Context context) {

        ctx = context;
        //VolleyLog.DEBUG = true;
        requestQueue = Volley.newRequestQueue(context, new CustomHurlStack());// getRequestQueue();

        imageLoader = new ImageLoader(requestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized RestService getInstance(Context context) {
        if (instance == null) {
            instance = new RestService(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public void getCall(String collectionQuery, final IHtmlCallback callback) {
        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.GET, ctx.getResources().getString(R.string.BASEURL) + collectionQuery, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("onResponse", response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = handleError(error);
                        callback.onError(body);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + ctx.getResources().getString(R.string.RESTAPIKEY));
                return params;
            }
        };
        addToRequestQueue(jsonObjectRequest);
    }


    public void postCall(String collectionQuery, String jsonBody, final IHtmlCallback callback) {
        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.POST, ctx.getResources().getString(R.string.BASEURL) + collectionQuery, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("onResponse", response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = handleError(error);
                        callback.onError(body);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + ctx.getResources().getString(R.string.RESTAPIKEY));
                params.put("Content-Type", "application/json");

                return params;
            }

            @Override
            public byte[] getBody() {
                return jsonBody.getBytes();
            }

        };
        addToRequestQueue(jsonObjectRequest);
    }

    public Request putCall(String collectionQuery, String jsonBody, final IHtmlCallback callback) {
        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.PUT, ctx.getResources().getString(R.string.BASEURL) + collectionQuery, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("onResponse", response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = handleError(error);
                        callback.onError(body);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + ctx.getResources().getString(R.string.RESTAPIKEY));
                params.put("Content-Type", "application/json");

                return params;
            }

            @Override
            public byte[] getBody() {
                return jsonBody.getBytes();
            }
        };
        addToRequestQueue(jsonObjectRequest);
        return  jsonObjectRequest;
    }

    public void putCall(String collectionQuery, String jsonBody, final IObjectCallback callback) {
        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.PUT, ctx.getResources().getString(R.string.BASEURL) + collectionQuery, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("onResponse", response);
                        if (response.equals("")) {
                            callback.onResponse(true);
                        } else {
                            callback.onResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = handleError(error);
                        callback.onResponse(body);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + ctx.getResources().getString(R.string.RESTAPIKEY));
                params.put("Content-Type", "application/json");

                return params;
            }

            @Override
            public byte[] getBody() {
                return jsonBody.toString().getBytes();
            }

        };
        addToRequestQueue(jsonObjectRequest);
    }

    private String handleError(VolleyError error) {
        String body;
        if (error.networkResponse != null) {
            String statusCode = String.valueOf(error.networkResponse.statusCode);
            Log.e("onErrorResponse", error.toString() + "statuscode" + statusCode);
            if (error.networkResponse.data != null) {
                try {
                    body = new String(error.networkResponse.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    Log.e("onErrorResponse", error.toString());
                    body = error.toString();
                }
            } else {
                body = "no data in network response";
            }
        } else {
            body = error.toString();
        }
        return body;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public void deleteCall(String collectionQuery, String jsonBody, final IHtmlCallback callback) {
        StringRequest jsonObjectRequest = new StringRequest
                (Request.Method.DELETE, ctx.getResources().getString(R.string.BASEURL) + collectionQuery, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("onResponse", response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String body = handleError(error);
                        callback.onError(body);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + ctx.getResources().getString(R.string.RESTAPIKEY));
                params.put("Content-Type", "application/json");

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonBody == null ? null : jsonBody.toString().getBytes();
            }
            //            @Override
//            public byte[] getBody() {
//                return jsonBody == null ? null : jsonBody.toString().getBytes();
//            }
        };
        addToRequestQueue(jsonObjectRequest);

    }
}
