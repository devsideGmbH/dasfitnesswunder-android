package de.fuenfzig670.sample.services;

public interface IObjectCallback {
    void onResponse(Object response);
}
