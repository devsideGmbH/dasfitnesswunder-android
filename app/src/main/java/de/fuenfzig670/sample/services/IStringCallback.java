package de.fuenfzig670.sample.services;

public interface IStringCallback {
     void onResponse(String response);
}
