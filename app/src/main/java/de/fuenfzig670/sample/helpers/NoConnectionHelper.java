package de.fuenfzig670.sample.helpers;

import android.content.Context;
import android.widget.Toast;

public class NoConnectionHelper {

    public static void showNoConnectionToast(Context context) {
        Toast.makeText(context, "Es konnte keine Internetverbindung hergestellt werden. Stellen Sie sicher, dass Sie eine Internetverbindung haben und laden sie diese Seite neu.", Toast.LENGTH_LONG).show();
    }

    public static void showNoDataToast(Context context) {
        Toast.makeText(context, "Es konnten keine Daten vom Server gelesen werden. Stellen Sie sicher, dass Sie eine Internetverbindung haben und laden sie diese Seite neu.", Toast.LENGTH_LONG).show();
    }

    public static void showNoPasswordResetToast(Context context) {
        Toast.makeText(context, "Ihr Passwort konnte nicht zurückgesetzt werden. Stellen Sie sicher, dass Sie eine Internetverbindung haben und versuchen Sie es erneut.", Toast.LENGTH_LONG).show();
    }
}
