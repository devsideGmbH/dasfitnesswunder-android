package de.fuenfzig670.sample.helpers;

import android.content.Context;

import de.fuenfzig670.dasfitnesswunder.R;

public class HtmlHelper {
    public static String AddAndroidStylingHeader(String webStr) {

        String htmlStartWithStyle =
                "<html>" +
                        "<head>" +
                        "<style>" +
                        "body { margin: 1rem; }" +
                        "</style>" +
                        "</head>" +
                        "<body>";

        String htmlEnd =
                "</body>" +
                        "</html>";

        //return webStr.Replace("<head>", htmlStartWithStyle);
        return htmlStartWithStyle + webStr + htmlEnd;
    }

    public static String MakeHeader(String text) {
        return "<h1>" + text + "</h1>";
    }

    public static String GetHeaderImage(Context context) {
        return "<img src=" + context.getResources().getString(R.string.HeaderImagePath) + "style='width: 100%; height:auto;'>";
    }


}
