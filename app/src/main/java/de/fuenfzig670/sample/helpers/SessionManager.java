package de.fuenfzig670.sample.helpers;

import de.fuenfzig670.sample.model.Company;

public class SessionManager {
    private static SessionManager _instance;

    public String token;
    public Company company;

    protected SessionManager(){}

    public static synchronized SessionManager getInstance(){
        if(_instance == null){
            _instance = new SessionManager();
        }
        return _instance;
    }

    public boolean HasToken(){
        return (token != null);
    }
}
