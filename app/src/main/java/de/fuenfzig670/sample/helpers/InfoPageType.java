package de.fuenfzig670.sample.helpers;

public enum InfoPageType {
    IMPRESSUM(), DATENSCHUTZ, TEILNAHME, FAQ, NONE
}
