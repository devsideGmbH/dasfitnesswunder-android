package de.fuenfzig670.sample.helpers;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.thryve.connector.module_gfit.data.GFitDataType;
import com.thryve.connector.shealth.model.SHealthDataType;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.activities.AccountDeletionSuccessActivity;
import de.fuenfzig670.sample.activities.BaseActivity;
import de.fuenfzig670.sample.activities.LoginActivity;
import de.fuenfzig670.sample.activities.MainActivity;
import de.fuenfzig670.sample.activities.PinCodeActivity;
import de.fuenfzig670.sample.activities.RegistrySuccessActivity;
import de.fuenfzig670.sample.dataservice.GeneralService;
import de.fuenfzig670.sample.model.Challenge;
import de.fuenfzig670.sample.model.Company;
import de.fuenfzig670.sample.model.LoginRequestResponse;
import de.fuenfzig670.sample.model.UserModel;

public class GeneralHelper {

    public static final String SAMSUNG_HEALTH_PACKAGE = "com.sec.android.app.shealth";
    private static final String USER_MODEL_KEY = "USER_MODEL_KEY";
    private static final String PIN_CODE = "PIN_CODE";
    private static final String USER_LOGGED_OUT = "USER_LOGGED_OUT";
    private static final String ONBOARDING_SHOWN = "ONBOARDING_SHOWN";
    private static final String STAY_LOGGED_IN = "STAY_LOGGED_IN";
    private static final String SHARED_PREFS_NAME = "SHARED_PREFS_NAME";
    private static final String TAG = GeneralHelper.class.getName();

    public static final ArrayList<com.thryve.connector.shealth.model.SHealthDataType> S_HEALTH_DATA_TYPES = new ArrayList<SHealthDataType>() {
        {
            add(com.thryve.connector.shealth.model.SHealthDataType.STEP_DAILY_TREND);
        }
    };

    public static final ArrayList<GFitDataType> G_FIT_DATA_TYPES = new ArrayList<GFitDataType>() {
        {
            add(GFitDataType.TYPE_STEP_COUNT_DELTA);
            add(GFitDataType.AGGREGATE_STEP_COUNT_DELTA);
        }
    };

    public static Boolean IsChallengeExpired(Company company) {
        boolean expired = true;
        Date now = Calendar.getInstance().getTime();

        for (Challenge challenge : company.Challenges) {
            if (now.before(challenge.Finish)) expired = false;
        }
        return expired;
    }


    public static boolean isDarkModeOn(Context context) {
        int nightModeFlags = context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES;
    }

    public static boolean isPackageInstalled(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void showDialog(Context context, String title, String message) {
        showDialog(context, title, message, null);
    }

    public static void showDialog(Context context, String title, String message, DialogInterface.OnDismissListener listener) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
        dlgAlert.setMessage(message);
        dlgAlert.setTitle(title);
        dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dlgAlert.setCancelable(true);
        if (listener != null) {
            dlgAlert.setOnDismissListener(listener);
        }
        AlertDialog alertDialog = dlgAlert.create();
        alertDialog.setOnShowListener(dialogInterface -> {
            Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            button.setEnabled(true);
            if (isDarkModeOn(context)) {
                button.setTextColor(context.getColor(R.color.white));
            } else {
                button.setTextColor(context.getColor(R.color.black));
            }
        });
        alertDialog.show();
    }

    public static void DeleteUser(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = mPrefs.edit();
        edit.remove(USER_MODEL_KEY);
        edit.apply();
    }

    public static UserModel GetUser(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        String string = mPrefs.getString(USER_MODEL_KEY, "");
        Gson gson = new Gson();
        try {
            return gson.fromJson(string, UserModel.class);
        } catch (Exception e) {
            Log.d(TAG, "GetUser: ", e);
        }
        return null;
    }

    public static void showDialog(Context context, View view, DialogInterface.OnClickListener onClickListenerLogin, DialogInterface.OnClickListener onClickListenerChallenge) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle("Erfolg")
                .setMessage(context.getString(R.string.successText))
                .setPositiveButton("Zum Login", onClickListenerLogin)
                .setNeutralButton("Zur Challenge", onClickListenerChallenge)
                .setCancelable(false)
                .create();

        alertDialog.show();
    }

//    private static void goToChallenge(View view) {
//        NavOptions navOptions = new NavOptions.Builder().setLaunchSingleTop(true).setEnterAnim(R.anim.nav_default_enter_anim).build();
//        Navigation.findNavController(view).navigate(R.id.navigation_login, null, navOptions);
//    }
//
//    public static void goToLogin(View view) {
//        NavOptions navOptions = new NavOptions.Builder().setLaunchSingleTop(true).setEnterAnim(R.anim.nav_default_enter_anim).build();
//        Navigation.findNavController(view).navigate(R.id.navigation_login, null, navOptions);
//    }
    public static void goToLogin(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }
    public static void goToMain(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    public static String GetPin(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        String string = mPrefs.getString(PIN_CODE, "");
        try {
            return string;
        } catch (Exception e) {
            Log.d(TAG, "GetPin: ", e);
        }
        return null;
    }


    public static void SaveUser(Context context, UserModel userObject) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = mPrefs.edit();
        Gson gson = new Gson();
        String userObjectJson = gson.toJson(userObject);
        edit.putString(USER_MODEL_KEY, userObjectJson);
        edit.apply();
    }

    public static void SavePin(Context context, String pin) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = mPrefs.edit();
        edit.putString(PIN_CODE, String.valueOf(pin));
        edit.apply();
    }

    public static void setUserLoggedOut(Context context, boolean flag) {
        SharedPreferences mPrefs = context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = mPrefs.edit();
        edit.putBoolean(USER_LOGGED_OUT, flag);
        edit.apply();
    }

    public static boolean getUserLoggedOut(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE).getBoolean(USER_LOGGED_OUT, false);
    }

    public static void GoToChallenge(Context context, Company company) {
        /*  BaseActivity.COMPANY is null ??!! */
        context.startActivity((new Intent(context, MainActivity.class)).putExtra("COMPANY", company));
    }

    public static void HandleDeleteUser(Context context, Object response) {
        Log.d(TAG, "onResponse: " + response);
        if (response instanceof Boolean && (Boolean) response == Boolean.TRUE) return;
        GeneralHelper.showDialog(context, "Fehler", GeneralService.DELETE_USER_TEXT);
    }

    public static void handleLoginError(Context context, LoginRequestResponse response, HandleLoginErrorAction handleLoginErrorAction) {
        ArrayList<String> messages = new ArrayList<>();
        messages.add(response.error);

        StringBuilder message = new StringBuilder("Sie konnten nicht automatisch eingeloggt werden. Folgende Fehler sind aufgetretten: ");
        message.append("\n");
        for (String item : messages) {
            message.append("\n - ").append(item);
        }
        message.append("\n");
        message.append("\n");

        message.append("Bitte loggen Sie sich manuel ein.");
        GeneralHelper.showDialog(context, "Fehler", message.toString(), dialogInterface -> handleLoginErrorAction.Callback());
        GeneralHelper.DeleteUser(context);
    }

    public static boolean wasOnboardingShown(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE).getBoolean(ONBOARDING_SHOWN, false);
    }

    public static void setOnboardingShown(Context context, boolean flag) {
        context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE).edit().putBoolean(ONBOARDING_SHOWN, flag).apply();
    }

    public static void setStayLoggedIn(@NotNull Context context, boolean flag) {
        context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE).edit().putBoolean(STAY_LOGGED_IN, flag).apply();
    }

    public static boolean getStayLoggedIn(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE).getBoolean(STAY_LOGGED_IN, false);
    }

    public static void goToAccountDeletionSuccess(@NotNull Context context) {
        context.startActivity(new Intent(context, AccountDeletionSuccessActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public interface HandleLoginErrorAction {
        void Callback();
    }

    public static void goToRegistrySuccess(Context context, Company company) {
        context.startActivity(new Intent(context, RegistrySuccessActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK).putExtra(BaseActivity.COMPANY, company));
    }

    public static void goToPinCode(Context context) {
        setStayLoggedIn(context, false);
        SavePin(context, null);
        DeleteUser(context);
        context.startActivity(new Intent(context, PinCodeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
