package de.fuenfzig670.sample.model;

public class UserModelBase {
    public String email;
    public int id;

    public UserModelBase(String email, int id) {
        this.email = email;
        this.id = id;
    }
}
