package de.fuenfzig670.sample.model;

public class ArticleSummary {
    public int id;
    public String headline;
    public String created_at;
    public String updated_at;
    public String article_url;
}
