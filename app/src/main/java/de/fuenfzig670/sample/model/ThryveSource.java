package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ThryveSource implements Parcelable {
    public int Id;
    public int source_id;
    public String display_name;

    public ThryveSource(int id, int source_id, String display_name) {
        Id = id;
        this.source_id = source_id;
        this.display_name = display_name;
    }

    protected ThryveSource(Parcel in) {
        Id = in.readInt();
        source_id = in.readInt();
        display_name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(source_id);
        dest.writeString(display_name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ThryveSource> CREATOR = new Creator<ThryveSource>() {
        @Override
        public ThryveSource createFromParcel(Parcel in) {
            return new ThryveSource(in);
        }

        @Override
        public ThryveSource[] newArray(int size) {
            return new ThryveSource[size];
        }
    };
}
