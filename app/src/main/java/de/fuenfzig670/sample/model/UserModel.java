package de.fuenfzig670.sample.model;

public class UserModel extends UserModelBase {
    public String password;
    public String token;
    public int companyId;

    public String firstname;
    public String lastname;
    public String nickname;

    public Integer api_source;

    public UserModel(String email, String password, String token, int companyId, int id) {
        super(email, id);
        this.password = password;
        this.token = token;
        this.companyId = companyId;
    }
}
