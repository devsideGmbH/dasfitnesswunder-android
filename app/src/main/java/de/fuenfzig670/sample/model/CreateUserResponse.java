package de.fuenfzig670.sample.model;

import java.util.ArrayList;

public class CreateUserResponse {
    public int Id = -1;
    public ArrayList<String> firstname;
    public ArrayList<String> lastname;
    public ArrayList<String> email;
    public ArrayList<String> email_confirmation;
    public ArrayList<String> nickname;
    public ArrayList<String> agb_confirm;
    public ArrayList<String> polar_reg_confirm;
    public ArrayList<String> gender;
    public ArrayList<String> dayofbirth;
    public ArrayList<String> base;
    public ArrayList<String> userdropdowns_dropdownvalue;
    public ArrayList<String> messages = new ArrayList<>();
    public ArrayList<String> password;
    public ArrayList<String> password_confirmation;
    public ArrayList<String> security_code;
}
