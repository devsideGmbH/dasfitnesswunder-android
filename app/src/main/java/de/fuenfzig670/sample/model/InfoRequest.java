package de.fuenfzig670.sample.model;

public class InfoRequest {
    public String phone;
    public String lastname;
    public String firstname;
    public String company;
    public String employees;
    public String email;
    public String message;

    public InfoRequest(String phone, String lastname, String firstname, String company, String employees, String email, String message) {
        this.phone = phone;
        this.lastname = lastname;
        this.firstname = firstname;
        this.company = company;
        this.employees = employees;
        this.email = email;
        this.message = message;
    }

    public InfoRequest(String phone, String lastname, String firstname, String company,  String email, String message) {
        this.phone = phone;
        this.lastname = lastname;
        this.firstname = firstname;
        this.company = company;
        this.email = email;
        this.message = message;
    }
}
