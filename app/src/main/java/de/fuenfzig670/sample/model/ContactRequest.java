package de.fuenfzig670.sample.model;

public class ContactRequest {
    public String name;
    public String email;
    public String message;
    public String receiver = "";

    public ContactRequest(String name, String email, String message) {
        this.email = email;
        this.name = name;
        this.message = message;
    }
}
