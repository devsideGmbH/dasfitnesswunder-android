package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DropDown implements Parcelable {
    public int id;
    public String dropdowntitle;
    public ArrayList<DropDownValue> dropdownvalues;

    protected DropDown(Parcel in) {
        id = in.readInt();
        dropdowntitle = in.readString();
        dropdownvalues = in.createTypedArrayList(DropDownValue.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(dropdowntitle);
        dest.writeTypedList(dropdownvalues);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DropDown> CREATOR = new Creator<DropDown>() {
        @Override
        public DropDown createFromParcel(Parcel in) {
            return new DropDown(in);
        }

        @Override
        public DropDown[] newArray(int size) {
            return new DropDown[size];
        }
    };
}
