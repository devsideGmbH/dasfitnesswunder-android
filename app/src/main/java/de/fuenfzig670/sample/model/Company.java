package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Company implements Parcelable {
    public int Id;
    public String CompanyName;
    public String Slogan;
    public String Logo;
    public String ProfilUrl;
    public String ImageBigTeaser;
    public String ImageRegistration;
    public Date CreatedAt;
    public Date UpdatedAt;
    public ArrayList<Challenge> Challenges = new ArrayList<>();
    public ArrayList<Partner> Partners = new ArrayList<Partner>();
    public ArrayList<ThryveSource> MyProperty;
    public Registration registration;
    public byte[] Source;
    public String restricted;
    public boolean eco_challenge;

    public String sum_steps;

    public Double sum_ecos;
    public String sum_kilometers;

    public String mobile_map_url;

    public Company(){

    }

    protected Company(Parcel in) {
        Id = in.readInt();
        CompanyName = in.readString();
        Slogan = in.readString();
        Logo = in.readString();
        ProfilUrl = in.readString();
        ImageBigTeaser = in.readString();
        ImageRegistration = in.readString();
        CreatedAt = (Date) in.readSerializable();
        UpdatedAt = (Date) in.readSerializable();
        Challenges = in.createTypedArrayList(Challenge.CREATOR);
        Partners = in.createTypedArrayList(Partner.CREATOR);
        MyProperty = in.createTypedArrayList(ThryveSource.CREATOR);
        registration = in.readParcelable(Registration.class.getClassLoader());
        Source = in.createByteArray();
        restricted = in.readString();
        eco_challenge = in.readInt() == 1;
        sum_steps = in.readString();
        sum_kilometers = in.readString();
        mobile_map_url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(CompanyName);
        dest.writeString(Slogan);
        dest.writeString(Logo);
        dest.writeString(ProfilUrl);
        dest.writeString(ImageBigTeaser);
        dest.writeString(ImageRegistration);
        dest.writeSerializable(CreatedAt);
        dest.writeSerializable(UpdatedAt);
        dest.writeTypedList(Challenges);
        dest.writeTypedList(Partners);
        dest.writeTypedList(MyProperty);
        dest.writeParcelable(registration, flags);
        dest.writeByteArray(Source);
        dest.writeString(restricted);
        dest.writeInt(eco_challenge ? 1 : 0);
        dest.writeString(sum_steps);
        dest.writeString(sum_kilometers);
        dest.writeString(mobile_map_url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    public boolean isRestricted() {
        return "true".equalsIgnoreCase(restricted);
    }

    public boolean isRegistrationActive() {
        if (Challenges != null && Challenges.size() > 0) {
            Calendar hideDate = Calendar.getInstance();
            hideDate.setTime(Challenges.get(0).Finish);
//            hideDate.add(Calendar.DATE, Challenges.get(0).DaysVisible);
            if (hideDate.getTime().after(new Date())) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    protected boolean ActivatedContainsKey(Company company, String key) {
        String[] activatedSplinter = company.registration.activated.split(", ");
        return Arrays.asList(activatedSplinter).contains(key);
    }

    public boolean needsBirthDate() {
        return ActivatedContainsKey(this, "show_dayofbirth");
    }

    public boolean hasUserLogin() {
        return ActivatedContainsKey(this, "user_login");
    }

    public boolean needsSecurityCode() {
        return ActivatedContainsKey(this, "show_registration_secure_token");
    }
}
