package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RankingCategory implements Parcelable {
    public int Id;
    public int ranking_type;
    public String dropdowntitle;

    public Boolean isTeam = false;

    public RankingCategory(int id,String dt){
        this.Id = id;
        this.dropdowntitle = dt;
    }


    protected RankingCategory(Parcel in) {
        Id = in.readInt();
        ranking_type = in.readInt();
        dropdowntitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(ranking_type);
        dest.writeString(dropdowntitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RankingCategory> CREATOR = new Creator<RankingCategory>() {
        @Override
        public RankingCategory createFromParcel(Parcel in) {
            return new RankingCategory(in);
        }

        @Override
        public RankingCategory[] newArray(int size) {
            return new RankingCategory[size];
        }
    };

    public  int getId(){
        return Id;
    }
    @Override
    public String toString() {
        return dropdowntitle;
    }
}
