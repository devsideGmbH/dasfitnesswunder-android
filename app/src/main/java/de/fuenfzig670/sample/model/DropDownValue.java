package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DropDownValue implements Parcelable {
    public int id;
    public String dropdowvaluentitle;

    public DropDownValue(int id, String dropdowvaluentitle) {
        this.id = id;
        this.dropdowvaluentitle = dropdowvaluentitle;
    }

    protected DropDownValue(Parcel in) {
        id = in.readInt();
        dropdowvaluentitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(dropdowvaluentitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DropDownValue> CREATOR = new Creator<DropDownValue>() {
        @Override
        public DropDownValue createFromParcel(Parcel in) {
            return new DropDownValue(in);
        }

        @Override
        public DropDownValue[] newArray(int size) {
            return new DropDownValue[size];
        }
    };
}
