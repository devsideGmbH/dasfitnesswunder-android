package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Challenge implements Parcelable {
    public String ChallengeName;
    public Date Start;
    public Date Finish;
    public Date EntryStart;
    public int DaysVisible;


    protected Challenge(Parcel in) {
        ChallengeName = in.readString();
        DaysVisible = in.readInt();
        Start = (Date) in.readSerializable();
        Finish = (Date) in.readSerializable();
        EntryStart = (Date) in.readSerializable();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ChallengeName);
        dest.writeInt(DaysVisible);
        dest.writeSerializable(Start);
        dest.writeSerializable(Finish);
        dest.writeSerializable(EntryStart);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Challenge> CREATOR = new Creator<Challenge>() {
        @Override
        public Challenge createFromParcel(Parcel in) {
            return new Challenge(in);
        }

        @Override
        public Challenge[] newArray(int size) {
            return new Challenge[size];
        }
    };
}
