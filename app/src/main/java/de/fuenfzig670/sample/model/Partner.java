package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Partner implements Parcelable {
    public String PartnerName;
    public String Logo;
    public String Link;
    public Date CreatedAt;
    public Date UpdatedAt;

    protected Partner(Parcel in) {
        PartnerName = in.readString();
        Logo = in.readString();
        Link = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(PartnerName);
        dest.writeString(Logo);
        dest.writeString(Link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Partner> CREATOR = new Creator<Partner>() {
        @Override
        public Partner createFromParcel(Parcel in) {
            return new Partner(in);
        }

        @Override
        public Partner[] newArray(int size) {
            return new Partner[size];
        }
    };
}
