package de.fuenfzig670.sample.model;

public class PasswordResetRequest {
    public String email;

    public PasswordResetRequest(String email) {
        this.email = email;
    }
}
