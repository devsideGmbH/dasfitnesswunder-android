package de.fuenfzig670.sample.model;

import androidx.annotation.DrawableRes;

import de.fuenfzig670.dasfitnesswunder.R;

public class UserRanking {
    public int Ranking;
    public int Id;
    public String Nickname;
    public int Gender;
    public String Steps;
    public Boolean ShowGender = false;
    public String eco;
    public String tendency;

    public int group_header;

    public boolean isGroupHeader() {
        return group_header == 1;
    }

    public String GenderString() {
        if (!ShowGender) return "";
        return Gender == 0 ? "♂" : "♀";
    }

    public @DrawableRes
    int GenderDrawableId() {
        if (!ShowGender) return R.drawable.gender_male_man;
        return Gender == 0 ? R.drawable.gender_male_man :
            Gender == 1 ? R.drawable.female_gender : R.drawable.equality_female_gender_male;
    }

    public @DrawableRes
    int tendencyIcon() {
        int icon;
        if (tendency != null) {

            if (tendency.equals("up")) {
                icon = R.drawable.ic_baseline_arrow_drop_up_24_green;
            } else if (tendency.equals("down")) {
                icon = R.drawable.ic_baseline_arrow_drop_down_24_red;
            } else {
                icon = R.drawable.baseline_circle_24;
            }
        } else {
            icon = R.drawable.baseline_circle_24;
        }
        return icon;
    }
}
