package de.fuenfzig670.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Registration implements Parcelable {
    public String activated;
    public ArrayList<DropDown> dropdowns;
    public ArrayList<ThryveSource> api_source;


    protected Registration(Parcel in) {
        activated = in.readString();
        dropdowns = in.createTypedArrayList(DropDown.CREATOR);
        api_source = in.createTypedArrayList(ThryveSource.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(activated);
        dest.writeTypedList(dropdowns);
        dest.writeTypedList(api_source);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Registration> CREATOR = new Creator<Registration>() {
        @Override
        public Registration createFromParcel(Parcel in) {
            return new Registration(in);
        }

        @Override
        public Registration[] newArray(int size) {
            return new Registration[size];
        }
    };
}
