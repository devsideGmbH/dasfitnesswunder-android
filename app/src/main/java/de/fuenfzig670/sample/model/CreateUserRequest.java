package de.fuenfzig670.sample.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.fuenfzig670.sample.activities.RegistryStepsActivity;

public class CreateUserRequest {
    public int gender;
    public Map<Integer, Map<String, String>> userdropdowns_attributes;
    public String dayofbirth;
    public String nickname;
    public String lastname;
    public String firstname;
    public String email;
    public String email_confirmation;
    public Boolean polar_reg_confirm;
    public Boolean agb_confirm;
    public String api_source_id;
    public String password;
    public String password_confirmation;
    public String security_code;

    public CreateUserRequest(int gender,
                             String dayofbirth, String nickname, String lastname, String firstname,
                             String email, String email_confirmation,
                             Boolean polar_reg_confirm, Boolean agb_confirm, String api_source_id, String password, String password_confirmation
        , String securityCode, ArrayList<Integer> dropDownValues) {
        this.gender = gender;
        this.userdropdowns_attributes = null;
        if (dropDownValues.size() > 0) {
            this.userdropdowns_attributes = new HashMap();
            for (int i = 0; i < dropDownValues.size(); i++) {
                int value = dropDownValues.get(i);
                if (value != RegistryStepsActivity.DROPDOWN_NOT_CREATED) {
                    HashMap<String, String> hashMap = new HashMap();
                    hashMap.put("dropdownvalue_id", String.valueOf(value));
                    userdropdowns_attributes.put(i, hashMap);
                }
            }
        }

        this.dayofbirth = dayofbirth;
        this.nickname = nickname;
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.email_confirmation = email_confirmation;
        this.polar_reg_confirm = polar_reg_confirm;
        this.agb_confirm = agb_confirm;
        this.api_source_id = api_source_id.equals("-1") ? null : api_source_id;
        this.password = password;
        this.password_confirmation = password_confirmation;
        this.security_code = securityCode;
    }
}
