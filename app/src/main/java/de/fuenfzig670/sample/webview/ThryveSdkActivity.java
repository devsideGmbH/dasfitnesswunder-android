package de.fuenfzig670.sample.webview;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import androidx.appcompat.app.ActionBar;

import com.thryve.connector.sdk.CoreConnector;
import com.thryve.connector.sdk.logger.Logger;
import com.thryve.connector.sdk.model.ThryveResponse;
import com.thryve.connector.sdk.network.ConnectorCallback;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.activities.BaseActivity;
import de.fuenfzig670.sample.dataservice.GeneralService;
import de.fuenfzig670.sample.helpers.GeneralHelper;
import de.fuenfzig670.sample.model.Company;
import de.fuenfzig670.sample.model.ThryveToken;
import kotlin.jvm.internal.Intrinsics;

public final class ThryveSdkActivity extends WebViewActivity {
    public Company company;
    public int companyId = -1;
    public int userId = -1;
    public int thryveSourceId = -1;
    CoreConnector _coreConnector;
    boolean err = true;

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        if (err) {
            GeneralService generalService = new GeneralService(this);
            generalService.DeleteUser(companyId, userId, false, response -> GeneralHelper.HandleDeleteUser(ThryveSdkActivity.this, response));
        }
        super.onDestroy();
    }

    public void loadWebView() {
        Intent i = this.getIntent();
        company = i.getParcelableExtra(BaseActivity.COMPANY);
        companyId = i.getIntExtra("companyId", -1);
        userId = i.getIntExtra("userId", -1);
        if (this.getIntent().hasExtra("thryveSourceId")) {
            thryveSourceId = i.getIntExtra("thryveSourceId", -1);
            _coreConnector = new CoreConnector(this, getResources().getString(R.string.appId), getResources().getString(R.string.appSecret));
            AsyncTask<Void, Void, String> execute = new GetAccessToken().execute();
        }
    }

    class GetAccessToken extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            directConnect();
            super.onPostExecute(s);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                ThryveResponse<String> accessToken = _coreConnector.getAccessToken();
                if (accessToken != null) {
                    return accessToken.getData();
                }
                return "";
            } catch (IOException e) {
                Log.d(getClass().getName(), "doInBackground: ", e);
                e.printStackTrace();
                return "";
            }
        }
    }

    private void directConnect() {
        WebView webView = this.webView;
        Intrinsics.checkNotNull(webView);
        Log.d("ThryveloadWebView", "handleDataSourceDirectConnection with companyId: " + companyId + " userId: " + userId + " thryveSourceId: " + thryveSourceId);
        findViewById(R.id.progressBar_cyclic).setVisibility(View.GONE);
        _coreConnector.handleDataSourceDirectConnection(true, thryveSourceId, false, webView, new ConnectorCallback() {
            public void onResult(@Nullable String accessToken, @Nullable String url, int dataSourceType, boolean connected, @Nullable String message) {
                Logger.d("Thryve_DIRECT_CONNECTION", url + getResources().getString(R.string.direct_connection_result, dataSourceType, accessToken, connected, message));
                GeneralService generalService = new GeneralService(ThryveSdkActivity.this);
                ThryveToken token = new ThryveToken(accessToken);
                generalService.SetThryveToken(companyId, userId, token, x -> {
                    if (x instanceof Boolean && (Boolean) x) {
                        err = false;
                        goToSuccess();
                    } else {
                        GeneralHelper.showDialog(ThryveSdkActivity.this, "Fehler", "Verbindung mit dem Server fehlgeschlagen. Wenden Sie sich an den Support.", dialogInterface -> generalService.DeleteUser(companyId, userId, false, response -> GeneralHelper.HandleDeleteUser(ThryveSdkActivity.this, response)));
                    }
                    Log.d("SetThryveToken", "Response: " + x);
                });
            }


            public void onError(@Nullable String accessToken, @Nullable String url, int dataSourceType, @Nullable String message) {
                Logger.e("Thryve_DIRECT_CONNECTION", url);
                Logger.w(ThryveSdkActivity.class.getSimpleName(), getResources().getString(R.string.direct_connection_error, dataSourceType, accessToken, message));
                GeneralHelper.showDialog(ThryveSdkActivity.this, "Fehler", "Verbindung mit Fitness Tracker fehlgeschlagen. Wenden Sie sich an den Support.", dialogInterface -> {
                    ThryveSdkActivity.this.finish();
                    GeneralService generalService = new GeneralService(ThryveSdkActivity.this);
                    generalService.DeleteUser(companyId, userId, false, response -> GeneralHelper.HandleDeleteUser(ThryveSdkActivity.this, response));
                });
            }

            public void onSocialLoginError(String accessToken, String url, int dataSourceType, String message) {
                // Handle error
                Log.w("MyTag", "SocialLogin not supported in WebView. Source " + dataSourceType + ". Token " + accessToken);
            }
        });
    }

    private void goToSuccess() {
        GeneralHelper.goToRegistrySuccess(this, _company);
    }
}
