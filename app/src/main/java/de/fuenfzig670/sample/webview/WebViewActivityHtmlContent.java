package de.fuenfzig670.sample.webview;

import android.util.Log;
import android.webkit.WebView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import de.fuenfzig670.sample.dataservice.GeneralService;
import de.fuenfzig670.sample.model.InfoRequest;
import de.fuenfzig670.sample.model.InfoRequestResponse;
import de.fuenfzig670.dasfitnesswunder.R;

import org.json.JSONException;
import org.json.JSONObject;

public final class WebViewActivityHtmlContent extends WebViewActivity {
    public void loadWebView() {
        WebView webView = (WebView) findViewById(R.id.web_view);

        // Get a RequestQueue
//        RequestQueue queue = MySingleton.getInstance(this.getApplicationContext()).
//                getRequestQueue();

// ...

// Add a request (in this example, called stringRequest) to your RequestQueue.

        String url = "/api/v1/tenants/1/articles";
//        String url = Constants.BASEURL + "/api/v1/inforequests";
        RequestQueue queue = Volley.newRequestQueue(this);

        /*MySingleton.getInstance(this).getCall(url, new VolleyCallback(){
            @Override
            public void onSuccess(String result){
                webView.loadData(result, "text/html; charset=utf-8", "UTF-8");
            }
        });*/

//        MySingleton.getInstance(this).getCall(url, result -> webView.loadData(result, "text/html; charset=utf-8", "UTF-8"));
//        webView.loadData(RestService.getCall(url).toString(), "text/html; charset=utf-8", "UTF-8");

        ;
        String url2 = "/api/v1/inforequests";

        JSONObject student1 = new JSONObject();
        try {
            student1.put("phone", "1234");
            student1.put("lastname", "tester");
            student1.put("firstname", "test123");
            student1.put("company", "Arts");
            student1.put("email", "a@b.test");
            student1.put("message", "blablubblorem ipsum dolor sit amet");

        } catch (JSONException e) {
            Log.d(TAG, "loadWebView: ",e);
        }


//        RestService.getInstance(this).postCall(url2, student1, result -> webView.loadData("the result is" + result, "text/html; charset=utf-8", "UTF-8"));

// Access the RequestQueue through your singleton class.
//        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
//        queue.add(jsonObjectRequest);
//        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


        Gson gson = new Gson();
        InfoRequestResponse bla = gson.fromJson("{\"lastname\":[\"Der Nachname ist zu kurz (min. 3 Zeichen)\"]}", InfoRequestResponse.class);


        GeneralService generalService = new GeneralService(this);


        InfoRequest infoRequest = new InfoRequest("1234", "", "test123", "testCompany","testEmployees", "a@b.test", "blablubblorem ipsum dolor sit amet");

        generalService.SendInfoRequest(infoRequest, result -> webView.loadData("the result is" + result.toString(), "text/html; charset=utf-8", "UTF-8"));

    }
}
