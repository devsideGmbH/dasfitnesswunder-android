package de.fuenfzig670.sample.webview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebView;

import com.thryve.connector.sdk.CoreConnector;
import de.fuenfzig670.dasfitnesswunder.R;

import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

import de.fuenfzig670.sample.activities.BaseActivity;
import kotlin.jvm.JvmField;
import kotlin.jvm.internal.Intrinsics;

public class WebViewActivity extends BaseActivity {
    @JvmField
    @Nullable
    public WebView webView;
    private HashMap _$_findViewCache;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_web_view);
        this.webView = (WebView) this.findViewById(R.id.web_view);
        this.loadWebView();
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public void loadWebView() {
//        CoreConnector coreConnector = new CoreConnector((Context)this, Constants.appId, Constants.partnerUserId, (String)null, (String)null, 24, (DefaultConstructorMarker)null);
        CoreConnector coreConnector = new CoreConnector((Context) this, getResources().getString(R.string.appId), getResources().getString(R.string.partnerUserId));
        WebView webView = this.webView;
        Intrinsics.checkNotNull(webView);
        coreConnector.handleDataSourceConnection(webView);
    }
}
