package de.fuenfzig670.sample.activities

import android.os.Bundle
import androidx.activity.addCallback
import androidx.activity.compose.setContent
import androidx.appcompat.app.ActionBar
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.GreenButton
import de.fuenfzig670.sample.activities.composables.Headline1
import de.fuenfzig670.sample.activities.composables.ParagraphRegular
import de.fuenfzig670.sample.helpers.GeneralHelper

class AccountDeletionSuccessActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.layout_actionbar_full)
        findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tvTitle)?.text = "Account"
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        this.onBackPressedDispatcher.addCallback { GeneralHelper.goToPinCode(this@AccountDeletionSuccessActivity) }
        setContent {
            AccountDeletionSuccessScreen()
        }
    }

    @Preview(showSystemUi = true, showBackground = true)
    @Composable
    fun AccountDeletionSuccessScreenPreview() {
        AccountDeletionSuccessScreen()
    }

    @Composable
    fun AccountDeletionSuccessScreen() {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(24.dp))
                Headline1(text = "Account gelöscht")
                Spacer(modifier = Modifier.height(16.dp))
                ParagraphRegular(text = "Dein Account wurde erfolgreich gelöscht.")
                Spacer(modifier = Modifier.height(62.dp))
                ImageView(imageId = R.drawable.pincode_success_image)
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                GreenButton(text = "Zurück zum Start", onClick = { GeneralHelper.goToPinCode(this@AccountDeletionSuccessActivity) })
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}
