package de.fuenfzig670.sample.activities.ui.challenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.dasfitnesswunder.databinding.FragmentChallengeBinding;
import de.fuenfzig670.sample.activities.BaseFragment;
import de.fuenfzig670.sample.activities.ui.ranking_list.RankingListFragment;
import de.fuenfzig670.sample.adapter.RankingsRecyclerViewAdapter;
import de.fuenfzig670.sample.dataservice.CompanyDataService;
import de.fuenfzig670.sample.helpers.GeneralHelper;
import de.fuenfzig670.sample.helpers.NoConnectionHelper;
import de.fuenfzig670.sample.model.Company;
import de.fuenfzig670.sample.model.GroupRanking;
import de.fuenfzig670.sample.model.RankingCategory;
import de.fuenfzig670.sample.model.UserModel;
import de.fuenfzig670.sample.model.UserRanking;

@Deprecated
public class ChallengeFragment extends BaseFragment {

    protected int _selectedRanking = -1;
    private CompanyDataService _companyDataService;
    private ArrayList<RankingCategory> _rankingCategories;
    private ArrayList<RankingCategory> _dynamicRankingCategories;
    private ArrayList<RankingCategory> _allRankingCategories;
    private ArrayList<ArrayList<UserRanking>> _userRankings = new ArrayList<>();
    private ArrayList<Integer> _rankingOrder = new ArrayList<>();
    protected int _selectedCategory = 0;
    private View _challengeTableHeaderWrapperStart;
    private View _challengeTableHeaderWrapperEnd;
    private View _progressBar_cyclic;
    private Spinner spinnerRankings;
    private boolean alreadyGotRankings;

    private FragmentChallengeBinding binding;

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        Context context = getContext();
        if (context != null) {
            boolean isRankings = this instanceof RankingListFragment;
            UserModel userModel = GeneralHelper.GetUser(context);
            if (!isRankings && userModel == null) {
                inflater.inflate(R.menu.login, menu);
                MenuItem item = menu.findItem(R.id.menu_login);
                if (item != null) {
                    View actionView = item.getActionView();
                    if (actionView != null) {
                        Button button_menu_login = actionView.findViewById(R.id.button_menu_login);
                        button_menu_login.setOnClickListener(view -> {
//                            NavOptions navOptions = new NavOptions.Builder().setLaunchSingleTop(true).setEnterAnim(R.anim.nav_default_enter_anim).build();
//                            Navigation.findNavController(root).navigate(R.id.navigation_login, null, navOptions);
                        });
                    }
                }
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    protected boolean ActivatedContainsKey(Company company, String key) {
        String[] activatedSplinter = company.registration.activated.split(", ");
        return Arrays.asList(activatedSplinter).contains(key);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_login) {
//            NavOptions navOptions = new NavOptions.Builder().setLaunchSingleTop(true).setEnterAnim(R.anim.nav_default_enter_anim).build();
//            Navigation.findNavController(root).navigate(R.id.navigation_login, null, navOptions);
        }
        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        binding = FragmentChallengeBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        if (this instanceof RankingListFragment) {
        } else {
            setUpFragment();
        }

        return root;
    }

    protected void setUpFragment() {
        spinnerRankings = root.findViewById(R.id.spinnerRankings);

        spinnerRankings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                boolean isRankingActivity = ChallengeFragment.this instanceof RankingListFragment;
                if (!isRankingActivity && position == 0) return;

                ShowLoadingIndicator();
                spinnerRankings.setSelection(position);
                if (isRankingActivity) {
                    _selectedCategory = _selectedRanking = position;
                    showChallenge();
                } else {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        Intent intent = getIntent();
                        if (intent == null) return;
                        intent.putExtra("selectedRanking", position - 1);
                    }
                    Navigation.findNavController(parentView).navigate(R.id.action_navigation_home_to_navigation_dashboard);
                    spinnerRankings.setSelection(0);
                }
                //HideLoadingIndicator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        Intent i = getIntent();
        if (i == null) return;
        if (i.hasExtra("company"))
        if (i.hasExtra("selectedRanking")) {
            _selectedRanking = _selectedCategory = i.getIntExtra("selectedRanking", -1);
            i.removeExtra("selectedRanking");
        }

        if (_company != null) {
            _hasUserLogin = ActivatedContainsKey(_company, "user_login");
//            _needsBirthDate = ActivatedContainsKey(_company, "show_dayofbirth");
//            _needsSecurityCode = ActivatedContainsKey(_company, "show_registration_secure_token");
            setupHeader();
        }
        Context context = getContext();
        if (context == null) return;

        setUpMap(_company);

        _companyDataService = new CompanyDataService(context);

        _challengeTableHeaderWrapperStart = root.findViewById(R.id.challengeTableHeaderWrapperStart);
        _challengeTableHeaderWrapperEnd = root.findViewById(R.id.challengeTableHeaderWrapperEnd);
        _progressBar_cyclic = root.findViewById(R.id.progressBar_cyclic);

        if (_company.Challenges != null && _company.Challenges.size() > 0) {
            Calendar hideDate = Calendar.getInstance();
            hideDate.setTime(_company.Challenges.get(0).Finish);
            hideDate.add(Calendar.DATE, _company.Challenges.get(0).DaysVisible);
            if (hideDate.getTime().after(new Date())) {
                showChallenge();
            } else {
                hideChallenge();
            }
        } else {
            hideChallenge();
        }
    }

    private void setUpMap(Company company) {
        if (company.mobile_map_url == null || company.mobile_map_url.isEmpty()) {
            binding.mapWebview.setVisibility(View.GONE);
        } else {
            binding.mapWebview.getSettings().setJavaScriptEnabled(true);
            binding.mapWebview.getSettings().setDomStorageEnabled(true);
            binding.mapWebview.setOnClickListener(view -> startFullScreenWebView(company.mobile_map_url));
            binding.mapWebview.loadUrl(company.mobile_map_url);

            binding.mapWebview.setOnTouchListener(new View.OnTouchListener() {
                private final static long MAX_TOUCH_DURATION = 100;
                private long m_DownTime = 0;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            m_DownTime = event.getEventTime(); //init time
                            break;
                        case MotionEvent.ACTION_UP:
                            if (event.getEventTime() - m_DownTime <= MAX_TOUCH_DURATION)
                                startFullScreenWebView(company.mobile_map_url);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            break;
                        default:
                            break; //No-Op
                    }
                    return false;
                }
            });
        }
    }

    private void startFullScreenWebView(String url) {
//        Intent intent = new Intent(getContext(), WebViewFullscreenActivity.class).putExtra(WebViewFullscreenActivity.URL_KEY, url);
//        startActivity(intent);
    }

    //   Challenge States =================================================================
    private void hideChallenge() {
//        _registrationActive = false;
        root.findViewById(R.id.wrapper_inactive_challenge).setVisibility(View.VISIBLE);
    }

    private void ShowLoadingIndicator() {
        if (_progressBar_cyclic != null)
            _progressBar_cyclic.setVisibility(View.VISIBLE);
    }

    private void HideLoadingIndicator() {
        if (_progressBar_cyclic != null)
            _progressBar_cyclic.setVisibility(View.GONE);
    }

    private void initializeSpinner(int id, List<RankingCategory> content) {
        Context context = getContext();
        if (context == null) return;
        ArrayAdapter<RankingCategory> adapter = new ArrayAdapter<RankingCategory>(context, R.layout.simple_spinner_layout, content);
        Spinner spinner = root.findViewById(id);
        spinner.setAdapter(adapter);
        spinner.setSelection(_selectedCategory);
    }

    protected void setupRankings() {
        Context context = getContext();
        if (context == null) return;
        boolean isRankingFragment = this instanceof RankingListFragment;
        alreadyGotRankings = alreadyGotRankings && isRankingFragment;
        if (!alreadyGotRankings) {
            _allRankingCategories = new ArrayList<>();
            if (!(this instanceof RankingListFragment)) {
                String topTen = getString(R.string.top_10);
                _allRankingCategories.add(new RankingCategory(-1, topTen));
            }
            _allRankingCategories.addAll(_rankingCategories);
            _allRankingCategories.addAll(_dynamicRankingCategories);
            initializeSpinner(R.id.spinnerRankings, _allRankingCategories);
            alreadyGotRankings = true;
        }
        //setupRankingsFooter();
        fillRankings();
    }

    protected void fillRankings() {
        Context context = getContext();
        if (context == null) return;
        _userRankings.clear();
        int rankingCount = _rankingCategories.size() + _dynamicRankingCategories.size();
        for (int i = 0; i < rankingCount; i++) {
            _userRankings.add(new ArrayList<>());
        }

        for (int i = 0; i < _rankingCategories.size(); i++) {
            int iFinal = i;
            RankingCategory rankingCategory = _rankingCategories.get(i);
            int rankingId = rankingCategory.Id;
            Boolean showGender = rankingCategory.dropdowntitle.contains(getString(R.string.gender_enabled_required));
            _rankingOrder.add(rankingId);
            _companyDataService.GetSpecificRanking(_company.Id, rankingId, showGender, x -> {
                try {
                    if (x == null) {
                        NoConnectionHelper.showNoDataToast(context);
                        return;
                    }
                    _userRankings.set(iFinal, (ArrayList<UserRanking>) x);
                    if (iFinal == _selectedCategory) showRankingCategory(_selectedCategory);
                } catch (Exception e) {
                    NoConnectionHelper.showNoDataToast(context);
                    Log.d(TAG, "fillRankings: ", e);
                }
            });
        }

        for (int i = 0; i < _dynamicRankingCategories.size(); i++) {
            int iFinal = i;
            RankingCategory rankingCategory = _dynamicRankingCategories.get(i);
            int rankingId = rankingCategory.Id;
            Boolean showGender = rankingCategory.dropdowntitle.contains(getString(R.string.gender_enabled_required));
            _rankingOrder.add(rankingId);
            _companyDataService.GetSpecificDynamicRanking(_company.Id, rankingId, showGender, x -> {
                try {
                    if (x == null) {
                        NoConnectionHelper.showNoDataToast(context);
                        return;
                    }
                    int rankingSize = 0;
                    if (_rankingCategories != null) rankingSize = _rankingCategories.size();
                    _userRankings.set(iFinal + rankingSize, (ArrayList<UserRanking>) x);
                    if ((iFinal + rankingSize) == _selectedCategory)
                        showRankingCategory(_selectedCategory);
                } catch (Exception e) {
                    NoConnectionHelper.showNoDataToast(context);
                    Log.d(TAG, "fillRankings: ", e);
                }
            });
        }
    }

    private void showRankingCategory(int category) {
        _selectedCategory = category;

        if ((_userRankings.size() - 1) < category) return;
        Context context = getContext();
        if (context == null) return;

        RecyclerView recyclerView = root.findViewById(R.id.rankings_body);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        ArrayList<UserRanking> ranking = _userRankings.get(category);
        if (ranking == null || ranking.size() == 0) {
            ranking = new ArrayList<>();
            NoConnectionHelper.showNoDataToast(context);
        }
        View genderHeader = findViewById(R.id.text_gender);
        View text_steps = findViewById(R.id.text_steps);
        if (ranking.size() == 0 || ranking.get(0).ShowGender) {
            genderHeader.setVisibility(View.VISIBLE);
            if (text_steps instanceof TextView)
                ((TextView) text_steps).setText(R.string.steps_header);
        } else {
            genderHeader.setVisibility(View.GONE);
            if (text_steps instanceof TextView)
                ((TextView) text_steps).setText("Ø " + getString(R.string.steps_header));
        }

        ArrayList<UserRanking> rankings = new ArrayList<>();

        if (this instanceof RankingListFragment) {
            rankings = ranking;
            UserModel userModel = GeneralHelper.GetUser(context);
            if (userModel != null && !hasGroupHeader(rankings)) {
                int userPostion = -1;
                for (int i = 0; i < rankings.size(); i++) {
                    if (rankings.get(i).Id == userModel.id) {
                        userPostion = i;
                        break;
                    }
                }
                if (userPostion != -1) {
                    UserRanking userRanking = rankings.get(userPostion);
                    rankings.remove(userPostion);
                    rankings.add(0, userRanking);
                }
            }
        } else {
            ArrayList<UserRanking> tmp = new ArrayList<>();
            for (UserRanking item : ranking) {
                if (!item.isGroupHeader()) tmp.add(item);
            }
            for (int i = 0; i < 10 && i < tmp.size(); i++) {
                UserRanking userRanking = tmp.get(i);
                userRanking.Ranking = i + 1;
                rankings.add(userRanking);
            }
        }
        RankingsRecyclerViewAdapter bodyAdapter = new RankingsRecyclerViewAdapter(context, rankings, this instanceof RankingListFragment, _company, hasGroupHeader(rankings));
        recyclerView.setAdapter(bodyAdapter);
        updatePloggingHeader();
        updateTableHeader();
        if (rankings == null || rankings.size() == 0) {
            updateGroupTableHeader(false);
        } else {
            updateGroupTableHeader(rankings.get(0) instanceof GroupRanking);
        }
        HideLoadingIndicator();
    }

    private boolean hasGroupHeader(ArrayList<UserRanking> rankings) {
        boolean hasGroupHeader = false;
        if (rankings != null)
            for (UserRanking item : rankings) {
                if (item.isGroupHeader()) {
                    hasGroupHeader = true;
                    break;
                }
            }
        return hasGroupHeader;
    }

    private void showChallenge() {
        _dynamicRankingCategories = null;
        _rankingCategories = null;
        ShowLoadingIndicator();
        Context context = getContext();
        if (context == null) return;
//        _registrationActive = true;
        _companyDataService.GetAllRankings(_company.Id, x -> {
            _rankingCategories = (ArrayList<RankingCategory>) x;
            if (_rankingCategories == null) {
                NoConnectionHelper.showNoConnectionToast(context);
                return;
            }
            if (_dynamicRankingCategories != null) setupRankings();

        });
        _companyDataService.GetDynamicRankings(_company.Id, x -> {
            _dynamicRankingCategories = (ArrayList<RankingCategory>) x;
            if (_rankingCategories != null) setupRankings();
        });
        View wrapper_inactive_challenge = root.findViewById(R.id.wrapper_inactive_challenge);
        if (wrapper_inactive_challenge != null) {
            wrapper_inactive_challenge.setVisibility(View.GONE);
        }
        setupTiles();
//        setupProfilePanel();
    }

    private String GetCounterDaysString(DateTime end, DateTime start) {
        return Days.daysBetween(start, end).getDays() + " Tage";
//        return String.valueOf(Days.daysBetween(start, end).getDays());
    }


    private String GetCounterStringWithTime(DateTime end, DateTime start) {
        Date endDate = end.toDate();
        Date startDate = start.toDate();

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        return getResources().getString(R.string.counter_string, String.valueOf(elapsedDays), String.valueOf(elapsedHours), String.valueOf(elapsedMinutes));
    }

    private void setupTiles() {
        DateTime start = new DateTime(_company.Challenges.get(0).Start);
        DateTime utcNow = new DateTime(DateTimeZone.UTC);
        DateTime entryStart = new DateTime(_company.Challenges.get(0).EntryStart);
        DateTime finish = new DateTime(_company.Challenges.get(0).Finish);

        String dateFormat = "dd.MM.yyyy";
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd.MM.yyyy");

        if (_challengeTableHeaderWrapperStart != null && _challengeTableHeaderWrapperEnd != null) {
            TextView firstTitleLabel = _challengeTableHeaderWrapperStart.findViewById(R.id.challengeTableHeaderWrapperStartText);
            TextView secondTitleLabel = _challengeTableHeaderWrapperEnd.findViewById(R.id.challengeTableHeaderWrapperEndText);

            DateTime now = DateTime.now();

            boolean entryStarted = entryStart.isBefore(now);
            boolean isRunning = now.isAfter(start) && now.isBefore(finish);
            boolean stopped = now.isAfter(finish);

            if (firstTitleLabel != null && secondTitleLabel != null) {
                setTimes("Die Anmeldung startet am", entryStart.toString(dateFormat),
                    "Die Challenge startet am", start.toString(dateFormat));
//                firstTitleLabel.setText("Die Anmeldung startet am\n" + entryStart.toString(dateFormat));
//                secondTitleLabel.setText("Die Challenge startet am\n" + start.toString(dateFormat));

                if (entryStarted) {
                    setTimes("Die Anmeldung läuft noch", GetCounterStringWithTime(start, now),
                        "Die Challenge startet am", start.toString(dateFormat));
//                    firstTitleLabel.setText("Die Anmeldung läuft noch\n" + GetCounterStringWithTime(start, now)); // \(GetCounterStringWithTime(entryStart, start))");
//                    secondTitleLabel.setText("Die Challenge startet am\n" + start.toString(dateFormat)); //  \(start.dateString(strFormat))");
                }
                if (isRunning) {
                    setTimes("Challenge Startschuss", start.toString(dateFormat),
                        "Die Challenge läuft noch", GetCounterStringWithTime(finish, now));
//                    firstTitleLabel.setText("Challenge Startschuss\n" + start.toString(dateFormat)); // \(start.dateString(strFormat))");
//                    secondTitleLabel.setText("Die Challenge läuft noch\n" + GetCounterStringWithTime(finish, now)); // \(GetCounterString(finish, now))");
                }
                if (stopped) {
                    setTimes("Challenge Startschuss", start.toString(dateFormat),
                        "Die Challenge ist beendet seit", finish.toString(dateFormat));
//                    firstTitleLabel.setText("Challenge Startschuss\n" + start.toString(dateFormat)); // \(start.dateString(strFormat))");
//                    secondTitleLabel.setText("Die Challenge ist beendet seit\n" + finish.toString(dateFormat)); // \(finish.dateString(strFormat))");
                }
            }

            updatePloggingHeader();

//            binding.challengeTableHeaderSum
//            binding.challengeTableHeaderWrapperSumImage
            binding.challengeTableHeaderWrapperSumText.setText(requireContext().getString(R.string.sum_header, _company.sum_steps, _company.sum_kilometers));

//            if (startText != null) {
//                startText.setText("Challenge Startschuss:\n" + start.toString(dateFormat));
//            }
//            if (endText != null) {
//                endText.setText("Challenge läuft noch:\n" + String.valueOf(GetCounterDaysString(utcNow, finish)) + " Tage");
//                if (utcNow.isAfter(finish)){
//                    endText.setText("Die Challenge ist beendet seit:\n" + finish.toDateTime(DateTimeZone.UTC).toString(dateFormat));
//                }
//            }
        }
    }

    private void setTimes(String firstTitle, String firstTime, String secondTitle, String secondTime) {
        TextView firstTitleLabel = _challengeTableHeaderWrapperStart.findViewById(R.id.challengeTableHeaderWrapperStartText);
        TextView secondTitleLabel = _challengeTableHeaderWrapperEnd.findViewById(R.id.challengeTableHeaderWrapperEndText);
        TextView titleFirstTime = (TextView) findViewById(R.id.challengeTableHeaderWrapperEndTextFirstTime);
        TextView titleSecondTime = (TextView) findViewById(R.id.challengeTableHeaderWrapperEndTextSecondTime);
        firstTitleLabel.setText(firstTitle);
        titleFirstTime.setText(firstTime);
        secondTitleLabel.setText(secondTitle);
        titleSecondTime.setText(secondTime);
    }

    private void updatePloggingHeader() {
        View ploggingHeader = findViewById(R.id.challengeTableHeaderPlogging);
        if (ploggingHeader != null) {
            ploggingHeader.setVisibility(View.GONE);
            if (isEcoChallenge()) {
                ploggingHeader.setVisibility(View.VISIBLE);
                View ploggingText = findViewById(R.id.challengeTableHeaderWrapperPloggingText);
                if (ploggingText instanceof TextView) {
                    ((TextView) ploggingText).setText("Gesammelter Abfall" + System.lineSeparator() + getTrashCount() + " Liter");
                }
            }
        }

    }

    private void updateTableHeader() {
        View trashView = findViewById(R.id.text_trash);
        trashView.setVisibility(View.GONE);
        if (isEcoChallenge()) {
            trashView.setVisibility(View.VISIBLE);
        }
    }

    private void updateGroupTableHeader(boolean isGroup) {
        TextView nickNameView = (TextView) findViewById(R.id.text_nickname);
        if (isGroup) {
            nickNameView.setText("NAME");
        }
    }

    private double getTrashCount() {
        double res = 0.0;
        if (_userRankings != null && !(_userRankings.size() - 1 < _selectedCategory)) {
            ArrayList<UserRanking> userRankings = _userRankings.get(_selectedCategory);
            for (UserRanking ranking : userRankings
            ) {
                try {
                    res += Double.parseDouble(ranking.eco);
                } catch (Exception e) {
                    Log.d(TAG, "getTrashCount: ", e);
                }
            }
        }
        return res;
    }

    private boolean isEcoChallenge() {
        return _company != null && _company.eco_challenge;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
