package de.fuenfzig670.sample.activities.fragments.adapters

import android.content.Context
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.LeistungsKlasseExpandCallback
import de.fuenfzig670.sample.model.UserRanking

enum class RankingTendency(val value: String) {
    up("up"),
    down("down"),
    nochange("nochange");
}

enum class RankingGender(val value: Int) {
    male(0),
    female(1),
    diverse(2);
}

data class ChallengeHeader(val ranking: UserRanking, val type: Int)
data class ChallengeData(var type: Int = ItemTypes.PARENT_LEISTUNGKLASSEN, var isExpanded: Boolean = false, val header: ChallengeHeader?, val childRankings: ArrayList<UserRanking>)

object ItemTypes {
    const val PARENT_LEISTUNGKLASSEN = 0
    const val PARENT_TEAM = 1
    const val CHILD = 2
}

class ChallengesAdapter(var mContext: Context, val items: MutableList<ChallengeData>, val isEcoChalenge: Boolean, val liestungKlasseCallback: LeistungsKlasseExpandCallback) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var holder: RecyclerView.ViewHolder? = null

        if(viewType == ItemTypes.PARENT_LEISTUNGKLASSEN) {
            val rowView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_leistung_klassen_header, parent,false)
            holder = LeistunkKlassenHeaderViewHolder(rowView)
        }
        else if (viewType == ItemTypes.PARENT_TEAM) {
            val rowView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_team_header, parent,false)
            holder = TeamHeaderViewHolder(rowView)
        }
        else {
            val rowView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_user_ranking, parent,false)
            holder = UserRankingChildViewHolder(rowView)
        }
        return holder!!
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = items[position]

        if (item.type == ItemTypes.PARENT_LEISTUNGKLASSEN) {
            holder as LeistunkKlassenHeaderViewHolder

            holder.apply {
                if (position == (items.size-1)) {
                    separatorView.visibility = View.GONE
                }
                else { separatorView.visibility = View.VISIBLE }

                val chunks = item.header!!.ranking.Nickname.split("(").toTypedArray()
                if (chunks.size == 2) {
                    headerTitleTextView.text = chunks[0]
                    headerSubtitleTextView.text = "(${chunks[1]}"
                    headerSubtitleTextView.visibility = View.VISIBLE
                }
                else {
                    headerTitleTextView.text = item.header!!.ranking.Nickname
                    headerSubtitleTextView.visibility = View.GONE
                    //headerSubtitleTextView.text = ""
                }

                parentLayout.setOnClickListener {
                    expandOrCollapseParentItem(item, position, true)
                }
                if (item.isExpanded)  {
                    rightArrowImageView.setImageResource(R.drawable.ic_arrow_up)
                }
                else {
                    rightArrowImageView.setImageResource(R.drawable.ic_arrow_down)
                }
                applyRoundCornersIfNeeded(position, holder.parentLayout)
            }
        }
        else if (item.type == ItemTypes.PARENT_TEAM) {
                holder as TeamHeaderViewHolder
                holder.apply {
                    if (position == (items.size-1)) { separatorView.visibility = View.GONE }
                    else { separatorView.visibility = View.VISIBLE }

                    nameTextView.text = item.header!!.ranking.Nickname
                    stepsTextView.text = item.header!!.ranking.Steps
                    rankingTextView.text = "${item.header!!.ranking.Ranking}."
                    parentLayout.setOnClickListener {
                        expandOrCollapseParentItem(item, position)
                    }
                    if (item.header!!.ranking.tendency == RankingTendency.up.value) {
                        holder.tendencyImageView.setImageResource(R.drawable.ic_progress_up)
                    }
                    else if (item.header!!.ranking.tendency == RankingTendency.up.value)  {
                        holder.tendencyImageView.setImageResource(R.drawable.ic_progress_down)
                    }
                    else if (item.header!!.ranking.tendency == RankingTendency.nochange.value) {
                        holder.tendencyImageView.setImageResource(R.drawable.ic_progress_same)
                    }

                    if (item.isExpanded)  {
                        rightArrowImageView.setImageResource(R.drawable.ic_arrow_up)
                    }
                    else {
                        rightArrowImageView.setImageResource(R.drawable.ic_arrow_down)
                    }
                    applyRoundCornersIfNeeded(position, holder.parentLayout)
                }
        }
        else {
            holder as UserRankingChildViewHolder

            if (position == items.size-1)  {
                holder.separatorView.visibility = View.GONE
            }
            else {
                holder.separatorView.visibility = View.VISIBLE
                if (items.size > (position + 1)) {
                    val nextItem = items[position + 1]
                    if (nextItem.type == ItemTypes.PARENT_LEISTUNGKLASSEN) {
                        //holder.separatorView.visibility = View.GONE
                    }
                }
            }

            if (item.header != null){
                if (item.header.type == ItemTypes.PARENT_LEISTUNGKLASSEN) {
                    holder.genderImageView.visibility = View.VISIBLE
                    holder.tendencyImageView.visibility = View.VISIBLE
                }
                else {
                    holder.genderImageView.visibility = View.GONE
                    holder.tendencyImageView.visibility = View.GONE
                }

                //Apply margin from the left so we indicate visually it's a sub item
                holder.rankingTextView.margin(left = 28F)
                holder.separatorView.margin(left = 28F)

                if (item.header.type == ItemTypes.PARENT_TEAM) {
                    holder.stepsTextView.setTextColor(ContextCompat.getColor(mContext, R.color.challenge_child_gray))
                    holder.nameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.challenge_child_gray))
                }
                else {
                    holder.stepsTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black))
                    holder.nameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black))
                }

                if (item.header.type == ItemTypes.PARENT_TEAM) {
                    holder.stepsTextView.margin(right = 46F)
                }
                else {
                    holder.stepsTextView.margin(right = 18F)
                }
            }
            else {

                //Regular 16dp margin for Gesamt challenge item
                holder.rankingTextView.margin(left = 16F)
                holder.separatorView.margin(left = 16F)

                holder.genderImageView.visibility = View.VISIBLE
                holder.tendencyImageView.visibility = View.VISIBLE

                // Put the default margin
                holder.stepsTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black))
                holder.nameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black))

                holder.rankingTextView.margin(left = 20F)
                holder.separatorView.margin(left = 20F)
                holder.stepsTextView.margin(right = 20F)
            }

            holder.apply {
                val userRanking = item.childRankings.first()
                nameTextView.text = userRanking.Nickname
                rankingTextView.text = "${userRanking.Ranking}."
                var ecoPart: String = ""

                if (isEcoChalenge && !userRanking.eco.isEmpty()) {
                    ecoPart = " ${userRanking.eco}l"
                }
                stepsTextView.text = "${userRanking.Steps}${ecoPart}"

                if (userRanking.tendency == RankingTendency.up.value) {
                    holder.tendencyImageView.setImageResource(R.drawable.ic_progress_up)
                }
                else if (userRanking.tendency == RankingTendency.down.value)  {
                    holder.tendencyImageView.setImageResource(R.drawable.ic_progress_down)
                }
                else if (userRanking.tendency == RankingTendency.nochange.value) {
                    holder.tendencyImageView.setImageResource(R.drawable.ic_progress_same)
                }

                if (userRanking.Gender == RankingGender.male.value)  {
                    holder.genderImageView.setImageResource(R.drawable.ic_gender_male)
                }
                else if (userRanking.Gender == RankingGender.female.value)  {
                    holder.genderImageView.setImageResource(R.drawable.ic_gender_female)
                }
                else if (userRanking.Gender == RankingGender.diverse.value)  {
                    holder.genderImageView.setImageResource(R.drawable.ic_gender_diverse)
                }
                applyRoundCornersIfNeeded(position, holder.parentLayout)

            }
        }
    }

    fun expandPerfomanceClass(perfomanceClassParentItem: ChallengeData?) {
        perfomanceClassParentItem?.let { perfomanceClassParentItem ->
            val position = items.indexOf(perfomanceClassParentItem)
            if (position != -1) {
                expandParentRow(position)
            }
        }
    }
    private fun expandOrCollapseParentItem(row: ChallengeData, position: Int, isLeistungKlasse: Boolean = false) {

        if (row.isExpanded) {
            if (isLeistungKlasse) { liestungKlasseCallback.onCollapsed(row) }
            collapseParentRow(position)
        } else {
            if (isLeistungKlasse) { liestungKlasseCallback.onExpanded(row) }
            expandParentRow(position)
        }
    }

    private fun expandParentRow(position: Int){
        val item = items[position]
        val childRankings = item.childRankings
        item.isExpanded = true
        var nextPosition = position
        if(item.type == ItemTypes.PARENT_LEISTUNGKLASSEN || item.type == ItemTypes.PARENT_TEAM){
            childRankings.forEach { ranking ->
                val parentModel = ChallengeData(ItemTypes.CHILD, false, item.header, ArrayList(listOf(ranking)))
                items.add(++nextPosition, parentModel)
            }
            notifyDataSetChanged()
        }
    }

    private fun collapseParentRow(position: Int){
        val item = items[position]
        val childRankings = item.childRankings

        items[position].isExpanded = false

        if(items[position].type == ItemTypes.PARENT_LEISTUNGKLASSEN || items[position].type == ItemTypes.PARENT_TEAM) {
            childRankings.forEach { _ ->
                items.removeAt(position + 1)
            }
            notifyDataSetChanged()
        }
    }

    private fun applyRoundCornersIfNeeded(position:Int, parentLayout: RelativeLayout)  {
        if (position == 0) {
            parentLayout.background = ContextCompat.getDrawable(mContext, R.drawable.challenge_top_round_corners)
        }
        else if (position == items.size-1) {
            parentLayout.background = ContextCompat.getDrawable(mContext, R.drawable.challenge_bottom_round_corners)
        }
        else {
            parentLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.bottom_menu_item_bg))
        }
    }

    override fun getItemViewType(position: Int): Int = items[position].type

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    class LeistunkKlassenHeaderViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        val parentLayout = row.findViewById<RelativeLayout>(R.id.parentLayout)
        val headerTitleTextView  = row.findViewById<TextView>(R.id.headerTitleTextView)
        val headerSubtitleTextView  = row.findViewById<TextView>(R.id.headerSubtitleTextView)
        val rightArrowImageView =  row.findViewById<ImageView>(R.id.rightArrowImageView)
        val separatorView =  row.findViewById<View>(R.id.separatorView)
    }

    class TeamHeaderViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        val parentLayout = row.findViewById<RelativeLayout>(R.id.parentLayout)
        val rankingTextView = row.findViewById<TextView>(R.id.numberTextView)
        val nameTextView  = row.findViewById<TextView>(R.id.nameTextView)
        val stepsTextView  = row.findViewById<TextView>(R.id.stepsTextView)
        val tendencyImageView = row.findViewById<ImageView>(R.id.tendencyImageView)
        val rightArrowImageView =  row.findViewById<ImageView>(R.id.rightArrowImageView)
        val separatorView = row.findViewById<View>(R.id.separatorView)
    }


    class UserRankingChildViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        val parentLayout = row.findViewById<RelativeLayout>(R.id.parentLayout)
        val nameTextView = row.findViewById<TextView>(R.id.nameTextView)
        val stepsTextView = row.findViewById<TextView>(R.id.stepsTextView)
        val rankingTextView = row.findViewById<TextView>(R.id.numberTextView)
        val tendencyImageView = row.findViewById<ImageView>(R.id.tendencyImageView)
        val genderImageView = row.findViewById<ImageView>(R.id.genderImageView)
        val separatorView = row.findViewById<View>(R.id.separatorView)
    }
}

/* Some extensions to conveniently set margin*/
fun View.margin(left: Float? = null, top: Float? = null, right: Float? = null, bottom: Float? = null) {
    layoutParams<ViewGroup.MarginLayoutParams> {
        left?.run { leftMargin = dpToPx(this) }
        top?.run { topMargin = dpToPx(this) }
        right?.run { rightMargin = dpToPx(this) }
        bottom?.run { bottomMargin = dpToPx(this) }
    }
}

inline fun <reified T : ViewGroup.LayoutParams> View.layoutParams(block: T.() -> Unit) {
    if (layoutParams is T) block(layoutParams as T)
}

fun View.dpToPx(dp: Float): Int = context.dpToPx(dp)
fun Context.dpToPx(dp: Float): Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()


