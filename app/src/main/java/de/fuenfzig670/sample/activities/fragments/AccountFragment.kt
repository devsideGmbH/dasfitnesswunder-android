package de.fuenfzig670.sample.activities.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.FragmentManager
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.BaseFragment
import de.fuenfzig670.sample.activities.MainActivity
import de.fuenfzig670.sample.activities.composables.GreenButton
import de.fuenfzig670.sample.activities.composables.Headline24
import de.fuenfzig670.sample.activities.composables.ParagraphRegular
import de.fuenfzig670.sample.activities.composables.SeperatorLine
import de.fuenfzig670.sample.model.Company

class AccountFragment: BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance(company: Company) = AccountFragment().apply { _company = company }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                AccountScreen({
                    (activity as? MainActivity)?.showAccountLogoutonfirmationFragment()
                }){
                    (activity as? MainActivity)?.showAccountDeleteConfirmationFragment()
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().supportFragmentManager.addOnBackStackChangedListener(backStackListener)
    }

    override fun onDetach() {
        super.onDetach()

        try {
            requireActivity().supportFragmentManager.removeOnBackStackChangedListener(backStackListener)
        }
        catch (e: Exception) {
            backStackListener
        }
    }


    override fun onResume() {
        super.onResume()
        (activity as? MainActivity)?.setProfileGrayColor()

        updateActionBarTitle()

        val parentCoordinatorLayout = requireActivity().findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
        parentCoordinatorLayout.fitsSystemWindows = true
    }

    val backStackListener = object: FragmentManager.OnBackStackChangedListener {
        override fun onBackStackChanged() {
            val index = requireActivity().supportFragmentManager.backStackEntryCount - 1
            if  (index >= 0) {
                val entry = requireActivity().supportFragmentManager.getBackStackEntryAt(index)
                if (entry.name == ChallengeDetailsFragment.STACK_ID) {
                    updateActionBarTitle()
                }
            }

        }
    }

    private fun updateActionBarTitle() {
        val actionBar = (requireActivity() as? AppCompatActivity)?.supportActionBar
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.title = requireActivity().getString(R.string.account)
    }

    @Preview(showSystemUi = true, showBackground = true)
    @Composable
    fun AccountScreenPreview(){
        AccountScreen({}) {}
    }

    @Composable
    fun AccountScreen(onLogout: () -> Unit, onDeleteAccount: () -> Unit) {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                Headline24(
                    text = "Ausloggen",
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(24.dp))
                GreenButton(text = "Jetzt ausloggen", onClick = onLogout)
                Spacer(modifier = Modifier.height(40.dp))
                SeperatorLine()
                Spacer(modifier = Modifier.height(32.dp))
                Headline24(
                    text = "Account löschen",
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                ParagraphRegular(
                    text = "Wenn du deinen Account löschst, kannst du Ihn anschließend nicht wiederherstellen. Alle gespeicherten Daten werden unwiederruflich gelöscht.",
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(24.dp))
                GreenButton(text = "Account löschen", onClick = onDeleteAccount)
            }
        }
    }
}
