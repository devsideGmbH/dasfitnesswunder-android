package de.fuenfzig670.sample.activities.ui.profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.activities.BaseFragment;
import de.fuenfzig670.sample.activities.MainActivity;
import de.fuenfzig670.sample.dataservice.GeneralService;
import de.fuenfzig670.sample.helpers.GeneralHelper;
import de.fuenfzig670.sample.helpers.NoConnectionHelper;
import de.fuenfzig670.sample.helpers.SessionManager;
import de.fuenfzig670.sample.model.LoginRequestResponse;
import de.fuenfzig670.sample.model.UserModel;

public class ProfileFragment extends BaseFragment {

    public static String STACK_ID = "ProfileFragmentStackId";

    private GeneralService generalService;
    private Request request;

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity()instanceof MainActivity) ((MainActivity) getActivity()).setProfileGreenColor();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        updateActionBarTitle();
        requireActivity().getSupportFragmentManager().addOnBackStackChangedListener(backStackListener);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            request.cancel();
            requireActivity().getSupportFragmentManager().removeOnBackStackChangedListener(backStackListener);
        }
        catch (Exception e) {
            Log.v("Fitness", e.getLocalizedMessage());
        }
    }

    FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            int index = requireActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
            if  (index >= 0)  {
                FragmentManager.BackStackEntry entry = requireActivity().getSupportFragmentManager().getBackStackEntryAt(index);
                if (Objects.equals(entry.getName(), STACK_ID)) {
                    updateActionBarTitle();
                }
            }

        }
    };

    private void updateActionBarTitle() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle(R.string.myProfile);

        CoordinatorLayout parentCoordinatorLayout = requireActivity().findViewById(R.id.parentCoordinatorLayout);
        parentCoordinatorLayout.setFitsSystemWindows(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_profile, container, false);
        setUpViews();

        return root;
    }

    private View progressBar_cyclic;
    private WebView _webView;
    private final String _script1 =
        "mainContent = document.getElementsByClassName('maincontent')[0];" +
            "mainContentElements = [].slice.call(mainContent.getElementsByTagName('*'));" +
            "allElements = [].slice.call(document.getElementsByTagName('*'));" +
            "elementsToHide = allElements.filter(" +
            "    function(x){" +
            "        return !mainContentElements.includes(x);" +
            "    }" +
            ");" +
            "for(i = 0; i < elementsToHide.length; i++){" +
            "    elementsToHide[i].style.display='none';" +
            "    elementsToHide[i].style.margin='0px';" +
            "    elementsToHide[i].style.padding='0px';" +
            "};" +
            "function showThisAndParent(element) {" +
            "    element.style.display='';" +
            "    if (element.parentElement != null) showThisAndParent(element.parentElement);" +
            "};" +
            "showThisAndParent(mainContent);";
    private final String _script2 = "function hideTag(tagName) { Array.prototype.slice.call(document.getElementsByTagName(tagName)).forEach( x => { x.style.display = 'none'; x.style.margin = '0px'; x.style.padding = '0px'; });}" +
        "hideTag('header');hideTag('footer');";
//        "function descendAndHide(x) {" +
//            "    if (x != null && x.children != null)" +
//            "        for (var i = 0; i < x.children.length; i++){" +
//            "            if(!(x.children[i].className.includes('maincontent')))" +
//            "                descendAndHide(x.children[i]);" +
//            "        }" +
//            "    x.style.display = 'none';" +
//            "    x.style.margin = '0px';" +
//            "    x.style.padding = '0px';" +
//            "}" +
//            "descendAndHide(document.getElementsByClassName('profile')[0]);" +
//            "document.getElementsByClassName('container')[1].setAttribute('style','width:100% !important');" +
//            "document.getElementsByClassName('container')[1].setAttribute('style','max-width:100% !important');" +
//            "" +
//            "function showThisAndParent(element) {" +
//            "    if (element.parentElement != null) " +
//            "        showThisAndParent(element.parentElement);" +
//            "    element.style.display='';" +
//            "}" +
//            "showThisAndParent(document.getElementsByClassName('maincontent')[0]);";

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            if (_webView != null) _webView.scrollTo(0, 0);
            timerHandler.postDelayed(this, 500);
        }
    };


    @SuppressLint("SetJavaScriptEnabled")
    protected void setUpViews() {
        Intent i = getIntent();
        if (i == null) return;

        if (i.hasExtra("hasUserLogin")) _hasUserLogin = i.getBooleanExtra("hasUserLogin", false);
        if (i.hasExtra("company")) {
            setupHeader();
        }
        _webView = (WebView) findViewById(R.id.webview_profile);
        progressBar_cyclic = findViewById(R.id.progressBar_cyclic);

        Context context = getContext();
        if (context == null) return;

        // Clear all the Application Cache, Web SQL Database and the HTML5 Web Storage
        WebStorage.getInstance().deleteAllData();

        // Clear all the cookies
        CookieManager.getInstance().removeAllCookies(null);
        CookieManager.getInstance().flush();

        _webView.clearCache(true);
        _webView.clearFormData();
        _webView.clearHistory();
        _webView.clearSslPreferences();

        WebSettings webSettings = _webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        _webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                _webView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar_cyclic.setVisibility(View.GONE);
                _webView.setVisibility(View.VISIBLE);
                _webView.loadUrl("javascript:(function() { " +
                    _script2 +
                    "})()");
                //TODO: find better solution
                timerHandler.postDelayed(timerRunnable, 0);
            }
        });

        UserModel userModel = GeneralHelper.GetUser(context);
        if (userModel != null) {
            generalService = new GeneralService(context);
            request = generalService.LoginUser(userModel, response -> {
                if (response == null) {
                    progressBar_cyclic.setVisibility(View.GONE);
                    NoConnectionHelper.showNoConnectionToast(context);
                } else if (response instanceof LoginRequestResponse) {
                    progressBar_cyclic.setVisibility(View.GONE);
                    GeneralHelper.showDialog(requireContext(), "Fehler", "Es ist ein Fehler aufgetretten: \n\n" + ((LoginRequestResponse) response).error);
                } else if (response instanceof String) {
                    String token = response.toString();
                    SessionManager.getInstance().token = token;

                    userModel.token = token;
                    GeneralHelper.SaveUser(context, userModel);

                    Map<String, String> map = new HashMap<>();
                    map.put("Authorization", "Bearer " + _sessionManager.token);

                    String url = getResources().getString(R.string.BASEURL) + _company.ProfilUrl;
                    _webView.loadUrl(url, map);
                }
            });
        }
    }
}
