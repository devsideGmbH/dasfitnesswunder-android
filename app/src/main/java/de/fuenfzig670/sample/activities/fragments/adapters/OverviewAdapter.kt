package de.fuenfzig670.sample.activities.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.OverviewClickCallback
import de.fuenfzig670.sample.model.Company
import org.joda.time.DateTime

enum class OverViewItemType {
    teamSuccess,
    start,
    end,
    plogging;
}

class OverviewAdapter(val company: Company, val context: Context,  val callback: OverviewClickCallback) : RecyclerView.Adapter<AchievementViewHolder>() {

    var items = ArrayList<OverViewItemType>()
    override fun getItemCount(): Int {
        if (company.eco_challenge) { items = ArrayList(listOf<OverViewItemType>(OverViewItemType.teamSuccess, OverViewItemType.start, OverViewItemType.end, OverViewItemType.plogging)) }
        else { items = ArrayList(listOf<OverViewItemType>(OverViewItemType.teamSuccess, OverViewItemType.start, OverViewItemType.end)) }

        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AchievementViewHolder {
        return AchievementViewHolder(LayoutInflater.from(context).inflate(R.layout.item_overview, parent, false))
    }

    override fun onBindViewHolder(holder: AchievementViewHolder, position: Int) {
        val item = items[position]

        when (item) {
            OverViewItemType.start -> setupStartItem(holder)
            OverViewItemType.end -> setupEndItem(holder)
            OverViewItemType.teamSuccess -> setupTeamSuccessItem(holder)
            OverViewItemType.plogging -> setupPloggingItem(holder)
        }
    }

    private fun setupStartItem(holder: AchievementViewHolder){
        holder.topImageView.setImageResource(R.drawable.ic_overview_start)

        val start = DateTime(company.Challenges[0].Start)
        val entryStart = DateTime(company.Challenges[0].EntryStart)
        val finish = DateTime(company.Challenges[0].Finish)

        val dateFormat = "dd.MM.yyyy"
        val now = DateTime.now()

        val entryStarted = entryStart.isBefore(now);
        val isRunning = now.isAfter(start) && now.isBefore(finish);
        val stopped = now.isAfter(finish);

        holder.titleTextView.text = "Die Anmeldung startet am"
        holder.infoTextView.text = entryStart.toString(dateFormat)

        if (entryStarted) {
            holder.titleTextView.text = "Die Anmeldung läuft noch"
            holder.infoTextView.text = getCounterStringWithTime(start, now)
        }
        if (isRunning) {
            holder.titleTextView.text = "Challenge Startschuss"
            holder.infoTextView.text = start.toString(dateFormat)
        }
        if (stopped) {
            holder.titleTextView.text = "Challenge Startschuss"
            holder.infoTextView.text = start.toString(dateFormat)

        }
    }

    private fun setupEndItem(holder: AchievementViewHolder){
        holder.topImageView.setImageResource(R.drawable.ic_overview_end)

        val start = DateTime(company.Challenges[0].Start)
        val entryStart = DateTime(company.Challenges[0].EntryStart)
        val finish = DateTime(company.Challenges[0].Finish)

        val dateFormat = "dd.MM.yyyy"
        val now = DateTime.now()

        val entryStarted = entryStart.isBefore(now);
        val isRunning = now.isAfter(start) && now.isBefore(finish);
        val stopped = now.isAfter(finish);

        holder.titleTextView.text = "Die Challenge startet am"
        holder.infoTextView.text = start.toString(dateFormat)

        if (entryStarted) {
            holder.titleTextView.text = "Die Challenge startet am"
            holder.infoTextView.text = start.toString(dateFormat)
        }
        if (isRunning) {
            holder.titleTextView.text = "Die Challenge läuft noch"
            holder.infoTextView.text = getCounterStringWithTime(finish, now)
        }
        if (stopped) {
            holder.titleTextView.text = "Challenge Startschuss"
            holder.infoTextView.text = finish.toString(dateFormat)
        }
    }

    private fun setupTeamSuccessItem(holder: AchievementViewHolder){
        holder.topImageView.setImageResource(R.drawable.ic_overview_medal)

        holder.titleTextView.text = "Teamerfolg"
        holder.infoTextView.text = context.getString(R.string.sum_header, company.sum_steps, company.sum_kilometers)
    }

    private fun setupPloggingItem(holder: AchievementViewHolder){
        holder.topImageView.setImageResource(R.drawable.ic_overview_trash)

        holder.titleTextView.text = "Gesammelter Abfall"
        holder.infoTextView.text = "${getTrashCount()} Liter"
    }

    private fun getTrashCount(): Double {
        return  company.sum_ecos
    }

    private fun getCounterStringWithTime(end: DateTime, start: DateTime): String {
        val endDate = end.toDate()
        val startDate = start.toDate()

        var different = endDate.time - startDate.time
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24
        val elapsedDays = different / daysInMilli
        different = different % daysInMilli
        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli
        val elapsedMinutes = different / minutesInMilli
        return context.getResources().getString(R.string.counter_string, elapsedDays.toString(), elapsedHours.toString(), elapsedMinutes.toString())
    }

}

class AchievementViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val topImageView = view.findViewById<ImageView>(R.id.topImageView)
    val titleTextView = view.findViewById<TextView>(R.id.titleTextView)
    val infoTextView = view.findViewById<TextView>(R.id.infoTextView)

}
