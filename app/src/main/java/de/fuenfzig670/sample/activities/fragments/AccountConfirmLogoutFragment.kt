package de.fuenfzig670.sample.activities.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.BaseFragment
import de.fuenfzig670.sample.activities.MessageView
import de.fuenfzig670.sample.activities.composables.GreenBorderButton
import de.fuenfzig670.sample.activities.composables.GreenButton
import de.fuenfzig670.sample.activities.composables.Headline24
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.model.Company

class AccountConfirmLogoutFragment: BaseFragment() {
    companion object {
        @JvmStatic
        fun newInstance(company: Company) = AccountConfirmLogoutFragment().apply { _company = company }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                AccountConfirmLogoutScreen({
                    activity?.onBackPressedDispatcher?.onBackPressed()
                }) {
                    GeneralHelper.setUserLoggedOut(requireContext(), true)
                    GeneralHelper.goToPinCode(requireContext())
                }
            }
        }
    }

    @Preview(showSystemUi = true, showBackground = true)
    @Composable
    fun AccountConfirmLogoutScreenPreview(){
        AccountConfirmLogoutScreen({}) {}
    }

    @Composable
    fun AccountConfirmLogoutScreen(onCancel: () -> Unit, onLogout: () -> Unit) {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, bottom = 20.dp)
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                Headline24(
                    text = "Aus meinem Profil ausloggen",
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(24.dp))
                MessageView(
                    title = "Hinweis",
                    message = "Bist du sicher, dass du dich aus deinem Account ausloggen möchtest?",
                    backgroundColor = colorResource(id = R.color.error)
                )
            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                GreenButton(text = "Ja, ausloggen", onClick = onLogout)
                Spacer(Modifier.height(16.dp))
                GreenBorderButton(text = "abbrechen", onClick = onCancel)
                Spacer(Modifier.height(136.dp))
            }
        }
    }
}
