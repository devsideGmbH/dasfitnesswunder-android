package de.fuenfzig670.sample.activities.composables

import androidx.annotation.DrawableRes
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.ThryveSource


@Preview(showSystemUi = true, showBackground = true)
@Composable
fun RegistryStep1Preview() {
    RegistryStep1(RegistryInputs()) {}
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun RegistryStep2Preview() {
    RegistryStep2(RegistryInputs()) {}
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun RegistryStep3Preview() {
    RegistryStep3(RegistryInputs()) {}
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun RegistryStep4Preview() {
    RegistryStep4(RegistryInputs()) {}
}

@Composable
fun RegistryStep1(registryInputs: RegistryInputs, company: Company? = null, onClick: () -> Unit) {
    RegistryFrame(
        0.25f, "1 / 4",
        "Deine persönlichen Daten", "Nächster Schritt: Weiterführende Infos",
        "Bitte gib deinen Namen ein",
        registryInputs.showLoadingIndicator,
        buttonClick = onClick
    ) {
        CustomInput(inputWrapper = registryInputs.firstname)
        CustomInput(inputWrapper = registryInputs.lastname)
        DropdownList(arrayListOf("Männlich", "Weiblich", "Divers"), inputWrapper = registryInputs.gender)
    }
}

@Composable
fun RegistryStep2(registryInputs: RegistryInputs, onClick: () -> Unit) {
    RegistryFrame(
        0.5f, "2 / 4",
        "Weiterführende Infos", "Nächster Schritt: Passwort",
        "Bitte gib weiterführende Infos ein",
        registryInputs.showLoadingIndicator,
        buttonClick = onClick
    ) {
        CustomInput(inputWrapper = registryInputs.nickname)
        CustomInput(inputWrapper = registryInputs.email)
        CustomInput(inputWrapper = registryInputs.email_confirmation)
    }
}

@Composable
fun RegistryStep3(registryInputs: RegistryInputs, onClick: () -> Unit) {
    RegistryFrame(
        0.75f, "3 / 4",
        "Passwort festlegen", "Nächster Schritt: Sonstige Informationen",
        "Lege nun dein Passwort fest",
        registryInputs.showLoadingIndicator,
        buttonClick = onClick
    ) {
        CustomInput(inputWrapper = registryInputs.password, isPasswordInput = true)
        CustomInput(inputWrapper = registryInputs.password_confirmation, isPasswordInput = true)
    }
}

@Composable
fun RegistryStep4(registryInputs: RegistryInputs, company: Company? = null, onClick: () -> Unit) {
    RegistryFrame(
        1f, "4 / 4",
        "Sonstige Informationen", "",
        "Bitte auswählen",
        showLoadingIndicator = registryInputs.showLoadingIndicator,
        buttonLabel = "Zur Datenfreigabe",
        buttonClick = onClick, bottomContent = {
        }
    ) {
        if (registryInputs.dropDowns.isEmpty()) {
            company?.registration?.dropdowns?.let { dropdowns ->
                dropdowns.forEach { dropdown ->
                    registryInputs.dropDowns.add(InputWrapper(dropdown.dropdowntitle, "Bitte auswählen", dropDown = dropdown))
                }
            }
        }
        registryInputs.dropDowns.forEach { wrapper ->
            wrapper.dropDown?.dropdownvalues?.let { downValues ->
                DropdownList(
                    items = downValues.mapTo(arrayListOf<String>()) { it.dropdowvaluentitle },
                    inputWrapper = wrapper
                )
            }
        }
        company?.registration?.api_source = company?.registration?.api_source?.filter { it.display_name != "Apple" } as ArrayList<ThryveSource>?
        company?.registration?.api_source?.let { thryveSources ->
            DropdownList(thryveSources.mapTo(arrayListOf<String>()) { it.display_name }, inputWrapper = registryInputs.tracker)
        }
        CustomSwitchInput(registryInputs.trackerCompliance)
        CustomSwitchInput(registryInputs.privacyPolicy)
    }
}

@Composable
fun RegistryFrame(
    progress: Float,
    step: String,
    headerTitle: String,
    headerSubTitle: String,
    contentTitle: String,
    showLoadingIndicator: MutableState<Boolean>,
    showFootNote: Boolean = false,

    @DrawableRes circleImage: Int? = null,

    buttonLabel: String = "Nächster Schritt",
    buttonClick: () -> Unit,
    bottomContent: @Composable () -> Unit = {},
    content: @Composable () -> Unit
) {
    val rememberScrollState = rememberScrollState()
    Column(
        Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp)
    ) {
        Column {
            Spacer(modifier = Modifier.height(24.dp))
            Row(Modifier.height(56.dp)) {
                Box(contentAlignment = Alignment.Center) {
                    CircularProgressIndicator(
                        progress = 1f,
                        Modifier.size(56.dp),
                        color = colorResource(id = R.color.light_grey), strokeWidth = 4.dp,
                    )
                    CircularProgressIndicator(
                        progress = progress,
                        Modifier.size(56.dp),
                        color = colorResource(id = R.color.light_green), strokeWidth = 4.dp,
                    )
                    if (circleImage != null) {
                        Image(painter = painterResource(id = circleImage), contentDescription = null)
                    } else {
                        RegistryCircleText(step)
                    }
                }
                Spacer(modifier = Modifier.width(22.dp))
                Column(
                    Modifier
                        .weight(1f)
                        .fillMaxHeight(), horizontalAlignment = Alignment.Start, verticalArrangement = Arrangement.Center
                ) {
                    RegistryHeader(text = headerTitle)
                    if (headerSubTitle.isNotBlank()) {
                        Spacer(modifier = Modifier.height(4.dp))
                        RegistrySubHeader(text = headerSubTitle)
                    }
                }
            }
            Spacer(modifier = Modifier.height(28.dp))
            SeperatorLine()
            if (showLoadingIndicator.value) {
                DefaultLinearProgressIndicator()
            }
        }
        Column(
            Modifier
                .weight(1f)
                .verticalFadingEdge(rememberScrollState, 48.dp, Color.White, topEdge = false)
        ) {
            Column(
                Modifier
                    .weight(1f)
                    .verticalScroll(rememberScrollState)
            ) {
                Spacer(modifier = Modifier.height(26.dp))
                if (contentTitle.isNotBlank()) {
                    RegistryHeader(text = contentTitle)
                    Spacer(modifier = Modifier.height(24.dp))
                }
                content()
                if (showFootNote) {
                    ParagraphRegular(text = "*Pflichtfelder", textAlign = TextAlign.Start)
                }
                Spacer(modifier = Modifier.height(48.dp))
            }
        }
        Column {
            bottomContent()
            GreenButton(text = buttonLabel) { buttonClick() }
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
fun DefaultLinearProgressIndicator() {
    LinearProgressIndicator(color = colorResource(id = R.color.light_green), modifier = Modifier.fillMaxWidth())
}

@Composable
fun CustomSwitchInput(inputWrapper: InputWrapperSwitch) {
    Column {
        Row(verticalAlignment = Alignment.Top) {
            Column {
                Spacer(modifier = Modifier.height(4.dp))
                CustomSwitch(checked = inputWrapper.inputValue)
            }
            Spacer(modifier = Modifier.width(16.dp))
            ParagraphRegularSmall(text = inputWrapper.label, textAlign = TextAlign.Start)
        }
        val hasError = inputWrapper.inputError.value.isNotBlank()
        if (hasError) {
            ErrorComposable(inputWrapper.inputError)
        }
        Spacer(modifier = Modifier.height(16.dp))
    }
}

@Composable
fun CustomSwitch(
    checked: MutableState<Boolean>,
    scale: Float = 1f,
    width: Dp = 44.dp,
    height: Dp = 24.dp,
    checkedTrackColor: Color = colorResource(id = R.color.light_green),
    uncheckedTrackColor: Color = colorResource(id = R.color.medium_grey),
    thumbColor: Color = Color.White,
    gapBetweenThumbAndTrackEdge: Dp = 2.dp
) {
    val thumbRadius = (height / 2) - gapBetweenThumbAndTrackEdge

    // To move the thumb, we need to calculate the position (along x axis)
    val animatePosition = animateFloatAsState(
        targetValue = if (checked.value)
            with(LocalDensity.current) { (width - thumbRadius - gapBetweenThumbAndTrackEdge).toPx() }
        else
            with(LocalDensity.current) { (thumbRadius + gapBetweenThumbAndTrackEdge).toPx() }
    )

    Canvas(
        modifier = Modifier
            .size(width = width, height = height)
            .scale(scale = scale)
            .pointerInput(Unit) {
                detectTapGestures(
                    onTap = {
                        checked.value = !checked.value
                    }
                )
            }
    ) {
        drawRoundRect(
            color = if (checked.value) checkedTrackColor else uncheckedTrackColor,
            cornerRadius = CornerRadius(x = 10.dp.toPx(), y = 10.dp.toPx())
        )
        drawCircle(
            color = thumbColor,
            radius = thumbRadius.toPx(),
            center = Offset(
                x = animatePosition.value,
                y = size.height / 2
            )
        )
    }
}
