package de.fuenfzig670.sample.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.*
import de.fuenfzig670.sample.dataservice.CompanyDataService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.SessionManager
import de.fuenfzig670.sample.model.Company

class PinCodeResponseActivity : BaseActivity() {
    companion object {
        const val PINCODE_SUCCESS = "PINCODE_SUCCESS"
        const val PINCODE_TITLE = "PINCODE_TITLE"
        const val PINCODE_MESSAGE = "PINCODE_MESSAGE"
        const val PINCODE_COMPANY_ID = "PINCODE_COMPANY_ID"
        const val PINCODE_PIN = "PINCODE_PIN"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val success = intent.getBooleanExtra(PINCODE_SUCCESS, false)
        val title = intent.getStringExtra(PINCODE_TITLE) ?: ""
        val message = intent.getStringExtra(PINCODE_MESSAGE) ?: ""

        val challengeActive = SessionManager.getInstance().company?.isRegistrationActive == true

        val companyId = intent.getIntExtra(PINCODE_COMPANY_ID, 0)
        val pin = intent.getStringExtra(PINCODE_PIN) ?: ""
        setContent {
            PinCodeResponseScreen(
                success, title, message, challengeActive,
                { goToLogin(companyId)
                }, { goToRegistration(companyId) }
            ) { onBackPressed() }
        }
    }

    private fun goToLogin(companyId: Int) {
        CompanyDataService(this).GetSpecific(companyId) {
            if (it is Company) {
                SessionManager.getInstance().company = it
                startActivity(
                    Intent(this, LoginActivity::class.java).putExtra(COMPANY, it)
                )
            }
        }
    }

    private fun goToMainScreen() {
        val companyId = intent.getIntExtra(PINCODE_COMPANY_ID, 0)
        CompanyDataService(this).GetSpecific(companyId) {
            if (it is Company) {
                val intent = Intent(this, MainActivity::class.java).putExtra(COMPANY, it)
                startActivity(intent)
            }
        }
    }

    private fun goToRegistration(companyId: Int) {
        CompanyDataService(this).GetSpecific(companyId) {
            if (it is Company) {
                SessionManager.getInstance().company = it
                startActivity(Intent(this, RegistryStepsActivity::class.java).putExtra(COMPANY, it))
            }
        }
    }

    @Preview
    @Composable
    fun PinCodeResponseScreenPreviewSuccess() {
        PinCodeResponseScreen(true, "Vielen Dank", "Dein Zugangscode ist korrekt!", true, {}, {}) { onBackPressed() }
    }

    @Preview
    @Composable
    fun PinCodeResponseScreenPreviewError() {
        PinCodeResponseScreen(
            false,
            "Oops...",
            "Der eingegebene Code ist nicht korrekt.", true,
            {},
            {}) { onBackPressed() }
    }

    @Composable
    fun PinCodeResponseScreen(
        success: Boolean,
        title: String,
        message: String,
        challengeActive: Boolean,
        loginCallback: () -> Unit,
        registryCallback: () -> Unit,
        goBackCallback: () -> Unit
    ) {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.heightIn(8.dp, 70.dp))
                Headline1(text = title)
                Spacer(modifier = Modifier.height(16.dp))
                ParagraphRegular(text = message)
                Spacer(modifier = Modifier.height(40.dp))
                ImageView(imageId = if (success) R.drawable.pincode_success_image else R.drawable.pincode_error_image)
                Spacer(modifier = Modifier.height(64.dp))
                val successMessage = "${if(GeneralHelper.getUserLoggedOut(LocalContext.current) == false) "Du kannst dich jetzt registrieren.\n\n" else ""}Falls Du für diese Challenge bereits registriert bist, kannst du dich einfach einloggen."
                MessageView(
                    "Hinweis",
                    if (success) successMessage
                    else "Überprüfe ob du deinen Zugangscode korrekt eingegeben hast oder wende dich bitte an den Service.",
                    if (success) colorResource(id = R.color.mediumGrey) else colorResource(id = R.color.error),
                    Modifier.padding(horizontal = 20.dp)
                )
                Spacer(modifier = Modifier.height(16.dp))
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                if (success) {
                    GreenButton(text = "Jetzt Registrieren", enabled = challengeActive && !GeneralHelper.getUserLoggedOut(this@PinCodeResponseActivity)) {
                        registryCallback()
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    GreenBorderButton(text = "Einloggen") {
                        loginCallback()
                    }
                } else {
                    GreenButton(text = "Zugangscode erneut eingeben") {
                        goBackCallback()
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}

@Preview
@Composable
fun ImageViewPreview1() {
    ImageView(R.drawable.pincode_success_image)
}

@Preview
@Composable
fun ImageViewPreview2() {
    ImageView(R.drawable.pincode_error_image)
}

@Composable
fun ImageView(@DrawableRes imageId: Int) {
    Image(painter = painterResource(id = imageId), contentDescription = null, modifier = Modifier.size(103.dp, 87.dp))
}

@Preview
@Composable
fun MessageViewPreview() {
    MessageView(title = "Hinweis", message = "Message", Color.Red)
}

@Composable
fun MessageView(title: String, message: String, backgroundColor: Color, modifier: Modifier = Modifier, annotatedMessage: AnnotatedString? = null) {
    Box(modifier = modifier.clip(RoundedCornerShape(8.dp))) {
        Box(Modifier.background(backgroundColor)) {
            Row(
                modifier = Modifier
                    .padding(1.dp)
                    .clip(RoundedCornerShape(8.dp))
            ) {
                Spacer(modifier = Modifier.width(7.dp))
                Box(
                    modifier = Modifier
                        .background(Color.White)
                        .weight(1f)
                ) {
                    Column(
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.Start,
                        modifier = Modifier.padding(24.dp, 16.dp, 24.dp, 24.dp)
                    ) {
                        Headline17BoldGrey(text = title, textAlign = TextAlign.Start)
                        Spacer(modifier = Modifier.height(8.dp))
                        if(annotatedMessage != null){
                            ParagraphRegularAnnotated(text = annotatedMessage, textAlign = TextAlign.Start)
                        } else {
                            ParagraphRegular(text = message, textAlign = TextAlign.Start)
                        }
                    }
                }
            }
        }
    }
}
