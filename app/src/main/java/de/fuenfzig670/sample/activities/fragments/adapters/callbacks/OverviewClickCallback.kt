package de.fuenfzig670.sample.activities.fragments.adapters.callbacks

import de.fuenfzig670.sample.activities.fragments.adapters.OverViewItemType

interface OverviewClickCallback {
    fun onItemClicked(type: OverViewItemType)
}
