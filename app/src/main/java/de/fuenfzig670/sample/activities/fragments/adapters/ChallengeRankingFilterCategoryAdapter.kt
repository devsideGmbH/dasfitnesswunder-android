package de.fuenfzig670.sample.activities.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.RankingCategoryClickCallback
import de.fuenfzig670.sample.model.RankingCategory

class ChallengeRankingFilterCategoryAdapter(val categories: ArrayList<RankingCategory>, var selectedCategory: RankingCategory, val context: Context,  val callback: RankingCategoryClickCallback) : RecyclerView.Adapter<RankingFilterCategoryViewHolder>() {

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankingFilterCategoryViewHolder {
        return RankingFilterCategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.item_challenge_filter_ranking_category, parent, false))
    }

    override fun onBindViewHolder(holder: RankingFilterCategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.textView.text = category.dropdowntitle

        val selectedIndex = categories.indexOf(selectedCategory)
        if  (position == selectedIndex)  {
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.cardView.setCardBackgroundColor(context.getColor(R.color.light_green))

            holder.cardView.strokeWidth = 0
        }
        else {
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.light_green))
            holder.cardView.setCardBackgroundColor(context.getColor(R.color.white))
            holder.cardView.strokeColor = (ContextCompat.getColor(context, R.color.light_green))
            holder.cardView.strokeWidth = holder.cardView.dpToPx(1F)
        }

        holder.cardView.setOnClickListener {
            callback.onRankingCategoryClicked(category, category.ranking_type == 1)

            // Gesamt challenge
            if (category.ranking_type == 0) {
                val oldIndex = categories.indexOf(selectedCategory)
                notifyItemChanged(oldIndex)
                selectedCategory = category

                val newIndex = categories.indexOf(selectedCategory)
                notifyItemChanged(newIndex)
            }
        }
    }
}

class RankingFilterCategoryViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val cardView = view.findViewById<MaterialCardView>(R.id.cardView)
    val textView = view.findViewById<TextView>(R.id.textView)
}
