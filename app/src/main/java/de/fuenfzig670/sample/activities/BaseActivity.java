package de.fuenfzig670.sample.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.helpers.SessionManager;
import de.fuenfzig670.sample.model.Company;

public class BaseActivity extends AppCompatActivity {
    public static final String COMPANY = "COMPANY";

    protected static final String TAG = BaseActivity.class.getName();

    protected Company _company = SessionManager.getInstance().company;
    protected SessionManager _sessionManager = SessionManager.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setBackgroundDrawable(null);
            supportActionBar.setDisplayShowTitleEnabled(true);
            supportActionBar.setHomeAsUpIndicator(R.drawable.arrow_back);
        }
    }

    protected void changeStatusBarContrastStyle(Window window, Boolean lightIcons) {
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        View decorView = window.getDecorView();
        if (lightIcons) {
            // Draw light icons on a dark background color
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() | ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            // Draw dark icons on a light background color
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    protected boolean ActivatedContainsKey(Company company, String key) {
        String[] activatedSplinter = company.registration.activated.split(", ");
        return Arrays.asList(activatedSplinter).contains(key);
    }
}
