package de.fuenfzig670.sample.activities.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.BaseFragment
import de.fuenfzig670.sample.activities.MessageView
import de.fuenfzig670.sample.activities.composables.GreenBorderButton
import de.fuenfzig670.sample.activities.composables.GreenButton
import de.fuenfzig670.sample.activities.composables.Headline24
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.LoginRequestResponse

class AccountConfirmDeletionFragment: BaseFragment() {
    companion object {
        @JvmStatic
        fun newInstance(company: Company) = AccountConfirmDeletionFragment().apply { _company = company }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                AccountConfirmDeletionScreen(onCancel = {
                    activity?.onBackPressedDispatcher?.onBackPressed()
                }) {
                    val context = requireContext()
                    val userModel = GeneralHelper.GetUser(requireContext())
                    GeneralService(activity).DeleteUser(userModel.companyId, userModel.id, true) { response ->
                        if (response is LoginRequestResponse || response == null) {
                            val fehler = AlertDialog.Builder(context)
                                .setTitle("Fehler")
                                .setPositiveButton(android.R.string.ok) { dialogInterface: DialogInterface?, _: Int -> dialogInterface?.dismiss() }
                                .setMessage("Account konnte nicht gelöscht werden.").create()
                            fehler.show()
                        } else {
                            GeneralHelper.goToAccountDeletionSuccess(requireContext())
                        }
                    }
                }
            }
        }
    }

    @Preview(showSystemUi = true, showBackground = true)
    @Composable
    fun AccountConfirmDeletionScreenPreview(){
        AccountConfirmDeletionScreen({}) {}
    }

    @Composable
    fun AccountConfirmDeletionScreen(onCancel: () -> Unit, onDeleteAccount: () -> Unit) {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, bottom = 20.dp)
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                Headline24(
                    text = "Meinen Account löschen",
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(24.dp))
                MessageView(
                    title = "Hinweis",
                    message = "Bist du sicher, dass du deinen Account endgültig löschen möchtest?",
                    backgroundColor = colorResource(id = R.color.error)
                )
            }

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                GreenButton(text = "Ja, meinen Account löschen", onClick = onDeleteAccount)
                Spacer(Modifier.height(16.dp))
                GreenBorderButton(text = "abbrechen", onClick = onCancel)
                Spacer(Modifier.height(136.dp))
            }
        }
    }
}
