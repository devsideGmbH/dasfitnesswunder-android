package de.fuenfzig670.sample.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.lifecycle.lifecycleScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.thryve.connector.module_gfit.GFitConnector
import com.thryve.connector.sdk.CoreConnector
import com.thryve.connector.shealth.SHealthConnector
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.*
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.dataservice.ValidationResponse
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.NoConnectionHelper
import de.fuenfzig670.sample.model.CreateUserRequest
import de.fuenfzig670.sample.model.CreateUserResponse
import de.fuenfzig670.sample.model.ThryveToken
import de.fuenfzig670.sample.webview.ThryveSdkActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegistryStepsActivity : BaseRegistryActivity() {
    companion object {
        const val SHEALTH_THRYVE_ID = 6
        const val GFIT_NATIVE_THRYVE_ID = 40

        const val DROPDOWN_NOT_CREATED = -2
    }

    private var _userId: Int? = null
    private var _thryveSourceId: Int? = null
    private var _trackerId: Int? = null
    private var sHealthIntegration = false

    private var connector: CoreConnector? = null
    private var gFitConnector: GFitConnector? = null
    private var sConnector: SHealthConnector? = null
    private var _startedThryveCall = false
    private var _success: Boolean? = null

    private val generalService = GeneralService(this)

    private fun getDropDownValues(registryInputs: RegistryInputs) : ArrayList<Int> {
        val res = arrayListOf<Int>()

        _company.registration.dropdowns.forEachIndexed { index, dropDown ->
            var value = DROPDOWN_NOT_CREATED
            try {
                value = dropDown.dropdownvalues[registryInputs.dropDowns[index].selectedIndex.value].id
            } catch (e: Exception) {
                Log.d(TAG, "getDropDownValues: could not get dropdown with index $index")
            }
            res.add(value)
        }

        return res
    }

    private fun setThryveToken(token: String) {
        if (_startedThryveCall) return
        _startedThryveCall = true
        if (_success == true) return
        generalService.SetThryveToken(_company.Id, _userId ?: -1, ThryveToken(token)) { x: Any ->
            val context: Context = this
            if (x is Boolean && x) {
                GeneralHelper.goToRegistrySuccess(this, _company)
                _success = true
            } else {
                GeneralHelper.showDialog(context, "Fehler", "Die Verbindung mit dem Server konnte nicht hergestellt werden. Wenden Sie sich an den Support.") {
                    deleteUser(this@RegistryStepsActivity)
                }
            }
            Log.d("SetThryveToken", "Response: $x")

            _startedThryveCall = false
        }
    }


    private fun enableGFitIntegration(token: String) {
        if (!checkTokenAndShowError(token, "Google Fit App")) return
        gFitConnector = GFitConnector(this@RegistryStepsActivity, GeneralHelper.G_FIT_DATA_TYPES)
        Log.d(TAG, "initGFit: start")
        if (gFitConnector?.isAvailable == true) {
            if(gFitConnector?.isActive() == true) {
                gFitConnector?.stop {
                    Log.d(TAG, "enableSHealthIntegration: stop: ${it.data} ${it.successful}")
                    initGFit(token)
                }
            } else {
                initGFit(token)
            }
        } else {
            GeneralHelper.showDialog(this, "Fehler", "Google Fit ist nicht verfügbar.")
        }
    }

    private fun initGFit(token: String) {
        Log.d(TAG, "enableGFitIntegration: isAvailable")
        gFitConnector?.start(GeneralHelper.G_FIT_DATA_TYPES) { response ->
            if (response.data == false || !response.successful) {
                GeneralHelper.showDialog(this@RegistryStepsActivity, "Warnung", "Google Fit ist nicht autorisiert.")
                deleteUser(this@RegistryStepsActivity)
            } else {
                setThryveToken(token = token)
                gFitConnector?.synchroniseEpoch(GeneralHelper.G_FIT_DATA_TYPES) {
                    Log.d(TAG, "enableGFitIntegration: synchroniseEpoch data: ${it.data} success: ${it.successful}")
                }
                gFitConnector?.synchroniseDaily(GeneralHelper.G_FIT_DATA_TYPES) {
                    Log.d(TAG, "enableGFitIntegration: synchroniseDaily data: ${it.data} success: ${it.successful}")
                }
            }
        }
    }

    private fun deleteUser(context: Context) {
        generalService.DeleteUser(_company.Id, _userId!!, false) { response: Any? -> GeneralHelper.HandleDeleteUser(context, response) }
    }

    private fun checkTokenAndShowError(token: String?, tracker: String): Boolean {
        if (token == null || token == "") {
            val context: Context = this
            GeneralHelper.showDialog(
                context, "Fehler",
                "Verbindung mit $tracker fehlgeschlagen. Wenden Sie sich an den Support."
            ) {
                deleteUser(this@RegistryStepsActivity)
            }

            return false
        }
        return true
    }


    private fun enableSHealthIntegration(token: String) {
        if (!checkTokenAndShowError(token, "Samsung Health")) return
        sConnector = SHealthConnector(context = this@RegistryStepsActivity, GeneralHelper.S_HEALTH_DATA_TYPES)
        if (sConnector?.isAvailable == true) {
            if (sConnector?.isActive() == true) {
                sConnector?.stop {
                    Log.d(TAG, "enableSHealthIntegration: stop: $it")
                    sConnector?.let { initSHealth(it, token) }
                }
            } else {
                sConnector?.let { initSHealth(it, token) }
            }
        } else {
            GeneralHelper.showDialog(this, "Warnung", "Samsung Health ist nicht verfügbar.")
            deleteUser(this@RegistryStepsActivity)
        }
    }

    private fun initSHealth(sConnector: SHealthConnector, token: String) {
        sConnector.authorize(this, GeneralHelper.S_HEALTH_DATA_TYPES, {granted ->
            Log.d(TAG, "initSHealth: authorize $granted")
            if (granted) {
                sConnector.start(GeneralHelper.S_HEALTH_DATA_TYPES) {
                    setThryveToken(token = token)
                    Log.d(TAG, "initSHealth: start data: ${it.data} success: ${it.successful}")
                    sConnector.synchroniseEpoch(GeneralHelper.S_HEALTH_DATA_TYPES) {
                        Log.d(TAG, "initSHealth: synchroniseEpoch data: ${it.data} success: ${it.successful}")
                    }
                    sConnector.synchroniseDaily(GeneralHelper.S_HEALTH_DATA_TYPES) {
                        Log.d(TAG, "initSHealth: synchroniseEpoch data: ${it.data} success: ${it.successful}")
                    }
                }
            } else {
                Log.d(TAG, "initSHealth: not authorized, stop")
                sConnector.stop {
                    Log.d(TAG, "initSHealth: stop data: ${it.data} success: ${it.successful}")
                }
                GeneralHelper.showDialog(this@RegistryStepsActivity, "Warnung", "Samsung Health ist nicht autorisiert.")
                deleteUser(this@RegistryStepsActivity)
            }
        }) {
            Log.d(TAG, "initSHealth: authorize exception", it)
            GeneralHelper.showDialog(this@RegistryStepsActivity, "Fehler", "Samsung Health Autorisierung ist fehlgeschlagen.")
            deleteUser(this@RegistryStepsActivity)
        }
    }

    private fun handleCreateUserResponse(responseObject: Any?, registryInputs: RegistryInputs) {
        val context: Context = this
        if (responseObject == null) {
            NoConnectionHelper.showNoConnectionToast(context)
            return
        }
        if (responseObject is Int) {
            _userId = responseObject
            //Success

            if (_thryveSourceId == SHEALTH_THRYVE_ID || _thryveSourceId == GFIT_NATIVE_THRYVE_ID) {
                lifecycleScope.launch(Dispatchers.Main) {
                    val accessToken = withContext(Dispatchers.IO) {
                        connector = CoreConnector(this@RegistryStepsActivity.applicationContext, resources.getString(R.string.appId),
                            resources.getString(R.string.appSecret), "$_userId", "de")
                        connector?.getAccessToken()?.data ?: ""
                    }
                    Log.d(javaClass.name, "got Thryve Token: $accessToken")
                    if (_thryveSourceId == SHEALTH_THRYVE_ID) enableSHealthIntegration(accessToken)
                    else if (_thryveSourceId == GFIT_NATIVE_THRYVE_ID) enableGFitIntegration(accessToken)
                }
            } else {
                this.startActivity(
                    Intent(context, ThryveSdkActivity::class.java)
                        .putExtra("companyId", _company.Id)
                        .putExtra(COMPANY, _company)
                        .putExtra("userId", _userId)
                        .putExtra("thryveSourceId", _thryveSourceId)
                )
            }
        } else {
            //fail
            val response = responseObject as CreateUserResponse
            if (response.lastname != null && response.lastname.size > 0) {
                registryInputs.lastname.inputError.value = (response.lastname[0])
            }
            if (response.firstname != null && response.firstname.size > 0) {
                registryInputs.firstname.inputError.value = (response.firstname[0])
            }
            if (response.nickname != null && response.nickname.size > 0) {
                registryInputs.nickname.inputError.value = (response.nickname[0])
            }
            if (response.email != null && response.email.size > 0) {
                registryInputs.email.inputError.value = (response.email[0])
            }
            if (response.email_confirmation != null && response.email.size > 0) {
                registryInputs.email_confirmation.inputError.value = (response.email[0])
            }
            if (response.polar_reg_confirm != null && response.polar_reg_confirm.size > 0) {
                registryInputs.trackerCompliance.inputError.value = (response.polar_reg_confirm[0])
            }
            if (response.agb_confirm != null && response.agb_confirm.size > 0) {
                registryInputs.privacyPolicy.inputError.value = (response.agb_confirm[0])
            }
//            if (response.security_code != null && response.security_code.size > 0) {
//                registryInputs.security_code.inputError.value = (response.security_code[0])
//            }
            if (response.password != null && response.password.size > 0) {
                registryInputs.password.inputError.value = (response.password[0])
            }
            if (response.password_confirmation != null && response.password_confirmation.size > 0) {
                registryInputs.password_confirmation.inputError.value = (response.password_confirmation[0])
            }
//            if (response.dayofbirth != null && response.dayofbirth.size > 0) {
//                registryInputs.dayofbirth.inputError.value = (response.dayofbirth[0])
//            }
            if (response.userdropdowns_dropdownvalue != null && response.userdropdowns_dropdownvalue.size > 0) {
                registryInputs.dropDowns.forEach { it.inputError.value = (response.userdropdowns_dropdownvalue[0]) }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        gFitConnector!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        gFitConnector!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val registryInputs = RegistryInputs()

            val navController = rememberNavController()
            NavHost(
                navController = navController,
                startDestination = "RegistryStep1"
            ) {
                composable(route = "RegistryStep1") {
                    RegistryStep1(registryInputs, _company) {
                        registryInputs.showLoadingIndicator.value = true
                        generalService.validateFields(_company, 1, registryInputs) { response ->
                            registryInputs.showLoadingIndicator.value = false
                            if (response is ValidationResponse) {
                                registryInputs.firstname.inputError.value = response.firstname?.firstOrNull() ?: ""
                                registryInputs.lastname.inputError.value = response.lastname?.firstOrNull() ?: ""
                                registryInputs.gender.inputError.value = response.gender?.firstOrNull() ?: ""
//                              registryInputs.dayofbirth.inputError.value = response.dayofbirth?.firstOrNull() ?: ""
                            } else {
                                navController.navigate("RegistryStep2")
                            }
                        }
                    }
                }
                composable(route = "RegistryStep2") {
                    RegistryStep2(registryInputs) {
                        generalService.validateFields(_company, 2, registryInputs) { response ->
                            if (response is ValidationResponse) {
                                registryInputs.nickname.inputError.value = response.nickname?.firstOrNull() ?: ""
                                registryInputs.email.inputError.value = response.email?.firstOrNull() ?: ""
                                registryInputs.email_confirmation.inputError.value = response.email_confirmation?.firstOrNull() ?: ""
                            } else {
                                navController.navigate("RegistryStep3")
                            }
                        }
                    }
                }
                composable(route = "RegistryStep3") {
                    RegistryStep3(registryInputs) {
                        generalService.validateFields(_company, 3, registryInputs) { response ->
                            if (response is ValidationResponse) {
                                registryInputs.password.inputError.value = response.password?.firstOrNull() ?: ""
                                registryInputs.password_confirmation.inputError.value = response.password_confirmation?.firstOrNull() ?: ""
                            } else {
                                navController.navigate("RegistryStep4")
                            }
                        }
                    }
                }
                composable(route = "RegistryStep4") {
                    RegistryStep4(registryInputs, _company) {
                        generalService.validateFields(_company, 4, registryInputs) { response ->
                            if (response is ValidationResponse) {
                                registryInputs.dropDowns.forEach { it.inputError.value = response.password?.firstOrNull() ?: "" }
                                registryInputs.privacyPolicy.inputError.value = response.agb_confirm?.firstOrNull() ?: ""
                                registryInputs.trackerCompliance.inputError.value = response.polar_reg_confirm?.firstOrNull() ?: ""
                                registryInputs.tracker.inputError.value = response.api_source_id?.firstOrNull() ?: ""
                            } else {
                                _thryveSourceId = _company.registration.api_source[registryInputs.tracker.selectedIndex.value].source_id
                                _trackerId = _company.registration.api_source[registryInputs.tracker.selectedIndex.value].Id

                                val request = CreateUserRequest(
                                    registryInputs.gender.selectedIndex.value,
                                    "",
                                    registryInputs.nickname.inputValue.value.trim(),
                                    registryInputs.lastname.inputValue.value.trim(),
                                    registryInputs.firstname.inputValue.value.trim(),
                                    registryInputs.email.inputValue.value.trim(),
                                    registryInputs.email_confirmation.inputValue.value.trim(),
                                    registryInputs.trackerCompliance.inputValue.value,
                                    registryInputs.privacyPolicy.inputValue.value,
                                    "${(_trackerId ?: "")}",
                                    registryInputs.password.inputValue.value.trim(),
                                    registryInputs.password_confirmation.inputValue.value.trim(),
                                    "",
                                    getDropDownValues(registryInputs)
                                )

                                generalService.CreateUser(
                                    _company.Id,
                                    request
                                ) { responseObject: Any? ->
                                    this@RegistryStepsActivity.handleCreateUserResponse(responseObject, registryInputs)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


