package de.fuenfzig670.sample.activities.fragments.adapters.callbacks

import de.fuenfzig670.sample.activities.fragments.adapters.ChallengeData

interface LeistungsKlasseExpandCallback {
    fun onExpanded(header: ChallengeData)
    fun onCollapsed(header: ChallengeData)
}
