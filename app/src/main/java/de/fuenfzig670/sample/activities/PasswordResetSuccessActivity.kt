package de.fuenfzig670.sample.activities

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.GreenButton
import de.fuenfzig670.sample.activities.composables.Headline1
import de.fuenfzig670.sample.activities.composables.ParagraphRegular

class PasswordResetSuccessActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PasswordResetSuccessScreen()
        }
    }

    @Preview
    @Composable
    fun PasswordResetSuccessScreenPreview() {
        PasswordResetSuccessScreen()
    }


    @Composable
    fun PasswordResetSuccessScreen() {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(32.dp))
                Headline1(text = "Passwort vergessen")
                Spacer(modifier = Modifier.height(16.dp))
                ParagraphRegular(text = "Du kannst nun ein neues Passwort festlegen")
                Spacer(modifier = Modifier.height(47.dp))
                ImageView(imageId = R.drawable.password_reset_success)
                Spacer(modifier = Modifier.height(40.dp))
                val message = buildAnnotatedString {
                    append("In deinem ")
                    withStyle(style = SpanStyle(fontFamily = FontFamily(Font(R.font.titilliumweb_bold)))) {
                        append("Posteingang")
                    }
                    append(" findest du nun einen Link um ein neues Passwort festzulegen.")
                }
                MessageView(
                    title = "Hinweis",
                    message = "",
                    annotatedMessage = message,
                    backgroundColor = colorResource(id = R.color.medium_grey)
                )
                Spacer(modifier = Modifier.height(16.dp))
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                GreenButton(text = "Einloggen") { onBackPressed() }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}
