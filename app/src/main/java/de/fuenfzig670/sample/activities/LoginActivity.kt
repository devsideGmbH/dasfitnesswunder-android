package de.fuenfzig670.sample.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.auth0.android.jwt.JWT
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.*
import de.fuenfzig670.sample.dataservice.CompanyDataService
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.NoConnectionHelper
import de.fuenfzig670.sample.model.LoginRequest
import de.fuenfzig670.sample.model.LoginRequestResponse
import de.fuenfzig670.sample.model.UserModel

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val loginInputs = LoginInputs()
            val inputCallback: (String) -> Unit = {
                loginInputs.enabledButton.value = loginInputs.email.inputValue.value.isNotBlank() && loginInputs.passwort.inputValue.value.isNotBlank()
            }
            LoginScreen(loginInputs, inputCallback)
        }
    }

    @Preview(showSystemUi = true, showBackground = true)
    @Composable
    private fun LoginScreenPreview() {
        LoginScreen()
    }

    @Composable
    private fun LoginScreen(loginInputs: LoginInputs = LoginInputs(), inputCallback: (String) -> Unit = {}) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            val rememberScrollState = rememberScrollState()
            Column(
                Modifier
                    .weight(1f)
                    .verticalFadingEdge(rememberScrollState, 48.dp, Color.White, topEdge = false)
            ) {
                Column(
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .padding(horizontal = 20.dp)
                        .weight(1f)
                        .fillMaxWidth()
                        .verticalScroll(rememberScrollState())
                ) {
                    Spacer(modifier = Modifier.height(38.dp))
                    Headline1(text = "Login")
                    Spacer(modifier = Modifier.height(16.dp))
                    ParagraphRegular(text = "Bitte gib deine E-Mail-Adresse\nund dein Passwort ein")
                    Spacer(modifier = Modifier.height(48.dp))
                    CustomInput(inputWrapper = loginInputs.email, onInputCallback = inputCallback)
                    CustomInput(inputWrapper = loginInputs.passwort, isPasswordInput = true, onInputCallback = inputCallback)
                    CustomCheckBox(loginInputs.stayLoggedIn)
                    Spacer(modifier = Modifier.height(32.dp))
                    CustomTextLink {
                        startActivity(Intent(this@LoginActivity, PasswordResetActivity::class.java))
                    }
                    Spacer(modifier = Modifier.height(48.dp))
                }
            }

            Column(modifier = Modifier.padding(horizontal = 20.dp)) {
                GreenButton(
                    text = "Einloggen", enabled = loginInputs.enabledButton.value
                ) {
                    val userObject = UserModel(
                        loginInputs.email.inputValue.value.trim(),
                        loginInputs.passwort.inputValue.value.trim(),
                        null,
                        _company.Id,
                        -1
                    )
                    GeneralService(this@LoginActivity).LoginUser(
                        _company.Id,
                        LoginRequest(userObject.email, userObject.password)
                    ) { response: Any? ->
                        this@LoginActivity.onLoginResponse(
                            response,
                            loginInputs,
                            userObject
                        )
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }

    @Composable
    fun CustomTextLink(onClick: () -> Unit) {
        Headline17BoldGrey(
            "Passwort vergessen",
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    onClick()
                },
            textAlign = TextAlign.Start,
            textColor = R.color.light_green,
            textStyle = TextStyle(textDecoration = TextDecoration.Underline)
        )
    }

    @Composable
    fun CustomCheckBox(stayLoggedIn: InputWrapperSwitch) {
        Row(
            Modifier
                .fillMaxWidth()
                .clickable {
                    stayLoggedIn.inputValue.value = !stayLoggedIn.inputValue.value
                },
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {
            val drawable =
                if (stayLoggedIn.inputValue.value) R.drawable.checkbox_checked else R.drawable.checkbox_unchecked
            Image(
                painter = painterResource(id = drawable),
                contentDescription = null,
                modifier = Modifier.size(24.dp)
            )
            Spacer(modifier = Modifier.width(12.dp))
            ParagraphRegular(text = stayLoggedIn.label)
        }
    }

    private fun onLoginResponse(response: Any?, loginInputs: LoginInputs, userModel: UserModel) {
        if (response == null) {
            NoConnectionHelper.showNoConnectionToast(this)
            return
        }
        //error
        if (response is LoginRequestResponse) {
            loginInputs.email.inputError.value = response.error
            loginInputs.passwort.inputError.value = response.error
        }
        //success
        if (response is String) {
            val token = response.toString()
            _sessionManager.token = token

            val jwt = JWT(token)
            val id: Int = jwt.getClaim("id").asInt()!!

            val context: Context = this
            val companyDataService = CompanyDataService(context)
            companyDataService.GetSpecificUser(id, userModel.companyId) { x: Any? ->
                if (x is UserModel) {
                    x.companyId = userModel.companyId
                    x.email = userModel.email
                    x.password = userModel.password
                    userModel.id = x.id
                    userModel.api_source = x.api_source
                    x.token = token
                    GeneralHelper.setStayLoggedIn(context, loginInputs.stayLoggedIn.inputValue.value)
                    GeneralHelper.SaveUser(context, x)
                    GeneralHelper.goToMain(this)
                    finish()
                }
            }
        }
    }

    data class LoginInputs(
        val enabledButton: MutableState<Boolean> = mutableStateOf(false),
        val email: InputWrapper = InputWrapper("E-Mail-Adresse", "E-Mail-Adresse eingeben"),
        val passwort: InputWrapper = InputWrapper("Passwort", "Passwort eingeben"),
        val stayLoggedIn: InputWrapperSwitch = InputWrapperSwitch("Eingeloggt bleiben"),
    )
}
