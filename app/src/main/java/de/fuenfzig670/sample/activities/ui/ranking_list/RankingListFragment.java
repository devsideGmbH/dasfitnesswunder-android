package de.fuenfzig670.sample.activities.ui.ranking_list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import de.fuenfzig670.dasfitnesswunder.databinding.FragmentRankingListBinding;
import de.fuenfzig670.sample.activities.ui.challenge.ChallengeFragment;

@Deprecated
public class RankingListFragment extends ChallengeFragment {

    private FragmentRankingListBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentRankingListBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        setUpFragment();

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
