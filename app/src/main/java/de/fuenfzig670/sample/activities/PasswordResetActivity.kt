package de.fuenfzig670.sample.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.*
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.model.PasswordResetRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PasswordResetActivity : BaseActivity() {
    private val generalService: GeneralService = GeneralService(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val inputWrapper = InputWrapper("E-Mail-Adresse", "E-Mail-Adresse eingeben")
            PasswordResetScreen(inputWrapper)
        }
    }

    @Preview
    @Composable
    fun PasswordResetScreenPreview() {
        PasswordResetScreen(InputWrapper("Passwort vergessen", "Bitte gib deine E-Mail-Adresse ein"))
    }

    @Composable
    fun PasswordResetScreen(inputWrapper: InputWrapper) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Column(
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .padding(horizontal = 20.dp)
                    .weight(1f)
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState())
            ) {
                Spacer(modifier = Modifier.height(32.dp))
                Headline1(text = "Passwort vergessen")
                Spacer(modifier = Modifier.height(16.dp))
                ParagraphRegular(text = "Bitte gib deine E-Mail-Adresse ein")
                Spacer(modifier = Modifier.height(40.dp))
                val message = buildAnnotatedString {
                    append("Wenn du dein Passwort vergessen hast, gib bitte deine E-Mail Adresse ein. \n\nWir senden dir einen ")
                    withStyle(style = SpanStyle(fontFamily = FontFamily(Font(R.font.titilliumweb_bold)))) {
                        append("Link")
                    }
                    append(" per E-Mail um ein neues Passwort festzulegen.")
                }
                MessageView(
                    title = "Hinweis",
                    message = "",
                    annotatedMessage = message,
                    backgroundColor = colorResource(id = R.color.medium_grey)
                )
                Spacer(modifier = Modifier.height(48.dp))
                CustomInput(inputWrapper = inputWrapper)
                Spacer(modifier = Modifier.height(32.dp))
            }

            Column(modifier = Modifier.padding(horizontal = 20.dp)) {
                Spacer(modifier = Modifier.height(16.dp))
                GreenButton(text = "Passwort zurücksetzen") {
                    onClickPasswordReset(inputWrapper)
                }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }

    private fun onClickPasswordReset(inputWrapper: InputWrapper) {
        val res = inputWrapper.inputValue.value.trim()
        if(res.isBlank()) {
            onPasswordResetResponse(false, inputWrapper)
        } else {
            generalService.PasswordReset(
                _company.Id, PasswordResetRequest(inputWrapper.inputValue.value.trim())
            ) { response: Any -> onPasswordResetResponse(response, inputWrapper) }
        }
    }

    private fun onPasswordResetResponse(response: Any, inputWrapper: InputWrapper) {
        try {
            if (response as Boolean) {
                lifecycleScope.launch(Dispatchers.Main) {
                    startActivity(Intent(this@PasswordResetActivity, PasswordResetSuccessActivity::class.java))
                    finish()
                }
            } else {
                inputWrapper.inputError.value =
                    "Ihr Passwort konnte nicht zurückgesetzt werden. Stellen Sie sicher, dass Sie eine Internetverbindung haben und versuchen Sie es erneut."
            }
        } catch (e: Exception) {
            inputWrapper.inputError.value =
                "Ihr Passwort konnte nicht zurückgesetzt werden. Stellen Sie sicher, dass Sie eine Internetverbindung haben und versuchen Sie es erneut."
        }
    }
}
