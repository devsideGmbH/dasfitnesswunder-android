package de.fuenfzig670.sample.activities.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager.OnBackStackChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.MainActivity
import de.fuenfzig670.sample.activities.fragments.adapters.*
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.LeistungsKlasseExpandCallback
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.RankingCategoryClickCallback
import de.fuenfzig670.sample.activities.fragments.adapters.itemdecorators.LeftSpaceItemDecoration
import de.fuenfzig670.sample.dataservice.CompanyDataService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.SessionManager
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.GroupRanking
import de.fuenfzig670.sample.model.RankingCategory
import de.fuenfzig670.sample.model.UserRanking


/**
 * A simple [Fragment] subclass.
 * Use the [StartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChallengeDetailsFragment : Fragment() {

    private var company: Company = SessionManager.getInstance().company
    private lateinit var selectedRankingCategory: RankingCategory
    private lateinit var rankingCategories: ArrayList<RankingCategory>
    private lateinit var companyDataService: CompanyDataService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            selectedRankingCategory = it.getParcelable(SELECTED_RANKING_CATEGORY_PARAM)!!
            rankingCategories = it.getParcelableArrayList(RANKING_CATEGORIES_PARAM)!!
        }

        companyDataService = CompanyDataService(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupCategories()
        setupChallenges()
        hidePlatzierungCardIfNeeded()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().supportFragmentManager.addOnBackStackChangedListener(backStackListener)
    }

    override fun onResume() {
        super.onResume()

        updateActionBarTitle()

        val parentCoordinatorLayout = requireActivity().findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
        parentCoordinatorLayout.fitsSystemWindows = true
    }

    val backStackListener = object : OnBackStackChangedListener {
        override fun onBackStackChanged() {
            activity?.let { activity ->
                val index = activity.supportFragmentManager.backStackEntryCount - 1
                if (index >= 0) {
                    val entry = activity.supportFragmentManager.getBackStackEntryAt(index)
                    if (entry.name == STACK_ID) {
                        updateActionBarTitle()
                    }
                }
            }
        }
    }

    override fun onDetach() {
        super.onDetach()

        try {
            requireActivity().supportFragmentManager.removeOnBackStackChangedListener(backStackListener)
        } catch (e: Exception) {
            backStackListener
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_challenge_details, container, false)
    }

    private fun updateActionBarTitle() {
        val actionBar = (requireActivity() as? AppCompatActivity)?.supportActionBar
        actionBar?.hide()

        /*actionBar?.show()
          actionBar?.setDisplayShowTitleEnabled(true)
          setActionBarTitle(selectedRankingCategory.dropdowntitle)
        */
    }

    private fun setupCategories() {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL

        val categoriesRecyclerView = view?.findViewById<RecyclerView>(R.id.categoriesRecyclerView)
        categoriesRecyclerView?.layoutManager = linearLayoutManager

        val indexOfSelectedCategory = rankingCategories.indexOf(selectedRankingCategory)
        categoriesRecyclerView?.addItemDecoration(LeftSpaceItemDecoration(requireView().dpToPx(8F)))

        val adapter =
            ChallengeRankingFilterCategoryAdapter(rankingCategories, selectedRankingCategory, requireContext(), object : RankingCategoryClickCallback {
                override fun onRankingCategoryClicked(rankingCategory: RankingCategory, isMap: Boolean) {
                    if (isMap) {
                        showMapChallenge()
                    } else {
                        selectedRankingCategory = rankingCategory
                        hidePlatzierungCardIfNeeded()
                        setActionBarTitle(selectedRankingCategory.dropdowntitle)
                        setupChallenges()
                    }

                }
            })
        categoriesRecyclerView?.adapter = adapter

        /* Scroll to the selected category as it can be out of sight */
        linearLayoutManager.scrollToPositionWithOffset(indexOfSelectedCategory, requireView().dpToPx(10F))
    }

    private fun setupChallenges() {
        val allPlacesRecyclerView = view?.findViewById<RecyclerView>(R.id.allPlacesRecyclerView)
        allPlacesRecyclerView?.visibility = View.GONE

        if (selectedRankingCategory.isTeam) {
            showTeamChallenge()
        } else {
            //Gesamt
            if (selectedRankingCategory.ranking_type == 0) {
                showGesamtChallenge()
            }
        }
    }

    private fun hidePlatzierungCardIfNeeded() {
        val placeTitleTextView = view?.findViewById<TextView>(R.id.placeTitleTextView)
        val placeCardView = view?.findViewById<CardView>(R.id.placeCardView)
        if (selectedRankingCategory.isTeam) {
            placeCardView?.visibility = View.GONE
            placeTitleTextView?.visibility = View.GONE
        } else {
            placeCardView?.visibility = View.VISIBLE
            placeTitleTextView?.visibility = View.VISIBLE
        }
    }

    private fun updatePlatzierungData(challengeDataItems: ArrayList<ChallengeData>, isLeistungsKlassen: Boolean): ChallengeData? {
        val user = GeneralHelper.GetUser(requireContext())

        var parentItemOfMyPerfomanceClass: ChallengeData? = null
        challengeDataItems.forEach { parentItem ->
            val myUserRanking = parentItem.childRankings.firstOrNull { childItem ->
                childItem.Id == user.id
            }
            if (myUserRanking != null) {
                parentItemOfMyPerfomanceClass = parentItem
                val placeValueTextView = view?.findViewById<TextView>(R.id.placeValueTextView)
                val stepsValueTextView = view?.findViewById<TextView>(R.id.stepsValueTextView)
                val literValueTextView = view?.findViewById<TextView>(R.id.literValueTextView)
                val literValueLabel = view?.findViewById<TextView>(R.id.literValueLabel)

                placeValueTextView?.text = "${myUserRanking.Ranking}."
                stepsValueTextView?.text = "${myUserRanking.Steps}"
                if (company.eco_challenge) {
                    literValueLabel?.visibility = View.VISIBLE
                    literValueTextView?.text = "${if (myUserRanking.eco == null || myUserRanking.eco == "") 0 else myUserRanking.eco} l"
                    literValueTextView?.visibility = View.VISIBLE
                } else {
                    literValueTextView?.visibility = View.GONE
                    literValueLabel?.visibility = View.GONE
                }

                if (isLeistungsKlassen) {
                    updatePlatzierungHeader(parentItem.header!!.ranking.Nickname)
                }
            }
        }

        return parentItemOfMyPerfomanceClass
    }

    private fun updatePlatzierungHeader(perfomanceClassTitle: String) {
        val placeHeader = view?.findViewById<RelativeLayout>(R.id.placeHeaderLayout)
        val placeHeaderTextView = view?.findViewById<TextView>(R.id.placeHeaderTextView)
        placeHeader?.visibility = View.VISIBLE
        placeHeaderTextView?.text = perfomanceClassTitle.substringBefore("(")
    }

    private fun showGesamtChallenge() {
        val showGender: Boolean = selectedRankingCategory.dropdowntitle.contains(getString(R.string.gender_enabled_required))

        val stepsTextView = requireView().findViewById<TextView>(R.id.stepsTextView)
        stepsTextView.margin(right = 24F)
        var isLeistungsKlassen = false

        val items = ArrayList<ChallengeData>()
        companyDataService.GetSpecificRanking(company.Id, selectedRankingCategory.Id, showGender) { response ->

            (response as? ArrayList<UserRanking>)?.let { userRankings ->
                var headerItem: ChallengeData? = null
                var rankingItems: ArrayList<UserRanking>? = null

                /* Make data source for expandable recycle view*/
                userRankings.forEach { ranking ->
                    if (ranking.isGroupHeader) {
                        isLeistungsKlassen = true

                        rankingItems = ArrayList()
                        headerItem = ChallengeData(
                            type = ItemTypes.PARENT_LEISTUNGKLASSEN,
                            header = ChallengeHeader(ranking, ItemTypes.PARENT_LEISTUNGKLASSEN),
                            childRankings = rankingItems!!
                        )
                        items.add(headerItem!!)
                    } else {
                        if (headerItem != null) {
                            rankingItems!!.add(ranking)
                        } else {
                            val childData = ChallengeData(type = ItemTypes.CHILD, header = null, childRankings = ArrayList(listOf(ranking)))
                            items.add(childData)
                        }
                    }
                }
                /* Show your place in you perfomance class and return the parent item  of this perfomance class*/
                val parentItemOfMyPerfomanceClass = updatePlatzierungData(items, isLeistungsKlassen)
                setupChallengeAdapter(items, isLeistungsKlassen, parentItemOfMyPerfomanceClass)
            }
        }
    }

    private fun showMapChallenge() {
        (activity as? MainActivity)?.showMapFragment()
    }

    private fun showTeamChallenge() {
        val showGender: Boolean = selectedRankingCategory.dropdowntitle.contains(getString(R.string.gender_enabled_required))

        val stepsTextView = requireView().findViewById<TextView>(R.id.stepsTextView)
        stepsTextView.margin(right = 50F)

        val items = ArrayList<ChallengeData>()
        companyDataService.GetSpecificDynamicRanking(company.Id, selectedRankingCategory.Id, showGender) { response ->
            (response as? ArrayList<GroupRanking>)?.let { groupRankings ->
                groupRankings.forEach { groupRanking ->
                    val childItems = ArrayList<ChallengeData>()
                    groupRanking.users.forEach { userRanking ->
                        childItems.add(
                            ChallengeData(
                                type = ItemTypes.CHILD,
                                header = ChallengeHeader(userRanking, ItemTypes.PARENT_TEAM),
                                childRankings = ArrayList(listOf(userRanking))
                            )
                        )
                    }
                    val headerRanking = UserRanking()
                    headerRanking.Nickname = groupRanking.grouptitle
                    headerRanking.Steps = groupRanking.Steps
                    headerRanking.tendency = groupRanking.tendency
                    headerRanking.Ranking = groupRanking.Ranking

                    val group = ChallengeData(
                        type = ItemTypes.PARENT_TEAM,
                        header = ChallengeHeader(headerRanking, ItemTypes.PARENT_TEAM),
                        childRankings = groupRanking.users
                    )
                    items.add(group)
                }

                setupChallengeAdapter(items, false)
            }
        }
    }

    private fun setupChallengeAdapter(items: ArrayList<ChallengeData>, isLeistungsKlassen: Boolean, parentItemOfMyPerfomanceClass: ChallengeData? = null) {
        view?.let { view ->
            val stepsTextView = view.findViewById<TextView>(R.id.stepsTextView)
            if (isLeistungsKlassen) {
                stepsTextView.visibility = View.GONE
            } else {
                stepsTextView.visibility = View.VISIBLE
            }

            val linearLayoutManager = LinearLayoutManager(context)// .apply { isAutoMeasureEnabled = true }
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

            val allPlacesRecyclerView = view.findViewById<RecyclerView>(R.id.allPlacesRecyclerView)
            allPlacesRecyclerView?.visibility = View.VISIBLE
            allPlacesRecyclerView?.layoutManager = linearLayoutManager

            val adapter = ChallengesAdapter(requireContext(), items, company.eco_challenge, object : LeistungsKlasseExpandCallback {
                override fun onCollapsed(header: ChallengeData) {

                }

                override fun onExpanded(header: ChallengeData) {

                }
            })
            allPlacesRecyclerView?.adapter = adapter
            adapter.expandPerfomanceClass(parentItemOfMyPerfomanceClass)
        }
    }

    private fun setActionBarTitle(title: String) {
        (requireActivity() as? AppCompatActivity)?.supportActionBar?.title = title
    }

    companion object {
        const val STACK_ID = "ChallengeDetailsFragmentStackId"
        const val RANKING_CATEGORIES_PARAM = "RANKING_CATEGORIES_PARAM"
        const val SELECTED_RANKING_CATEGORY_PARAM = "SELECTED_RANKING_CATEGORY_PARAM"

        @JvmStatic
        fun newInstance(company: Company, selectedCategory: RankingCategory, categories: ArrayList<RankingCategory>) =
            ChallengeDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(COMPANY_PARAM, company)
                    putParcelable(SELECTED_RANKING_CATEGORY_PARAM, selectedCategory)
                    putParcelableArrayList(RANKING_CATEGORIES_PARAM, categories)
                }
            }
    }
}
