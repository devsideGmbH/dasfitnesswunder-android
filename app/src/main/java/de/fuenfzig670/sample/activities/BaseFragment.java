package de.fuenfzig670.sample.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Arrays;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.helpers.SessionManager;
import de.fuenfzig670.sample.model.Company;

public class BaseFragment extends Fragment {
    protected View root;

    protected static final String TAG = BaseActivity.class.getName();

    protected Boolean _hasUserLogin;
    protected Company _company;
    RequestOptions _options = new RequestOptions().placeholder(R.mipmap.ic_launcher_round).error(R.mipmap.ic_launcher_round);
    protected SessionManager _sessionManager = SessionManager.getInstance();

    protected boolean isNightMode() {
        return (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
    }

    protected boolean ActivatedContainsKey(Company company, String key) {
        String[] activatedSplinter = company.registration.activated.split(", ");
        return Arrays.asList(activatedSplinter).contains(key);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            callBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setupHeader() {
        if (_company == null) {
            Log.d(getClass().getName(), "setupHeader: ", new Exception("no company"));
            return;
        }
        ImageView imageView = root.findViewById(R.id.company_logo);
        if (imageView != null) {
            Glide.with(this)
                    .load(getString(R.string.BASEURL) + _company.Logo)
                    .apply(_options).into(imageView);
        }
        TextView textView = root.findViewById(R.id.company_slogan);
        if (textView != null) {
            textView.setText(_company.Slogan);
            if (_company.Slogan.equals("")) textView.setVisibility(View.GONE);
        }
    }

    protected View findViewById(int rootViewById) {
        return root.findViewById(rootViewById);
    }

    protected Intent getIntent() {
        FragmentActivity activity = getActivity();
        if (activity == null) return null;
        return activity.getIntent();
    }

    static public void highlightText(String textToHighlight, TextView textView) {
        String oldString = textView.getText().toString();
        String newString = oldString.replaceAll(textToHighlight, "<font color='#007AFF'>" + textToHighlight + "</font>");
        textView.setText(Html.fromHtml(newString));
    }

    protected void callBackPressed() {
        FragmentActivity activity = getActivity();
        if (activity != null) activity.onBackPressed();
    }
}
