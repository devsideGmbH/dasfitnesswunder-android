package de.fuenfzig670.sample.activities.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.MainActivity
import de.fuenfzig670.sample.activities.fragments.adapters.OverViewItemType
import de.fuenfzig670.sample.activities.fragments.adapters.OverviewAdapter
import de.fuenfzig670.sample.activities.fragments.adapters.RankingCategoryAdapter
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.OverviewClickCallback
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.RankingCategoryClickCallback
import de.fuenfzig670.sample.activities.fragments.adapters.dpToPx
import de.fuenfzig670.sample.activities.fragments.adapters.itemdecorators.LeftSpaceItemDecoration
import de.fuenfzig670.sample.activities.fragments.adapters.itemdecorators.TopSpaceItemDecoration
import de.fuenfzig670.sample.dataservice.CompanyDataService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.SessionManager
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.RankingCategory
import de.fuenfzig670.sample.services.IObjectCallback

const val COMPANY_PARAM = "COMPANY_PARAM"

/**
 * A simple [Fragment] subclass.
 * Use the [StartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StartFragment : Fragment() {
    private var company: Company = SessionManager.getInstance().company
    private lateinit var companyDataService: CompanyDataService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        companyDataService = CompanyDataService(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupOverViewAdapter()
        setupRankingCategories()

        val userModel = GeneralHelper.GetUser(requireContext())

        val welcomeTextView = view.findViewById<TextView>(R.id.welcomeTextView)
        welcomeTextView.text = "Willkommen ${userModel.firstname}!"
        val sloganTextView = view.findViewById<TextView>(R.id.sloganTextView)

        if (company.Slogan.isBlank()) {
            sloganTextView.visibility = View.GONE
        } else {
            sloganTextView.visibility = View.VISIBLE
            sloganTextView.text = "${company.Slogan}"
        }

        val companyLogoImageView = view.findViewById<ImageView>(R.id.companyLogoImageView)
        Glide.with(requireContext())
            .load("${getString(R.string.BASEURL)}/${company.Logo}")
            .fitCenter()
            .into(companyLogoImageView)

        (requireActivity() as? AppCompatActivity)?.supportActionBar?.hide()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_main_start, container, false)
        return view
    }

    override fun onDetach() {
        super.onDetach()
    }

    private fun setupOverViewAdapter() {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL

        val overViewRecyclerView = view?.findViewById<RecyclerView>(R.id.overViewRecyclerView)
        overViewRecyclerView?.layoutManager = linearLayoutManager

        overViewRecyclerView?.addItemDecoration(LeftSpaceItemDecoration(requireView().dpToPx(16F)))
        overViewRecyclerView?.adapter = OverviewAdapter(company, requireContext(), object : OverviewClickCallback {

            override fun onItemClicked(type: OverViewItemType) {

            }
        })
    }

    private fun setupRankingCategories() {
        companyDataService.GetAllRankings(company.Id, object : IObjectCallback {
            override fun onResponse(response: Any?) {
                val rankingCategories = response as ArrayList<RankingCategory>
                rankingCategories.forEach { ranking -> ranking.isTeam = false }

                companyDataService.GetDynamicRankings(company.Id, object : IObjectCallback {
                    override fun onResponse(response: Any?) {
                        val dynamicRankings = response as ArrayList<RankingCategory>
                        dynamicRankings.forEach { ranking -> ranking.isTeam = true }

                        val allRankingCategories = rankingCategories + dynamicRankings
                        setupRankingCategoriesAdapter(ArrayList(allRankingCategories))
                    }
                })
            }
        })
    }

    private fun setupRankingCategoriesAdapter(rankingCategories: ArrayList<RankingCategory>) {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        val rankingCategoriesRecyclerView = view?.findViewById<RecyclerView>(R.id.rankingCategoriesRecyclerView)
        rankingCategoriesRecyclerView?.layoutManager = linearLayoutManager
        rankingCategoriesRecyclerView?.addItemDecoration(TopSpaceItemDecoration(requireView().dpToPx(16F)))

        rankingCategoriesRecyclerView?.adapter = RankingCategoryAdapter(rankingCategories, requireContext(), object : RankingCategoryClickCallback {
            override fun onRankingCategoryClicked(selectedRankingCategory: RankingCategory, isMap: Boolean) {
                if (isMap) {
                    showMapFragment()
                } else {
                    showChallengesFragment(rankingCategories, selectedRankingCategory)
                }
            }
        })
    }

    private fun showChallengesFragment(categories: ArrayList<RankingCategory>, selectedRankingCategory: RankingCategory) {
        (activity as? MainActivity)?.showChallengesFragment(categories, selectedRankingCategory)
    }

    private fun showMapFragment() {
        (activity as? MainActivity)?.showMapFragment()
    }

    companion object {
        val STACK_ID = "StartFragmentStackId"

        @JvmStatic
        fun newInstance() = StartFragment()
    }
}
