package de.fuenfzig670.sample.activities.fragments.adapters.callbacks

import de.fuenfzig670.sample.model.RankingCategory
import de.fuenfzig670.sample.model.UserRanking

interface RankingCategoryClickCallback {
    fun onRankingCategoryClicked(rankingCategory: RankingCategory, isMap: Boolean)
}
