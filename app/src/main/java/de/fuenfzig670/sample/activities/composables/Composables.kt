package de.fuenfzig670.sample.activities.composables


import android.graphics.Rect
import android.view.KeyEvent.ACTION_DOWN
import android.view.ViewTreeObserver
import androidx.annotation.ColorRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.platform.debugInspectorInfo
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.*
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.model.DropDown

@Preview
@Composable
fun GreenButtonPreview() {
    GreenButton(text = "Test") {
    }
}

@Preview
@Composable
fun GreenBorderButtonPreview() {
    GreenBorderButton(text = "Test") {
    }
}

@Preview
@Composable
fun DisabeledButtonPreview() {
    GreenButton(text = "Test", enabled = false) {
    }
}

@Composable
fun GreenButton(text: String, modifier: Modifier = Modifier, enabled: Boolean = true, onClick: () -> Unit) {
    BaseButton(
        text,
        colorResource(id = R.color.light_green), colorResource(R.color.white),
        enabled = enabled,
        borderStroke = BorderStroke(2.dp, colorResource(id = if (enabled) R.color.light_green else R.color.light_grey)),
        disabledBackgroundColor = colorResource(id = R.color.light_grey),
        disabledContentColor = colorResource(id = R.color.primary_button_disabled_text_color), modifier = modifier, onClick = onClick
    )
}

@Composable
fun GreenBorderButton(text: String, modifier: Modifier = Modifier, enabled: Boolean = true, onClick: () -> Unit) {
    BaseButton(
        text,
        colorResource(R.color.white),
        colorResource(id = R.color.light_green),
        enabled = enabled,
        borderStroke = BorderStroke(2.dp, colorResource(id = if (enabled) R.color.light_green else R.color.secondary_button_disabled_text_color)),
        disabledBackgroundColor = colorResource(id = R.color.light_grey),
        disabledContentColor = colorResource(id = R.color.dark_grey),
        modifier = modifier,
        onClick = onClick
    )
}

@Composable
fun BaseButton(
    text: String,
    backgroundColor: Color,
    contentColor: Color,
    modifier: Modifier = Modifier,
    borderStroke: BorderStroke,
    disabledBackgroundColor: Color,
    disabledContentColor: Color,
    enabled: Boolean = true,
    onClick: () -> Unit
) {
    OutlinedButton(
        shape = RoundedCornerShape(14.dp),
        modifier = modifier
            .height(50.dp)
            .fillMaxWidth(),
        onClick = onClick,
        enabled = enabled,
        border = borderStroke,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = backgroundColor,
            contentColor = contentColor,
            disabledBackgroundColor = disabledBackgroundColor,
            disabledContentColor = disabledContentColor
        )
    ) {
        ButtonText(text, if (enabled) contentColor else disabledContentColor)
    }
}

@Composable
fun ButtonText(text: String, color: Color = Color.White, modifier: Modifier = Modifier) {
    Text(
        text = text,
        textAlign = TextAlign.Center,
        color = color,
        fontFamily = FontFamily(Font(R.font.titilliumweb_bold)),
        fontSize = 17.sp,
        letterSpacing = (-0.02).sp,
        modifier = modifier
    )
}

@Preview
@Composable
fun Headline1Preview() {
    Headline1(text = "Headline1")
}

@Composable
fun Headline1(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Center) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.light_green),
        fontFamily = FontFamily(Font(R.font.titilliumweb_bolditalic)),
        fontSize = 32.sp,
        modifier = modifier,
        lineHeight = 40.sp
    )
}

@Composable
fun Headline24(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Center) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.black),
        fontFamily = FontFamily(Font(R.font.titilliumweb_bold)),
        fontSize = 24.sp,
        modifier = modifier
    )
}

@Composable
fun RegistryHeader(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Start) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.black),
        fontFamily = FontFamily(Font(R.font.titilliumweb_semibold)),
        fontSize = 17.sp,
        modifier = modifier
    )
}

@Composable
fun RegistrySubHeader(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Start, color: Color = colorResource(id = R.color.black)) {
    Text(
        text = text,
        textAlign = textAlign,
        color = color,
        fontFamily = FontFamily(Font(R.font.titilliumweb_regular)),
        fontSize = 14.sp,
        modifier = modifier
    )
}

@Composable
fun RegistryCircleText(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Center) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.black),
        fontFamily = FontFamily(Font(R.font.titilliumweb_bold)),
        fontSize = 17.sp,
        modifier = modifier
    )
}

@Preview
@Composable
fun ParagraphRegularPreview() {
    ParagraphRegular("Default paragraph", textAlign = TextAlign.Start)
}

@Composable
fun PlaceHolder(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Start) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.dark_grey),
        fontFamily = FontFamily(Font(R.font.titilliumweb_regular)),
        fontSize = 17.sp,
        modifier = modifier
    )
}

@Composable
fun ParagraphRegular(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Center) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.dark_grey),
        fontFamily = FontFamily(Font(R.font.titilliumweb_regular)),
        fontSize = 16.sp,
        modifier = modifier
    )
}

@Composable
fun ParagraphRegularAnnotated(text: AnnotatedString, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Center) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.dark_grey),
        fontFamily = FontFamily(Font(R.font.titilliumweb_regular)),
        fontSize = 16.sp,
        modifier = modifier
    )
}

@Composable
fun ParagraphRegularSmall(text: String, modifier: Modifier = Modifier, textAlign: TextAlign = TextAlign.Center) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = R.color.dark_grey),
        fontFamily = FontFamily(Font(R.font.titilliumweb_regular)),
        fontSize = 14.sp,
        modifier = modifier
    )
}

@Composable
fun Headline17BoldGrey(
    text: String,
    modifier: Modifier = Modifier,
    textAlign: TextAlign = TextAlign.Center,
    @ColorRes textColor: Int = R.color.dark_grey_transparent,
    textStyle: TextStyle = LocalTextStyle.current
) {
    Text(
        text = text,
        textAlign = textAlign,
        color = colorResource(id = textColor),
        fontFamily = FontFamily(Font(R.font.titilliumweb_bold)),
        fontSize = 17.sp,
        modifier = modifier,
        style = textStyle
    )
}

private const val TEXT_SCALE_REDUCTION_INTERVAL = 0.9f

@Composable
fun ResponsiveText(
    modifier: Modifier = Modifier,
    text: String,
    color: Color,
    textAlign: TextAlign = TextAlign.Center,
    textStyle: TextStyle,
    targetTextSizeHeight: TextUnit = textStyle.fontSize,
    maxLines: Int = 1,
) {
    var textSize by remember { mutableStateOf(targetTextSizeHeight) }

    Text(
        modifier = modifier,
        text = text,
        color = color,
        textAlign = textAlign,
        fontSize = textSize,
        fontFamily = textStyle.fontFamily,
        fontStyle = textStyle.fontStyle,
        fontWeight = textStyle.fontWeight,
        lineHeight = textStyle.lineHeight,
        maxLines = maxLines,
        overflow = TextOverflow.Ellipsis,
        onTextLayout = { textLayoutResult ->
            val maxCurrentLineIndex: Int = textLayoutResult.lineCount - 1

            if (textLayoutResult.isLineEllipsized(maxCurrentLineIndex)) {
                textSize = textSize.times(TEXT_SCALE_REDUCTION_INTERVAL)
            }
        },
    )
}


@Composable
fun SeperatorLine() {
    Spacer(
        modifier = Modifier
            .height(1.dp)
            .fillMaxWidth()
            .background(colorResource(id = R.color.black_transparent_10))
    )
}


data class RegistryInputs(
    val showLoadingIndicator: MutableState<Boolean> = mutableStateOf(false),

    val firstname: InputWrapper = InputWrapper("Vorname*", "Vorname eingeben"),
    val lastname: InputWrapper = InputWrapper("Nachname*", "Nachname eingeben"),
    val gender: InputWrapper = InputWrapper("Geschlecht*", "Bitte auswählen"),

    val nickname: InputWrapper = InputWrapper("Spitzname*", "Spitzname eingeben", note = "Der Spitzname wird in der Challenge angezeigt"),
    val email: InputWrapper = InputWrapper("E-Mail Adresse*", "E-Mail Adresse eingeben"),
    val email_confirmation: InputWrapper = InputWrapper("E-Mail Adresse wiederholen*", "E-Mail Adresse eingeben"),

    val password: InputWrapper = InputWrapper("Passwort*", "Passwort eingeben", note = "min. sechs Zeichen mit Groß- und Kleinbuchstaben"),
    val password_confirmation: InputWrapper = InputWrapper("Passwort wiederholen*", "Passwort eingeben"),

    val dropDowns: ArrayList<InputWrapper> = arrayListOf(),

    val tracker: InputWrapper = InputWrapper("Schrittezähler*", "Bitte auswählen"),

    val trackerCompliance: InputWrapperSwitch = InputWrapperSwitch("Ja, ich benutze mein Smartphone und habe die nötige Health App installiert und eingerichtet."),
    val privacyPolicy: InputWrapperSwitch = InputWrapperSwitch("Ja, ich habe die Datenschutz- und Teilnahmebedingungen gelesen und bin mit diesen einverstanden."),
) {
    fun clearErrors() {
        firstname.inputError.value = ""
        lastname.inputError.value = ""
        gender.inputError.value = ""
        nickname.inputError.value = ""
        email.inputError.value = ""
        email_confirmation.inputError.value = ""
        password.inputError.value = ""
        password_confirmation.inputError.value = ""
        dropDowns.forEach { it.inputError.value = "" }
        tracker.inputError.value = ""
        trackerCompliance.inputError.value = ""
        privacyPolicy.inputError.value = ""
    }
}

data class InputWrapper(
    val label: String,
    val placeHolder: String,
    val note: String? = null,
    var inputValue: MutableState<String> = mutableStateOf(""),
    var inputError: MutableState<String> = mutableStateOf(""),
    var selectedIndex: MutableState<Int> = mutableStateOf(0),
    val dropDown: DropDown? = null
)

data class InputWrapperSwitch(
    val label: String,
    var inputValue: MutableState<Boolean> = mutableStateOf(false),
    var inputError: MutableState<String> = mutableStateOf(""),
)

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CustomInput(
    inputWrapper: InputWrapper,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    isPasswordInput: Boolean = false,
    maxLines: Int = 1,
    onInputCallback: (String) -> Unit = {},
    trailingIcon: @Composable (() -> Unit)? = null
) {
    val hasError = inputWrapper.inputError.value.isNotBlank()
    val borderColor = colorResource(id = if (hasError) R.color.error else R.color.textfield_border_color)
    val textColor = colorResource(id = R.color.black)

    var passwordInvisible by rememberSaveable { mutableStateOf(true) }

    val passwordIcon = @Composable {
        val image = if (passwordInvisible)
            Icons.Filled.VisibilityOff
        else Icons.Filled.Visibility
        IconButton(onClick = { passwordInvisible = !passwordInvisible }) {
            Icon(imageVector = image, null)
        }
    }

    Column(Modifier.fillMaxWidth()) {
        ParagraphRegular(text = inputWrapper.label)
        Spacer(modifier = Modifier.height(8.dp))
        val focusManager = LocalFocusManager.current
        OutlinedTextField(
            modifier = modifier
                .fillMaxWidth()
                .onPreviewKeyEvent {
                    if (it.key == Key.Tab && it.nativeKeyEvent.action == ACTION_DOWN) {
                        focusManager.moveFocus(FocusDirection.Down)
                        true
                    } else {
                        false
                    }
                },
            value = inputWrapper.inputValue.value,
            onValueChange = {
                inputWrapper.inputValue.value = it
                onInputCallback(it)
            },
            placeholder = {
                PlaceHolder(inputWrapper.placeHolder)
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = borderColor,
                unfocusedBorderColor = borderColor,
                textColor = textColor,
                disabledBorderColor = borderColor,
                disabledTextColor = textColor
            ),
            shape = RoundedCornerShape(12.dp),
            textStyle = TextStyle(fontFamily = FontFamily(Font(R.font.titilliumweb_regular)), fontSize = 17.sp),
            enabled = enabled,
            maxLines = maxLines,

            trailingIcon = if (isPasswordInput) passwordIcon else trailingIcon,
            visualTransformation = if (passwordInvisible && isPasswordInput) PasswordVisualTransformation() else VisualTransformation.None,
            keyboardOptions = if (isPasswordInput) KeyboardOptions(keyboardType = KeyboardType.Password, imeAction = ImeAction.Next) else KeyboardOptions(
                imeAction = ImeAction.Next
            ),
            keyboardActions = KeyboardActions(
                onNext = { focusManager.moveFocus(FocusDirection.Down) }
            )
        )
        inputWrapper.note?.let {
            Spacer(modifier = Modifier.height(8.dp))
            RegistrySubHeader(it, color = colorResource(id = R.color.dark_grey))
        }
        if (hasError) {
            ErrorComposable(inputWrapper.inputError)
        }
        Spacer(modifier = Modifier.height(24.dp))
    }
}

@Composable
fun ErrorComposable(inputError: MutableState<String>) {
    Spacer(modifier = Modifier.height(8.dp))
    Row {
        Image(painter = painterResource(id = R.drawable.input_error_image), contentDescription = null, modifier = Modifier.size(16.dp))
        Spacer(modifier = Modifier.width(8.dp))
        RegistrySubHeader(text = inputError.value, color = colorResource(id = R.color.error))
    }
}

@Composable
fun DropdownList(items: List<String>, inputWrapper: InputWrapper) {
    var expanded by remember { mutableStateOf(false) }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.TopStart)
    ) {
        CustomInput(inputWrapper = inputWrapper, modifier = Modifier.clickable { expanded = !expanded }, false, trailingIcon = {
            IconButton(onClick = { expanded = !expanded }) {
                val drawable = if (expanded) R.drawable.dropdown_up else R.drawable.dropdown_down
                Image(painter = painterResource(id = drawable), contentDescription = null, modifier = Modifier.size(24.dp, 8.dp))
            }
        })
        inputWrapper.inputValue.value = items[inputWrapper.selectedIndex.value]
        DropdownMenu(
            offset = DpOffset(x = 0.dp, y = (-24).dp),
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .background(Color.White)
                .wrapContentSize()
        ) {
            items.forEachIndexed { index, s ->
                DropdownMenuItem(onClick = {
                    inputWrapper.selectedIndex.value = index
                    expanded = false
                }) {
                    Text(
                        text = s,
                        fontFamily = FontFamily(Font(R.font.titilliumweb_regular))
                    )
                }
            }
        }
    }
}

fun Modifier.horizontalFadingEdge(
    scrollState: ScrollState,
    length: Dp,
    edgeColor: Color? = null,
) = composed(
    debugInspectorInfo {
        name = "length"
        value = length
    }
) {
    val color = edgeColor ?: MaterialTheme.colors.surface

    drawWithContent {
        val lengthValue = length.toPx()
        val scrollFromStart = scrollState.value
        val scrollFromEnd = scrollState.maxValue - scrollState.value

        val startFadingEdgeStrength = lengthValue * (scrollFromStart / lengthValue).coerceAtMost(1f)

        val endFadingEdgeStrength = lengthValue * (scrollFromEnd / lengthValue).coerceAtMost(1f)

        drawContent()

        drawRect(
            brush = Brush.horizontalGradient(
                colors = listOf(
                    color,
                    Color.Transparent,
                ),
                startX = 0f,
                endX = startFadingEdgeStrength,
            ),
            size = Size(
                startFadingEdgeStrength,
                this.size.height,
            ),
        )

        drawRect(
            brush = Brush.horizontalGradient(
                colors = listOf(
                    Color.Transparent,
                    color,
                ),
                startX = size.width - endFadingEdgeStrength,
                endX = size.width,
            ),
            topLeft = Offset(x = size.width - endFadingEdgeStrength, y = 0f),
        )
    }
}

fun Modifier.verticalFadingEdge(
    scrollState: ScrollState,
    length: Dp,
    edgeColor: Color? = null,
    topEdge: Boolean = true,
    bottomEdge: Boolean = true,
    topOffset: Dp = 0.dp,
    bottomOffset: Dp = 0.dp,
) = composed(
    debugInspectorInfo {
        name = "length"
        value = length
    }
) {
    val color = edgeColor ?: MaterialTheme.colors.surface

    drawWithContent {
        val lengthValue = length.toPx()
        val scrollFromTop = scrollState.value
        val scrollFromBottom = scrollState.maxValue - scrollState.value

        val topOffsetLength = topOffset.toPx()
        val bottomOffsetLength = bottomOffset.toPx()

        val topFadingEdgeStrength = lengthValue * (scrollFromTop / lengthValue).coerceAtMost(1f)

        val bottomFadingEdgeStrength = lengthValue * (scrollFromBottom / lengthValue).coerceAtMost(1f)

        drawContent()

        if (topEdge) {
            drawRect(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        color,
                        Color.Transparent,
                    ),
                    startY = topOffsetLength,
                    endY = topFadingEdgeStrength + topOffsetLength,
                ),
                size = Size(
                    this.size.width,
                    topFadingEdgeStrength
                ),
            )
        }

        if (bottomEdge) {
            drawRect(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        Color.Transparent,
                        color,
                    ),
                    startY = size.height - bottomFadingEdgeStrength - bottomOffsetLength,
                    endY = size.height - bottomOffsetLength,
                ),
                topLeft = Offset(x = 0f, y = size.height - bottomFadingEdgeStrength - bottomOffsetLength),
            )
        }
    }
}

enum class Keyboard {
    Opened, Closed
}

@Composable
fun keyboardAsState(): State<Keyboard> {
    val keyboardState = remember { mutableStateOf(Keyboard.Closed) }
    val view = LocalView.current
    DisposableEffect(view) {
        val onGlobalListener = ViewTreeObserver.OnGlobalLayoutListener {
            val rect = Rect()
            view.getWindowVisibleDisplayFrame(rect)
            val screenHeight = view.rootView.height
            val keypadHeight = screenHeight - rect.bottom
            keyboardState.value = if (keypadHeight > screenHeight * 0.15) {
                Keyboard.Opened
            } else {
                Keyboard.Closed
            }
        }
        view.viewTreeObserver.addOnGlobalLayoutListener(onGlobalListener)

        onDispose {
            view.viewTreeObserver.removeOnGlobalLayoutListener(onGlobalListener)
        }
    }

    return keyboardState
}
