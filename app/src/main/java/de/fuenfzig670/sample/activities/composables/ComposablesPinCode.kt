package de.fuenfzig670.sample.activities.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import de.fuenfzig670.dasfitnesswunder.R
import kotlinx.coroutines.android.awaitFrame
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@Preview(showSystemUi = true, showBackground = true)
@Composable


fun ScreenPincodePreview() {
    ScreenPincode { }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ScreenPincode(
    pinEntry: MutableState<TextFieldValue> = mutableStateOf(TextFieldValue("")),
    onEntryFinished: (String) -> Unit
) {
    val focusRequester = remember { FocusRequester() }
    val (text, setText) = remember {
        pinEntry
    }
    val charLimit = 6
    val lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current
    val keyboard = LocalSoftwareKeyboardController.current
    val scope = rememberCoroutineScope()

    val keyboardState by keyboardAsState()

    if (keyboardState == Keyboard.Closed) {
        keyboard?.show()
    }

    DisposableEffect(key1 = lifecycleOwner, effect = {
        val observer = LifecycleEventObserver { _, event ->
            scope.launch {
                if (event == Lifecycle.Event.ON_RESUME) {
                    delay(100)
                    focusRequester.requestFocus()
                    awaitFrame()
                    keyboard?.show()
                }
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    })

    LaunchedEffect(key1 = text) {
        if (text.text.length == charLimit) {
            onEntryFinished(text.text)
        }
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Spacer(modifier = Modifier.height(70.dp))
        Headline1(text = "Zugangscode")
        Spacer(modifier = Modifier.height(16.dp))
        ParagraphRegular(text = "Bitte gib deinen 6-stelligen\nZugangscode ein:")
        Spacer(modifier = Modifier.height(64.dp))
        Box(modifier = Modifier.fillMaxWidth()) {
            BasicTextField(
                value = text,
                onValueChange = { newValue ->
                    if (newValue.text.length <= 6) {
                        setText(
                            if (newValue.selection.length > 0) {
                                newValue.copy(selection = text.selection)
                            } else {
                                newValue
                            }
                        )
                    }
                },
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.NumberPassword,
                    imeAction = ImeAction.Done,
                ),
                textStyle = TextStyle(
                    color = Color.Transparent,
                ),
                cursorBrush = SolidColor(Color.Transparent),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .focusRequester(focusRequester)
            )
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
            ) {
                for (i in 0 until charLimit) {
                    if (i < text.text.length) {
                        PinEntryText(text = text.text.substring(i, i + 1))
                    } else {
                        Line(Color.Black)
                    }
                }
            }
        }
    }
}

@Composable
fun Line(color: Color) {
    PinEntryBox {
        Box(
            modifier = Modifier
                .requiredSize(32.dp, 3.dp)
                .background(color = color)
        )
    }
}

@Composable
fun PinEntryText(text: String, modifier: Modifier = Modifier) {
    PinEntryBox {
        Text(
            text = text,
            textAlign = TextAlign.Center,
            color = colorResource(id = R.color.black),
            fontFamily = FontFamily(Font(R.font.titilliumweb_bold)),
            fontSize = 34.sp,
            modifier = modifier.fillMaxWidth()
        )
    }
}

@Composable
fun PinEntryBox(content: @Composable () -> Unit) {
    Box(modifier = Modifier.requiredSize(32.dp, 41.dp)) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            content()
        }
    }
}
