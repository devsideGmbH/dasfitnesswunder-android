package de.fuenfzig670.sample.activities.ui.info;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;

import java.io.Serializable;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.activities.BaseFragment;
import de.fuenfzig670.sample.activities.MainActivity;
import de.fuenfzig670.sample.dataservice.GeneralService;
import de.fuenfzig670.sample.helpers.InfoPageType;
import de.fuenfzig670.sample.helpers.NoConnectionHelper;
import de.fuenfzig670.sample.helpers.SessionManager;
import de.fuenfzig670.sample.model.ArticleSummary;
import de.fuenfzig670.sample.model.Company;

public class InfoFragment extends BaseFragment {
    public static String STACK_ID = "InfoFragmentStackId";

    public InfoPageType type = InfoPageType.FAQ;
    private WebView infoPageWebView;
    private String textTitle;
    private int companyId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_info, container, false);
        setUpFragment();
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        requireActivity().getSupportFragmentManager().addOnBackStackChangedListener(backStackListener);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            requireActivity().getSupportFragmentManager().removeOnBackStackChangedListener(backStackListener);
        }
        catch (Exception e) {
            Log.v("Fitness", e.getLocalizedMessage());
        }
    }

    FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            int index = requireActivity().getSupportFragmentManager().getBackStackEntryCount() - 1;
            if (index >= 0) {
                FragmentManager.BackStackEntry entry = requireActivity().getSupportFragmentManager().getBackStackEntryAt(index);
                if (entry.getName() == InfoFragment.STACK_ID) {
                    updateActionBarTitle();
                }
            }

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity()instanceof MainActivity) ((MainActivity) getActivity()).setProfileGrayColor();

        CoordinatorLayout parentCoordinatorLayout = requireActivity().findViewById(R.id.parentCoordinatorLayout);
        parentCoordinatorLayout.setFitsSystemWindows(true);
    }

    void updateActionBarTitle() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        String title = titleFromType(type);
        actionBar.setTitle(title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = getContext();
        if (context == null) return;

        Intent intent = getIntent();
        if (intent == null) return;
        Serializable serializable = intent.getSerializableExtra("type");

        if (serializable instanceof InfoPageType) {
            type = (InfoPageType) serializable;
            textTitle = titleFromType(type);
        }
    }

    private String titleFromType(InfoPageType type) {
        String textTitle = "";
        switch (type) {
            case IMPRESSUM:
                textTitle = this.getString(R.string.impressum);
                break;
            case DATENSCHUTZ:
                textTitle = this.getString(R.string.Datenschutz);
                break;
            case TEILNAHME:
                textTitle = this.getString(R.string.Teilnahmebedingungen);
                break;
            case FAQ:
                textTitle = this.getString(R.string.FAQ);
                break;
        }
        return textTitle;
    }

    private void setUpFragment() {
        Context context = getContext();
        if (context == null) return;
        this.infoPageWebView = (WebView) findViewById(R.id.infoPageWebView);
        if (infoPageWebView != null) {
            infoPageWebView.getSettings().setJavaScriptEnabled(true);
            infoPageWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    Uri url = request.getUrl();
                    String host = url.getHost();

                    if (host != null && host.contains(getString(R.string.BASEURL))) {
                        return super.shouldOverrideUrlLoading(view, request);
                    }
                    // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
                    Intent intent = new Intent(Intent.ACTION_VIEW, (url));
                    startActivity(intent);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    view.loadUrl("javascript:(function(){ document.body.style.paddingBottom = '125px'})();");
                }
            });
        }
        Intent intent = getIntent();
        if (intent == null) return;
        companyId = intent.getIntExtra("companyId", 1);

        Company company = SessionManager.getInstance().company;
        if (company != null) {
            companyId = company.Id;
        }

        if (textTitle != null && !textTitle.isEmpty()) {
            new GeneralService(context).GetSpecificArticleId(companyId, textTitle, response -> {
                try {
                    if (response instanceof ArticleSummary) {
                        String article_url = ((ArticleSummary) response).article_url;
                        if (article_url == null || article_url.trim().isEmpty()) {
                            int articleId = ((ArticleSummary) response).id;
                            if (articleId != -1) {
                                new GeneralService(context).GetSpecificArticle(companyId, articleId, response1 -> {
                                    Context context1 = getContext();
                                    if (context1 == null) return;
                                    if (response1 != null && !response1.isEmpty()) {
                                        String baseUrl = getString(R.string.BASEURL);
                                        String res = response1 //.replace("", "")
                                            .replace("<img", "<img style=\"width:100%; height:auto;\" ")
                                            .replace("href=\"https://flow.polar.com\" target=\"_blank\" rel=\"noopener\"", "href =\"https://flow.polar.com\"");

                                        String html = getHtmlString(res);

                                        infoPageWebView.loadDataWithBaseURL(baseUrl, html, "text/html", "utf-8", null);
                                    }
                                });
                            } else {
                                NoConnectionHelper.showNoConnectionToast(context);
                            }
                        } else {
                            infoPageWebView.loadUrl(getString(R.string.BASEURL) + ((ArticleSummary) response).article_url);
                            infoPageWebView.post(() -> {
                               findViewById(R.id.spacer1).setVisibility(View.GONE);
                               findViewById(R.id.spacer2).setVisibility(View.GONE);
                            });
                        }
                    } else {
                        NoConnectionHelper.showNoConnectionToast(context);
                    }
                } catch (Exception e) {
                    Log.d(TAG, "onResponse: ", e);
                }
            });
        }
    }

    /**
     * returns html string with head, body, style and font setting
     *
     * @param input body inner html
     * @return returns html string with head, body, style and font setting
     */
    private String getHtmlString(String input) {
        String res = "";
        String htmlStyle = "<style>"
                + "@font-face {\n" +
                "    font-family: \"Titillium Web\";\n" +
                "    font-weight: normal;\n" +
                "    src: url(\"file:///android_asset/fonts/titilliumweb_regular.ttf\")\n" +
                "}\n" +
                "\n" +
                "@font-face {\n" +
                "    font-family: \"Titillium Web\";\n" +
                "    font-style: italic;\n" +
                "    src: url(\"file:///android_asset/fonts/titilliumweb_italic.ttf\")\n" +
                "}\n" +
                "\n" +
                "@font-face {\n" +
                "    font-family: \"Titillium Web\";\n" +
                "    font-weight: bold;\n" +
                "    src: url(\"file:///android_asset/fonts/titilliumweb_bold.ttf\")\n" +
                "}\n" +
                "body {\n" +
                "    margin: 0;\n" +
                "    font-family: \"Titillium Web\" !important;\n" +
                "    font-weight: normal;\n" +
                "}"
                + "</style>";
        String htmlStart = "<head>" + htmlStyle + "</head><body>";
        String htmlEnd = "<body>";

        res = htmlStart + input + htmlEnd;

        return res;
    }
}
