package de.fuenfzig670.sample.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.dataservice.CompanyDataService
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.NoConnectionHelper
import de.fuenfzig670.sample.helpers.SessionManager
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.LoginRequestResponse
import de.fuenfzig670.sample.model.UserModel

class SplashScreenActivity : AppCompatActivity() {
    private var _companyDataService: CompanyDataService? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        _companyDataService = CompanyDataService(this)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        if (GeneralHelper.wasOnboardingShown(this) == false) {
            goToOnboarding()
            return
        }
        if (GeneralHelper.getStayLoggedIn(this) == false) {
            GeneralHelper.goToPinCode(this)
            return
        }
        val userModel = GeneralHelper.GetUser(this)
        if (userModel == null) {
            goToPincode()
        } else {
            SessionManager.getInstance().token = userModel.token
            val generalService = GeneralService(this)
            generalService.LoginUser(userModel) { response: Any? ->
                //error
                if (response == null) {
                    NoConnectionHelper.showNoConnectionToast(this)
                } else if (response is LoginRequestResponse) {
                    GeneralHelper.handleLoginError(
                        this,
                        response as LoginRequestResponse?
                    ) { goToPincode() }
                } else if (response is String) {
                    val token = response.toString()
                    SessionManager.getInstance().token = token
                    userModel.token = token
                    GeneralHelper.SaveUser(this, userModel)
                    val companyDataService = CompanyDataService(this)
                    companyDataService.GetSpecificUser(userModel.id, userModel.companyId) { x: Any? ->
                        if (x is UserModel) {
                            userModel.id = x.id
                            userModel.api_source = x.api_source
                            x.token = token
                            x.companyId = userModel.companyId
                            x.password = userModel.password
                            x.api_source = userModel.api_source
                            GeneralHelper.SaveUser(this, x)
                        }
                    }
                    companyDataService.GetSpecific(userModel.companyId) { x: Any? ->
                        try {
                            val company = x as Company
                            SessionManager.getInstance().company = company
                            GeneralHelper.GoToChallenge(this, company)
                            finish()
                        } catch (e: Exception) {
                            Log.d(TAG, "onCreate: ", e)
                            showReloggingError()
                            GeneralHelper.DeleteUser(this)
                        }
                    }
                }
            }
        }
    }

    private fun showReloggingError() {
        GeneralHelper.showDialog(
            this,
            "Fehler",
            "Sie konnten nicht automatisch eingeloggt werden. Bitte loggen Sie sich manuel ein."
        ) { goToPincode() }
    }

    private fun onChallengeCodeResponse(response: Any, pin: String) {
        if (response is LoginRequestResponse) {
            GeneralHelper.SavePin(this, "")
            GeneralHelper.DeleteUser(this)
            showReloggingError()
        } else {
            val companyId = response.toString().toInt()
            _companyDataService?.GetSpecific(companyId) { response: Any ->
                onCompanyResponse(response)
            }
        }
    }

    private fun onCompanyResponse(response: Any) {
        if (response is Company) {
            SessionManager.getInstance().company = response
            GeneralHelper.GoToChallenge(this, response)
            finish()
        }
    }

    private fun goToPincode() {
        startActivity(Intent(this@SplashScreenActivity, PinCodeActivity::class.java))
        finish()
    }

    private fun goToOnboarding() {
        startActivity(Intent(this@SplashScreenActivity, OnboardingActivity::class.java))
        finish()
    }

    companion object {
        private val TAG = SplashScreenActivity::class.java.name
    }
}
