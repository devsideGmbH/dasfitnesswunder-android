package de.fuenfzig670.sample.activities

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener
import com.thryve.connector.module_gfit.GFitConnector
import com.thryve.connector.sdk.CoreConnector
import com.thryve.connector.shealth.SHealthConnector
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.fragments.AccountConfirmDeletionFragment
import de.fuenfzig670.sample.activities.fragments.AccountConfirmLogoutFragment
import de.fuenfzig670.sample.activities.fragments.AccountFragment
import de.fuenfzig670.sample.activities.fragments.ChallengeDetailsFragment
import de.fuenfzig670.sample.activities.fragments.ContactFragment
import de.fuenfzig670.sample.activities.fragments.ContactSuccessFragment
import de.fuenfzig670.sample.activities.fragments.MapWebViewFragment
import de.fuenfzig670.sample.activities.fragments.StartFragment
import de.fuenfzig670.sample.activities.ui.info.InfoFragment
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.InfoPageType
import de.fuenfzig670.sample.model.RankingCategory
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class MainActivity : BaseActivity(), CoroutineScope {

    private val handler = CoroutineExceptionHandler { _, exception ->
        println("CoroutineExceptionHandler got $exception")
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job + handler

    lateinit var job: Job

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }

    private lateinit var connector: CoreConnector
    private var gFitConnector: GFitConnector? = null
    private lateinit var activeFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        changeStatusBarContrastStyle(window, false)
        super.onCreate(savedInstanceState)
        job = SupervisorJob()

        setContentView(R.layout.activity_main)
        createCustomActionbar()

        setupStartFragment()
        setupBottomPanelUX()
        setupFab()
        setupMenu()

        /* Some weird but known issue on API  30.  Without this, icons on the
        bottom navigation bar will be squeezed */
        preventSystemNavigationBarOverlappingBottomBar()
    }

    override fun onResume() {
        super.onResume()
        launch(Dispatchers.IO) {
            val getUser = GeneralHelper.GetUser(this@MainActivity.applicationContext)
            connector = CoreConnector(this@MainActivity.applicationContext, getString(R.string.appId), getString(R.string.appSecret), "${getUser.id}", "de")
            val token = connector.getAccessToken().data
            Log.d(TAG, "onResume: $token")

            when (getUser.api_source) {
                RegistryStepsActivity.GFIT_NATIVE_THRYVE_ID -> initGFit()
                RegistryStepsActivity.SHEALTH_THRYVE_ID -> initSHealth()
            }
        }
    }


    private fun initGFit() {
        Log.d(javaClass.name, "initGFit: start")
        gFitConnector = GFitConnector(this, GeneralHelper.G_FIT_DATA_TYPES)
        gFitConnector?.start(GeneralHelper.G_FIT_DATA_TYPES) {
            Log.d(TAG, "initGFit: start data: ${it.data} success: ${it.successful}")
            if (it.data == false || it.successful == false) {
                gFitConnector?.stop {
                    Log.d(TAG, "initGFit: stop data: ${it.data} success: ${it.successful}")
                }
            } else {
                gFitConnector?.synchroniseEpoch(GeneralHelper.G_FIT_DATA_TYPES) {
                    Log.d(TAG, "initGFit: synchroniseEpoch data: ${it.data} success: ${it.successful}")
                }
                gFitConnector?.synchroniseDaily(GeneralHelper.G_FIT_DATA_TYPES) {
                    Log.d(TAG, "initGFit: synchroniseDaily data: ${it.data} success: ${it.successful}")
                }
            }
        }
    }

    private fun initSHealth() {
        val sConnector = SHealthConnector(context = this, GeneralHelper.S_HEALTH_DATA_TYPES)

        sConnector.authorize(this, GeneralHelper.S_HEALTH_DATA_TYPES, { granted ->
            Log.d(TAG, "initSHealth: authorize $granted")
            if (granted) {
                sConnector.start(GeneralHelper.S_HEALTH_DATA_TYPES) {
                    Log.d(TAG, "initSHealth: start data: ${it.data} success: ${it.successful}")
                    sConnector.synchroniseEpoch(GeneralHelper.S_HEALTH_DATA_TYPES) {
                        Log.d(TAG, "initSHealth: synchroniseEpoch data: ${it.data} success: ${it.successful}")
                    }
                    sConnector.synchroniseDaily(GeneralHelper.S_HEALTH_DATA_TYPES) {
                        Log.d(TAG, "initSHealth: synchroniseEpoch data: ${it.data} success: ${it.successful}")
                    }
                }
            } else {
                Log.d(TAG, "initSHealth: not authorized, stop")
                sConnector.stop {
                    Log.d(TAG, "initSHealth: stop data: ${it.data} success: ${it.successful}")
                }
            }
        }) {
            Log.d(TAG, "initSHealth: authorize exception", it)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        gFitConnector?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        gFitConnector?.onActivityResult(requestCode, resultCode, data)
    }

    private fun createCustomActionbar() {
        val toolbar = findViewById<com.google.android.material.appbar.MaterialToolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        supportActionBar?.hide()
    }

    private fun setupBottomPanelUX() {
        val panel = findViewById<SlidingUpPanelLayout>(R.id.sliding_layout)
        panel.panelHeight = 0
        val menuScrollView = findViewById<ScrollView>(R.id.scrollView)

        panel.setScrollableView(menuScrollView)
        val mainPanelContent = findViewById<TextView>(R.id.mainPanelContent)
        mainPanelContent.setOnClickListener {
            panel.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }

        panel.addPanelSlideListener(object : PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {

            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    panel?.visibility = View.VISIBLE

                    val profileCloseFab = setProfileGreenColor()
                    profileCloseFab?.setImageResource(R.drawable.ic_bottom_menu_fab_close)
                    profileCloseFab?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.light_green))
                    profileCloseFab?.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.white))

                    /* Unselect the start item on the bottom navigation bar*/
                    unselectStartIcon()
                    selectMenuIcon()

                    profileCloseFab?.setOnClickListener { (panel as? SlidingUpPanelLayout)?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }
                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    panel?.visibility = View.INVISIBLE

                    val profileCloseFab = setProfileGrayColor()
                    profileCloseFab?.setImageResource(R.drawable.ic_bottom_menu_fab_person_gray)
                    profileCloseFab?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.white))

                    if (checkIfCurrentFragmentIsProfile()) {
                        profileCloseFab?.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.light_green))
                    } else {
                        profileCloseFab?.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.menu_normal))
                    }

                    profileCloseFab?.setOnClickListener { showProfile() }
                    selectMenuIcon(false)

                    /* Select the start item on the bottom navigation bar*/
                    if (activeFragment is StartFragment) {
                        selectStartIcon()
                    }
                }
            }
        })

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.background = null

        //bottomNavigationView.setBackgroundColor(getColor(android.R.color.white))
        bottomNavigationView.menu.getItem(1).isEnabled = false
        bottomNavigationView.menu.getItem(2).isEnabled = false

        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_main_home -> {
                    panel.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED

                    if (panel.panelState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        popToMainAndUpdateIcon()
                    } else {
                        Handler().postDelayed({
                            popToMainAndUpdateIcon()
                        }, 600)
                    }

                    true
                }
                R.id.navigation_main_menu -> {
                    if (panel.panelState == SlidingUpPanelLayout.PanelState.COLLAPSED || panel.panelState == SlidingUpPanelLayout.PanelState.ANCHORED || panel.panelState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                        panel.visibility = View.VISIBLE

                        Handler().postDelayed({
                            panel.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
                        }, 100)
                    } else {
                        panel.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
                    }
                    false
                }
                else -> {
                    false
                }
            }

        }
    }

    private fun popToMainAndUpdateIcon() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val parentCoordinatorLayout = findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
            parentCoordinatorLayout.fitsSystemWindows = false

            supportActionBar?.hide()
            activeFragment = supportFragmentManager.fragments.last()
        }

        setProfileGrayColor()?.setImageResource(R.drawable.ic_bottom_menu_fab_person_gray)
    }

    private fun setupFab() {

        val profileCloseFab = setProfileGrayColor()
        profileCloseFab?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.white))
        profileCloseFab?.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.menu_normal))
        profileCloseFab?.setOnClickListener { showProfile() }
    }

    private fun showProfile() {
        /* Prevent adding fragment if its the already added and is frontmost */
        if (activeFragment is de.fuenfzig670.sample.activities.ui.profile.ProfileFragment) {
            return
        }

        /* Pass the data to fragment via intent,  should be refactored*/
        intent.putExtra("company", _company)
        intent.putExtra("hasUserLogin", true)
        /* -------------------------------------------  */

        unselectStartIcon()
        val fragment = de.fuenfzig670.sample.activities.ui.profile.ProfileFragment()
        fragment._company = _company

        setProfileGreenColor()?.setImageResource(R.drawable.ic_bottom_menu_fab_person_gray)

        activeFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(de.fuenfzig670.sample.activities.ui.profile.ProfileFragment.STACK_ID)
        transaction.commit()
    }

    private fun selectMenuIcon(checked: Boolean = true) {
        /* Unselect the start item on the bottom navigation bar*/
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.checkMenuItem(checked = checked)
    }
    private fun unselectStartIcon() {
        /* Unselect the start item on the bottom navigation bar*/
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.uncheckAllItems()
    }

    fun selectStartIcon() {
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView.menu.getItem(0).isChecked = true
    }

    private fun setupMenu() {
        val accountCardView = findViewById<CardView>(R.id.accountCardView)
        val datProtectionCardView = findViewById<CardView>(R.id.datProtectionCardView)
        val faqCardView = findViewById<CardView>(R.id.faqCardView)
        val impressumCardView = findViewById<CardView>(R.id.impressumCardView)
        val agreementCardView = findViewById<CardView>(R.id.agreementCardView)
        val contactCardView = findViewById<CardView>(R.id.contactCardView)

        accountCardView.setOnClickListener {
            showAccountFragment()
        }
        datProtectionCardView.setOnClickListener {
            showInfoFragment(InfoPageType.DATENSCHUTZ)
        }
        faqCardView.setOnClickListener {
            showInfoFragment(InfoPageType.FAQ)
        }
        impressumCardView.setOnClickListener {
            showInfoFragment(InfoPageType.IMPRESSUM)
        }
        agreementCardView.setOnClickListener {
            showInfoFragment(InfoPageType.TEILNAHME)
        }
        contactCardView.setOnClickListener {
            showContactFragment()
        }
    }

    private fun showInfoFragment(type: InfoPageType) {
        collapsePanel()

        if (checkIfSameInfoFragment(type)) {
            return
        }

        /* let the menu collapse then open fragment to avoid menu animation lagging*/
        Handler().postDelayed({
            intent.putExtra("companyId", _company.Id)
            intent.putExtra("type", type)
            val fragment = InfoFragment()
            activeFragment = fragment
            val transaction = supportFragmentManager.beginTransaction()
            transaction.addToBackStack(InfoFragment.STACK_ID)
            transaction.setReorderingAllowed(true)
            transaction.replace(R.id.fragmentsContainer, fragment)
            transaction.commit()

            unselectStartIcon()
        }, 500)

    }

    private fun collapsePanel() {
        val panel = findViewById<SlidingUpPanelLayout>(R.id.sliding_layout)
        panel?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    fun showAccountFragment() {
        collapsePanel()

        /* Prevent adding fragment if its the already added and is frontmost */
        if (activeFragment is AccountFragment) {
            return
        }

        /* let the menu collapse then open fragment to avoid menu animation lagging */
        Handler().postDelayed({
            val fragment = AccountFragment.newInstance(_company)
            activeFragment = fragment
            val transaction = supportFragmentManager.beginTransaction()
            transaction.addToBackStack(null)
            transaction.setReorderingAllowed(true)
            transaction.replace(R.id.fragmentsContainer, fragment)
            transaction.commit()
            unselectStartIcon()
        }, 500)
    }

    fun showMapFragment() {

        /* Prevent adding fragment if its the already added and is frontmost */
        if (activeFragment is MapWebViewFragment) {
            return
        }
        if (activeFragment is StartFragment == false) {
            goBack()
        }

        val fragment = MapWebViewFragment.newInstance(_company)
        activeFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.replace(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()

        unselectStartIcon()
    }

    fun showChallengesFragment(categories: ArrayList<RankingCategory>, selectedRankingCategory: RankingCategory) {

        val fragment = ChallengeDetailsFragment.newInstance(_company, selectedRankingCategory, categories)
        activeFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        //transaction.setReorderingAllowed(true)
        transaction.replace(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(ChallengeDetailsFragment.STACK_ID)
        transaction.commit()

        unselectStartIcon()
    }

    private fun setupStartFragment() {
        val fragment = StartFragment.newInstance()

        //val fragment = ChallengeFragment()
        //fragment._company = _company

        activeFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.replace(R.id.fragmentsContainer, fragment)
        //transaction.addToBackStack(StartFragment.STACK_ID)
        transaction.commit()
    }

    private fun preventSystemNavigationBarOverlappingBottomBar() {
        if (Build.VERSION.SDK_INT >= 30) {
            // Root ViewGroup of my activity
            val root = findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
            ViewCompat.setOnApplyWindowInsetsListener(root) { view, windowInsets ->

                val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())

                // Apply the insets as a margin to the view. Here the system is setting
                // only the bottom, left, and right dimensions, but apply whichever insets are
                // appropriate to your layout. You can also update the view padding
                // if that's more appropriate.

                view.layoutParams = (view.layoutParams as FrameLayout.LayoutParams).apply {
                    leftMargin = insets.left
                    bottomMargin = insets.bottom
                    rightMargin = insets.right
                }

                // Return CONSUMED if you don't want want the window insets to keep being
                // passed down to descendant views.
                WindowInsetsCompat.CONSUMED
            }

        }
    }

    fun showContactSuccessFragment() {
        /* Prevent adding fragment if its the already added and is frontmost */
        if (activeFragment is ContactSuccessFragment) {
            return
        }

        val fragment = ContactSuccessFragment.newInstance(_company)
        activeFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.replace(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()

        unselectStartIcon()

        val parentCoordinatorLayout = findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
        parentCoordinatorLayout.fitsSystemWindows = true
    }

    private fun showContactFragment() {
        collapsePanel()

        /* Prevent adding fragment if its the already added and is frontmost */
        if (activeFragment is ContactFragment) {
            return
        }

        /* let the panel collapse, the open fragment to avoid menu animation lagging */
        Handler().postDelayed({
            val fragment = ContactFragment.newInstance(_company)
            activeFragment = fragment

            val transaction = supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            transaction.replace(R.id.fragmentsContainer, fragment)
            transaction.addToBackStack(ContactFragment.STACK_ID)
            transaction.commit()

            unselectStartIcon()
        }, 500)

    }

    fun showStartFragment() {
        findViewById<BottomNavigationView>(R.id.bottomNavigationView)?.selectedItemId = R.id.navigation_main_home
    }

    fun showAccountDeleteConfirmationFragment() {
        val fragment = AccountConfirmDeletionFragment.newInstance(_company)
        val transaction = supportFragmentManager.beginTransaction()

        transaction.setReorderingAllowed(true)
        transaction.replace(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun showAccountLogoutonfirmationFragment() {
        val fragment = AccountConfirmLogoutFragment.newInstance(_company)
        val transaction = supportFragmentManager.beginTransaction()

        transaction.setReorderingAllowed(true)
        transaction.replace(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        goBack()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            goBack()
        }
        return true
    }

    private fun checkIfSameInfoFragment(type: InfoPageType): Boolean {
        supportFragmentManager.fragments.lastOrNull()?.let { fragment ->
            if (fragment is InfoFragment) {
                if ((fragment as InfoFragment).type == type) {
                    return true
                }
            }
        }
        return false
    }

    private fun checkIfCurrentFragmentIsProfile(): Boolean {
        supportFragmentManager.fragments.lastOrNull()?.let { fragment ->
            if (fragment is de.fuenfzig670.sample.activities.ui.profile.ProfileFragment) {
                return true
            }
        }
        return false
    }

    private fun goBack() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            return
        }
        supportFragmentManager.popBackStackImmediate()

        supportFragmentManager.fragments.lastOrNull()?.let { fragment ->
            activeFragment = fragment
        }

        /* If we got to the very first fragment which is StartFragment, we hide the action bar and select the start icon*/
        if (supportFragmentManager.backStackEntryCount == 0) {
            val parentCoordinatorLayout = findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
            parentCoordinatorLayout.fitsSystemWindows = false

            setProfileGrayColor()

            supportActionBar?.hide()

            selectStartIcon()
        }
    }

    fun setProfileGrayColor(): FloatingActionButton? {
        val profileCloseFab = findViewById<FloatingActionButton>(R.id.profileCloseFab)
        profileCloseFab?.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.menu_normal))
        return profileCloseFab
    }

    fun setProfileGreenColor(): FloatingActionButton? {
        val profileCloseFab = findViewById<FloatingActionButton>(R.id.profileCloseFab)
        profileCloseFab.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(applicationContext, R.color.light_green))
        return profileCloseFab
    }
}

fun BottomNavigationView.checkMenuItem(checked: Boolean = true) {
    menu.setGroupCheckable(0, true, false)
    menu.getItem(menu.size() - 1).isChecked = checked
    menu.setGroupCheckable(0, true, true)
}


fun BottomNavigationView.uncheckAllItems() {
    menu.setGroupCheckable(0, true, false)
    for (i in 0 until menu.size()) {
        menu.getItem(i).isChecked = false
    }
    menu.setGroupCheckable(0, true, true)
}
