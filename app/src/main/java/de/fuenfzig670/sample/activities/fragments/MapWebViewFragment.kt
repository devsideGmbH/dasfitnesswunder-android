package de.fuenfzig670.sample.activities.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentManager
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.BaseFragment
import de.fuenfzig670.sample.model.Company
import kotlin.math.roundToInt


class MapWebViewFragment : BaseFragment() {
    companion object {
        val STACK_ID = "MapWebViewFragmentStackId"

        @JvmStatic
        fun newInstance(company: Company) = MapWebViewFragment().apply { _company = company }
    }

    override fun onResume() {
        super.onResume()

        updateActionBarTitle()

        val parentCoordinatorLayout = requireActivity().findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
        parentCoordinatorLayout.fitsSystemWindows = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().supportFragmentManager.addOnBackStackChangedListener(backStackListener)
    }

    val backStackListener = object: FragmentManager.OnBackStackChangedListener {
        override fun onBackStackChanged() {
            val index = requireActivity().supportFragmentManager.backStackEntryCount - 1
            if (index >= 0) {
                val entry = requireActivity().supportFragmentManager.getBackStackEntryAt(index)
                if (entry.name == STACK_ID) {
                    updateActionBarTitle()
                }
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        requireActivity().supportFragmentManager.removeOnBackStackChangedListener(backStackListener)
    }

    private fun updateActionBarTitle() {
        val actionBar = (requireActivity() as? AppCompatActivity)?.supportActionBar
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.title = requireActivity().getString(R.string.location_battle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                mapWebViewComposable(_company)
            }
        }
    }

    @Composable
    @SuppressLint("SetJavaScriptEnabled")
    private fun mapWebViewComposable(company: Company?) {
        Column(Modifier.fillMaxSize()) {
            company?.mobile_map_url?.let { url ->
                val finalUrl = resources.getString(R.string.BASEURL) + url
                AndroidView(factory = {
                    WebView(it).apply {
                        layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,
                        )

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            verticalScrollbarThumbDrawable =  ResourcesCompat.getDrawable(resources, R.drawable.scrollview_thumb, null)
                        }
                        webViewClient = WebViewClient()
                        settings.javaScriptEnabled = true
                        settings.domStorageEnabled = true
                        addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                            val measuredHeight = activity?.findViewById<View>(R.id.profileCloseFab)?.height ?: 0
                            this.loadUrl(finalUrl + "?height=" + (pxToDp(this.height) - pxToDp(measuredHeight)))
                        }
                        this.webViewClient = object : WebViewClient() {
                            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                                if (request?.url?.host?.contains("dasfitnesswunder") == false) {
                                    activity?.startActivity(Intent(Intent.ACTION_VIEW, request.url))
                                    return true
                                }
                                return super.shouldOverrideUrlLoading(view, request)
                            }
                        }
                    }
                }, modifier = Modifier.weight(1f))
            }
            Spacer(modifier = Modifier.height(97.dp))
        }
    }

    private fun getNavigationBarHeight(): Int {
        val resources: Resources = requireContext().resources

        val resName = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            "navigation_bar_height"
        } else {
            "navigation_bar_height_landscape"
        }

        val id: Int = resources.getIdentifier(resName, "dimen", "android")

        return if (id > 0) {
            resources.getDimensionPixelSize(id)
        } else {
            0
        }
    }

    private fun pxToDp(px: Int): Int {
        val displayMetrics = requireContext().resources.displayMetrics
        return (px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }
}
