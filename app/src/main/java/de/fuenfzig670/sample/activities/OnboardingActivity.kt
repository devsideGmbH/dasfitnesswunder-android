package de.fuenfzig670.sample.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import com.google.accompanist.pager.ExperimentalPagerApi
import de.fuenfzig670.sample.activities.composables.OnboardingUI
import de.fuenfzig670.sample.helpers.GeneralHelper


class OnboardingActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.R)
    @OptIn(ExperimentalAnimationApi::class, ExperimentalPagerApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        changeStatusBarContrastStyle(window, false)

        super.onCreate(savedInstanceState)
        setContent {
            OnboardingUI {
                GeneralHelper.setOnboardingShown(this@OnboardingActivity, true)
                startActivity(Intent(this@OnboardingActivity, PinCodeActivity::class.java))
            }
        }
    }

    private fun changeStatusBarContrastStyle(window: Window, lightIcons: Boolean) {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
        }

        val decorView: View = window.getDecorView()
        if (lightIcons) {
            // Draw light icons on a dark background color
            decorView.systemUiVisibility = decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        } else {
            // Draw dark icons on a light background color
            decorView.systemUiVisibility = decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }
}
