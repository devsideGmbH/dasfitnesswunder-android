package de.fuenfzig670.sample.activities

import android.os.Bundle
import androidx.activity.addCallback
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.composables.RegistryFrame
import de.fuenfzig670.sample.activities.fragments.adapters.margin
import de.fuenfzig670.sample.helpers.GeneralHelper

class RegistrySuccessActivity : BaseRegistryActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.onBackPressedDispatcher.addCallback { GeneralHelper.goToLogin(this@RegistrySuccessActivity) }
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)

        val titleTextView = findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tvTitle)
        titleTextView.margin(right = 0F) // To align title right in the center as 48dp was taken by home button

        setContent {
            RegistrySuccessScreen()
        }
    }

    @Composable
    fun RegistrySuccessScreen(showLoadingIndicator: MutableState<Boolean> = mutableStateOf(false)) {
        RegistryFrame(
            progress = 1f,
            step = "",
            headerTitle = "Bitte nun Registrierung bestätigen",
            headerSubTitle = "",
            contentTitle = "",
            showLoadingIndicator = showLoadingIndicator,
            circleImage = R.drawable.registry_mail,
            buttonLabel = "Zum Login",
            buttonClick = { GeneralHelper.goToLogin(this) }) {
            Spacer(modifier = Modifier.height(32.dp))

            val message =
                buildAnnotatedString {
                    append("Um die Registrierung erfolgreich abzuschließen, öffne bitte deine E-Mails und klicke auf den ")
                    withStyle(style = SpanStyle(fontFamily = FontFamily(Font(R.font.titilliumweb_bold)))) {
                        append("Bestätigungslink")
                    }
                    append(".")
                    append("\n\n")
                    append("Direkt im Anschluss wirst du zur Challenge freigeschaltet!")
                }

            MessageView(
                title = "Wichtiger Hinweis",
                message = "",
                annotatedMessage = message,
                backgroundColor = colorResource(id = R.color.medium_grey)
            )
        }
    }

    @Preview(showBackground = true, showSystemUi = true)
    @Composable
    fun ContentPreview() {
        RegistrySuccessScreen()
    }
}
