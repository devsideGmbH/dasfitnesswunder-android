package de.fuenfzig670.sample.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import de.fuenfzig670.sample.activities.composables.ScreenPincode
import de.fuenfzig670.sample.dataservice.CompanyDataService
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.helpers.SessionManager
import de.fuenfzig670.sample.model.ChallengeCode
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.LoginRequestResponse

class PinCodeActivity : BaseActivity() {
    private var generalService: GeneralService = GeneralService(this)
    private var loadingSpinner: View? = null
    private var absenden: Button? = null

    var pinEntry: MutableState<TextFieldValue> = mutableStateOf(TextFieldValue(""))

    override fun onResume() {
        super.onResume()
        pinEntry.value = TextFieldValue("")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ScreenPincode(pinEntry) { pin ->
                startLoading()
                generalService.ChallengeCodeValidation(ChallengeCode(pin)) { response ->
                    onChallengeCodeResponse(response, pin)
                }
            }
        }

    }

    @Preview(showSystemUi = true, showBackground = true)
    @Composable
    fun ScreenPincodePreview() {
        ScreenPincode { }
    }

    private fun startLoading() {
        if (loadingSpinner != null) loadingSpinner?.visibility = View.VISIBLE
        if (absenden != null) absenden?.isEnabled = false
    }

    private fun stopLoading() {
        if (loadingSpinner != null) loadingSpinner?.visibility = View.GONE
        if (absenden != null) absenden?.isEnabled = true
    }

    private fun onChallengeCodeResponse(response: Any, pin: String) {
        stopLoading()
        if (response is LoginRequestResponse) {
            handleLoginError(response.error)
        } else {
            val companyId = response.toString().toInt()
            CompanyDataService(this).GetSpecific(companyId) {
                if (it is Company) {
                    SessionManager.getInstance().company = it
                    goToResponse(companyId, pin)
                } else {
                    GeneralHelper.showDialog(this, "Fehler", "Keine Challenge zum eingegebenen Code gefunden.")
                }
            }
        }
    }

    private fun goToResponse(companyId: Int, pin: String) {
        GeneralHelper.SavePin(this, pin)
        startActivity(
            Intent(this, PinCodeResponseActivity::class.java)
                .putExtra(PinCodeResponseActivity.PINCODE_SUCCESS, true)
                .putExtra(PinCodeResponseActivity.PINCODE_TITLE, "Vielen Dank")
                .putExtra(PinCodeResponseActivity.PINCODE_MESSAGE, "Dein Zugangscode ist korrekt!")
                .putExtra(PinCodeResponseActivity.PINCODE_COMPANY_ID, companyId)
                .putExtra(PinCodeResponseActivity.PINCODE_PIN, pin)
        )
    }

    private fun handleLoginError(response: String) {
        startActivity(
            Intent(this, PinCodeResponseActivity::class.java)
                .putExtra(PinCodeResponseActivity.PINCODE_SUCCESS, false)
                .putExtra(PinCodeResponseActivity.PINCODE_TITLE, "Oops...")
                .putExtra(PinCodeResponseActivity.PINCODE_MESSAGE, "Der eingegebene Code ist nicht korrekt.")
        )
        stopLoading()
    }
}
