package de.fuenfzig670.sample.activities

import android.os.Bundle
import androidx.appcompat.app.ActionBar
import de.fuenfzig670.dasfitnesswunder.R

open class BaseRegistryActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.layout_actionbar)
        findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tvTitle).text = "Registrierung"
        super.onCreate(savedInstanceState)
    }
}
