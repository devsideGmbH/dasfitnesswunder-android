package de.fuenfzig670.sample.activities.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.FragmentManager
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.BaseFragment
import de.fuenfzig670.sample.activities.MainActivity
import de.fuenfzig670.sample.activities.composables.*
import de.fuenfzig670.sample.dataservice.GeneralService
import de.fuenfzig670.sample.helpers.NoConnectionHelper
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.ContactRequest
import de.fuenfzig670.sample.model.ContactRequestResponse

class ContactFragment : BaseFragment() {
    private var _generalService: GeneralService? = null

    companion object {
        val STACK_ID = "ContactFragmentStackId"

        @JvmStatic
        fun newInstance(company: Company) = ContactFragment().apply { _company = company }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _generalService = GeneralService(this.context)
        return ComposeView(requireContext()).apply {
            setContent {
                setBackgroundColor(requireContext().getColor(de.fuenfzig670.dasfitnesswunder.R.color.white))
                val contactInputs = ContactInputs()
                ContactFragmentComposable(contactInputs)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().supportFragmentManager.addOnBackStackChangedListener(backStackListener)
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity)?.setProfileGrayColor()

        updateActionBarTitle()

        val parentCoordinatorLayout = requireActivity().findViewById<CoordinatorLayout>(R.id.parentCoordinatorLayout)
        parentCoordinatorLayout.fitsSystemWindows = true
    }

    val backStackListener = object: FragmentManager.OnBackStackChangedListener {
        override fun onBackStackChanged() {
            val index = requireActivity().supportFragmentManager.backStackEntryCount - 1
            if (index >= 0) {
                val entry = requireActivity().supportFragmentManager.getBackStackEntryAt(index)
                if (entry.name == STACK_ID) {
                    updateActionBarTitle()
                }
            }
        }
    }

    override fun onDetach() {
        super.onDetach()

        try {
            requireActivity().supportFragmentManager.removeOnBackStackChangedListener(backStackListener)
        }
        catch (e: Exception) {
            backStackListener
        }
    }

    @Preview(showBackground = true, showSystemUi = true)
    @Composable
    fun ContactFragmentComposablePreview() {
        ContactFragmentComposable(ContactInputs())
    }

    data class ContactInputs(
        val name: InputWrapper = InputWrapper("Name*", "Name eingeben"),
        val email: InputWrapper = InputWrapper("E-Mail Adresse*", "E-Mail Adresse"),
        val message: InputWrapper = InputWrapper("Nachricht*", "Nachricht eingeben"),
    )

    @Composable
    fun ContactFragmentComposable(contactInputs: ContactInputs) {
        Column(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, bottom = 54.dp)
                    .verticalScroll(rememberScrollState())
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                Headline24(text = "Kontaktiere uns", textAlign = TextAlign.Start, modifier = Modifier.fillMaxWidth())
                Spacer(modifier = Modifier.height(8.dp))
                ParagraphRegular(text = "Du hast Fragen, Anmerkungen oder Schwierigkeiten?", textAlign = TextAlign.Start, modifier = Modifier.fillMaxWidth())
                Spacer(modifier = Modifier.height(24.dp))
                CustomInput(inputWrapper = contactInputs.name)
                CustomInput(inputWrapper = contactInputs.email)
                CustomInput(inputWrapper = contactInputs.message, maxLines = Int.MAX_VALUE)
                ParagraphRegular(text = "*Pflichtfelder", textAlign = TextAlign.Start, modifier = Modifier.fillMaxWidth())
                Spacer(modifier = Modifier.height(24.dp))
                GreenButton(text = "Absenden") {
                    clearErrorMarks(contactInputs)
                    onClickSend(contactInputs)
                }
                Spacer(modifier = Modifier.height(240.dp))
            }

        }
    }


    private fun onClickSend(contactInputs: ContactInputs) {
        val contactRequest = ContactRequest(contactInputs.name.inputValue.value, contactInputs.email.inputValue.value, contactInputs.message.inputValue.value)
        _generalService!!.sendContactRequest(
            _company,
            contactRequest
        ) { reponseObject: Any? -> handleInfoRequestResponse(reponseObject, contactInputs) }
        clearErrorMarks(contactInputs)
    }

    private fun handleInfoRequestResponse(responseObject: Any?, contactInputs: ContactInputs) {
        val context = context ?: return
        if (responseObject == null) {
            NoConnectionHelper.showNoConnectionToast(context)
            return
        }
        val response = responseObject as ContactRequestResponse
        if (response.Success) {
            (activity as? MainActivity)?.showContactSuccessFragment()
        } else {
            contactInputs.name.inputError.value = response.name?.firstOrNull() ?: ""
            contactInputs.email.inputError.value = response.email?.firstOrNull() ?: ""
            contactInputs.message.inputError.value = response.message?.firstOrNull() ?: ""
        }
    }

    private fun clearErrorMarks(contactInputs: ContactInputs) {
        contactInputs.name.inputError.value = ""
        contactInputs.email.inputError.value = ""
        contactInputs.message.inputError.value = ""
    }

    private
    fun updateActionBarTitle() {
        val actionBar = (requireActivity() as? AppCompatActivity)?.supportActionBar
        actionBar?.show()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.title = requireActivity().getString(R.string.contact)
    }
}
