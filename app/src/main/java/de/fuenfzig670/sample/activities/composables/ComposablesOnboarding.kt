package de.fuenfzig670.sample.activities.composables

import androidx.annotation.DrawableRes
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.StyledPlayerView
import de.fuenfzig670.dasfitnesswunder.R
import kotlinx.coroutines.launch

@Composable
fun VideoView(videoUri: String, modifier: Modifier = Modifier) {
    val context = LocalContext.current

    val exoPlayer = ExoPlayer.Builder(LocalContext.current)
        .build()
        .also { exoPlayer ->
            val mediaItem = MediaItem.Builder()
                .setUri(videoUri)
                .build()
            exoPlayer.setMediaItem(mediaItem)
            exoPlayer.prepare()
        }
    exoPlayer.playWhenReady = true
    exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
    exoPlayer.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING

    DisposableEffect(
        AndroidView(factory = {
            StyledPlayerView(context).apply {
                resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
                useController = false
                player = exoPlayer
            }
        }, modifier = modifier)
    ) {
        onDispose { exoPlayer.release() }
    }
}


@OptIn(ExperimentalAnimationApi::class, ExperimentalPagerApi::class)
@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewMe() {
    OnboardingUI { /*TODO*/ }
}

data class Page(val title: String, val description: String, @DrawableRes val image: Int?)

val onboardPages = listOf(
    Page(
        "Easy Momo Transfer",
        "Make a quick transaction with someone besides you by scanning their Easy Momo Code",
        R.drawable.onboarding_logo
    ),
    Page(
        "Herzlich Willkommen",
        "Schöpfe dein Potenzial aus, entwickle dich weiter und gewinne Motivation durch Fortschritt",
        null
    ),
    Page(
        "Halte deinen\nFortschritt fest",
        "Im Challenge Überblick verfolgst du deinen aktuellen Fortschritt und den deiner Kollegen",
        null
    ),
    Page(
        "Datenübertragung",
        "Deine Schritte werden nach erfolgreicher Registrierung automatisch übertragen. Es kann ein paar Stunden dauern, bis deine Schritte eingelaufen sind.",
        null
    )
)

@Composable
fun PageUI(page: Page) {
    Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.verticalScroll(rememberScrollState())) {
        if (page.image != null) {
            Spacer(
                modifier = Modifier
                    .height(31.dp)
                    .fillMaxWidth()
            )
            Image(
                painter = painterResource(page.image),
                contentDescription = null,
                modifier = Modifier.size(168.dp, 188.dp)
            )
        } else {
            Spacer(
                modifier = Modifier
                    .height(48.dp)
                    .fillMaxWidth()
            )
            Headline1(
                text = page.title, Modifier.padding(24.dp, 0.dp)
            )
            Spacer(modifier = Modifier.height(24.dp))
            ParagraphRegular(
                text = page.description, Modifier.padding(24.dp, 0.dp)
            )
            Spacer(modifier = Modifier.height(24.dp))
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun OnboardingUI(
    onGettingStartedClick: () -> Unit
) {
    val pagerState = rememberPagerState(0)
    val scope = rememberCoroutineScope()

    val path = "android.resource://" + LocalContext.current.packageName + "/" + R.raw.onboarding_video
    Column(Modifier.background(Color.White)) {
        Box(modifier = Modifier.weight(1f).heightIn(73.dp, 390.dp)) {
            VideoView(videoUri = path, Modifier.heightIn(73.dp, 390.dp))
            Image(
                painter = painterResource(id = R.drawable.onboarding_white),
                contentDescription = null,
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .height(157.dp)
                    .offset(0.dp, (84).dp)
                    .fillMaxWidth()
            )
        }

        HorizontalPager(
            state = pagerState,
            count = 4,
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
        ) { page ->
            PageUI(page = onboardPages[page])
        }

        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(16.dp),
            activeColor = colorResource(R.color.black),
            inactiveColor = colorResource(id = R.color.black_transparent_32)
        )
        Spacer(modifier = Modifier.height(40.dp))

        val lastPage = pagerState.currentPage == onboardPages.size - 1
        GreenButton(
            if (lastPage)
                stringResource(R.string.start)
            else stringResource(R.string.next),
            Modifier.padding(horizontal = 20.dp)
        ) {
            if (lastPage) {
                onGettingStartedClick()
            } else {
                scope.launch {
                    pagerState.animateScrollToPage(pagerState.currentPage + 1)
                }
            }
        }
        Spacer(modifier = Modifier.height(16.dp))
    }
}
