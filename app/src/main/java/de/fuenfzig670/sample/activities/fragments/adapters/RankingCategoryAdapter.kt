package de.fuenfzig670.sample.activities.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.activities.fragments.adapters.callbacks.RankingCategoryClickCallback
import de.fuenfzig670.sample.model.RankingCategory

class RankingCategoryAdapter(var rankningCategories: ArrayList<RankingCategory>, val context: Context,  val callback: RankingCategoryClickCallback) : RecyclerView.Adapter<RankingViewHolder>() {

    private var images = mapOf<String, Int>("Gesamt Challenge" to R.drawable.ic_challenge_gesamt, "GEO Challenge" to R.drawable.ic_challenge_map, "Abteilungs-Battle" to R.drawable.ic_challenge_abteilung, "Weltumrundung" to R.drawable.ic_challenge_welt, "Moon Battle" to R.drawable.ic_challenge_moon, "Battle den Chef" to R.drawable.ic_challenge_chef, "Schuhbattle" to R.drawable.ic_challenge_other)

    override fun getItemCount(): Int {
        return rankningCategories.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankingViewHolder {
        return RankingViewHolder(LayoutInflater.from(context).inflate(R.layout.item_ranking_category, parent, false))
    }

    override fun onBindViewHolder(holder: RankingViewHolder, position: Int) {
        val category = rankningCategories[position]

        images[category.dropdowntitle]?.let { holder.imageView.setImageResource(it) }
        holder.textView.text = category.dropdowntitle

        holder.cardView.setOnClickListener {
            callback.onRankingCategoryClicked(category, category.ranking_type == 1)
        }
    }

}

class RankingViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val cardView = view.findViewById<CardView>(R.id.cardView)
    val imageView = view.findViewById<ImageView>(R.id.imageView)
    val textView = view.findViewById<TextView>(R.id.textView)
    val arrowView = view.findViewById<ImageView>(R.id.arrowImageView)
    //val deletePictureButton = view.deletePictureButton

}
