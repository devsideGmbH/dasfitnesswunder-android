package de.fuenfzig670.sample.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

import java.util.List;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.model.UserRanking;

public class RankingsAdapter extends ArrayAdapter<UserRanking> {
    private Context _context;
    private int _resourceId;

    public RankingsAdapter(@NonNull Context context, int resource, @NonNull List<UserRanking> objects) {
        super(context, resource, objects);
        _context = context;
        _resourceId = resource;

    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = LayoutInflater.from(_context);
            v = vi.inflate(_resourceId, null);
        }

        UserRanking userRanking = getItem(position);
        Log.d("getView", "index: " + position + " nickname: " + userRanking.Nickname);
        if (userRanking != null) {
            TextView ranking = v.findViewById(R.id.text_ranking);
            ImageView gender = v.findViewById(R.id.text_gender);
            TextView nickname = v.findViewById(R.id.text_nickname);
            TextView steps = v.findViewById(R.id.text_steps);
            TextView trash = v.findViewById(R.id.text_trash);


            if (ranking != null) ranking.setText(String.valueOf(userRanking.Ranking));
            if (_context != null)
                if (gender != null)
                    gender.setImageDrawable(AppCompatResources.getDrawable(_context, userRanking.GenderDrawableId()));
            if (nickname != null) nickname.setText(userRanking.Nickname);
            if (steps != null) steps.setText(userRanking.Steps);
            if (trash != null) {
                if(trash.getText()=="0"){
                    trash.setText("");
                }else{
                    trash.setText(userRanking.eco+" l");
                }
            }
        }
        return v;
    }
}
