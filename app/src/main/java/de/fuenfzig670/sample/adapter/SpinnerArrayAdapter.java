package de.fuenfzig670.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import de.fuenfzig670.sample.model.DropDownValue;
import de.fuenfzig670.sample.model.ThryveSource;

import java.util.List;

public class SpinnerArrayAdapter extends ArrayAdapter<Object> {
    LayoutInflater _inflater;
    int _resource;
    List<Object> _objects;
    Type _type;

    public SpinnerArrayAdapter(@NonNull Context context, Type type, int resource, @NonNull List<Object> objects) {
        super(context, resource, objects);
        _inflater = LayoutInflater.from(context);
        _resource = resource;
        _objects = objects;
        _type = type;
    }

    public enum Type {
        Normal,
        AbteilungsChallenge,
        Tracker
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView textView = (TextView) convertView;

        if (textView == null) {
            textView = (TextView) _inflater.inflate(_resource, parent, false);
        }
        switch (_type) {
            case Normal:
                textView.setText((String) _objects.get(position));
                break;
            case AbteilungsChallenge:
                textView.setText((String) ((DropDownValue) _objects.get(position)).dropdowvaluentitle);
                break;
            case Tracker:
                textView.setText((String) ((ThryveSource) _objects.get(position)).display_name);
                break;
        }
        return textView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView textView = (TextView) convertView;

        if (textView == null) {
            textView = (TextView) _inflater.inflate(_resource, parent, false);
        }
        switch (_type) {
            case Normal:
                textView.setText((String) _objects.get(position));
                break;
            case AbteilungsChallenge:
                textView.setText((String) ((DropDownValue) _objects.get(position)).dropdowvaluentitle);
                break;
            case Tracker:
                textView.setText((String) ((ThryveSource) _objects.get(position)).display_name);
                break;
        }
        return textView;
    }
}
