package de.fuenfzig670.sample.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import de.fuenfzig670.sample.helpers.GeneralHelper;
import de.fuenfzig670.sample.helpers.SessionManager;
import de.fuenfzig670.sample.model.Company;

import de.fuenfzig670.dasfitnesswunder.R;

import java.util.ArrayList;
import java.util.List;

public class CompanyRecyclerViewAdapter extends RecyclerView.Adapter<CompanyRecyclerViewAdapter.ViewHolder> {

    private final List<Company> _companies;
    private final List<Company> _companiesBackup = new ArrayList<>();
    private final LayoutInflater _inflater;
    //    private ItemClickListener _clickListener;
    private final Context _context;
    private final int _numberOfColumns;

    RequestOptions _options = new RequestOptions()
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.mipmap.ic_launcher_round);

    ColorMatrixColorFilter _filter;

    public CompanyRecyclerViewAdapter(Context context, List<Company> companies, int numberOfColumns) {
        _context = context;
        _companies = companies;
        this._inflater = LayoutInflater.from(context);
        _companiesBackup.addAll(companies);
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);
        _filter = new ColorMatrixColorFilter(colorMatrix);
        _numberOfColumns = numberOfColumns;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = _inflater.inflate(R.layout.layout_recyclerview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Company company = getItem(position);
        Glide.with(_context)
                .load(_context.getResources().getString(R.string.BASEURL) + company.Logo)
                .apply(_options).into(holder.imageView);
        if (GeneralHelper.IsChallengeExpired(company)) holder.imageView.setColorFilter(_filter);
        else holder.imageView.setColorFilter(null);

        int remainder = position % _numberOfColumns;
        if (remainder == 0) {
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_START);
        } else if (remainder == (_numberOfColumns - 1)) {
            holder.imageView.setScaleType(ImageView.ScaleType.FIT_END);
        }

    }

    @Override
    public int getItemCount() {
        return _companies.size();
    }

    public void filter(String text) {
        _companies.clear();
        if (text.isEmpty()) {
            _companies.addAll(_companiesBackup);
        } else {
            text = text.toLowerCase();
            for (Company company : _companiesBackup) {
                if (company.CompanyName.toLowerCase().contains(text)) {
                    _companies.add(company);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.company_logo);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Company company = getItem(getAdapterPosition());
            SessionManager.getInstance().token = null;
            GeneralHelper.GoToChallenge(_context, company);
        }
    }

    // convenience method for getting data at click position
    Company getItem(int id) {
        return _companies.get(id);
    }

/*    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this._clickListener = itemClickListener;
    }*/


/*    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }*/
}
