package de.fuenfzig670.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.fuenfzig670.sample.model.RankingCategory;
import de.fuenfzig670.dasfitnesswunder.R;

import java.util.List;

public class RankingsFooterRecyclerViewAdapter extends RecyclerView.Adapter<RankingsFooterRecyclerViewAdapter.ViewHolder> {
    private final List<RankingCategory> _categories;
    private final LayoutInflater _inflater;
    private ItemClickListener _clickListener;
    private final Context _context;
    private int _selectedRanking;

    public RankingsFooterRecyclerViewAdapter(Context context, List<RankingCategory> categories, int selectedRanking, ItemClickListener clickListener) {
        _context = context;
        _categories = categories;
        _clickListener = clickListener;
        _inflater = LayoutInflater.from(context);
        _selectedRanking = selectedRanking;
    }

    @Override
    public int getItemViewType(int position) {
        if (_selectedRanking == -1) return 0;
        if (_selectedRanking == position) return 0;
        return 1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = _inflater.inflate(R.layout.layout_rankings_footer, parent, false);
        } else {
            view = _inflater.inflate(R.layout.layout_rankings_footer_gray, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String category = getItem(position).dropdowntitle;
        holder.textView.setText(category);
    }

    private RankingCategory getItem(int position) {
        return _categories.get(position);
    }

    @Override
    public int getItemCount() {
        return _categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            textView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (_clickListener != null) _clickListener.onItemClick(/*view,*/ getAdapterPosition());
        }
    }

    /*    // allows clicks events to be caught
        void setClickListener(RankingsFooterRecyclerViewAdapter.ItemClickListener itemClickListener) {
            this._clickListener = itemClickListener;
        }*/
    public interface ItemClickListener {
        void onItemClick(/*View view,*/ int position);
    }
}
