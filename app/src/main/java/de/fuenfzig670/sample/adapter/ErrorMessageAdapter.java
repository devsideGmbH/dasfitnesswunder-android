package de.fuenfzig670.sample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.fuenfzig670.dasfitnesswunder.R;

import java.util.ArrayList;
import java.util.List;

public class ErrorMessageAdapter extends RecyclerView.Adapter<ErrorMessageAdapter.ViewHolder> {
    private final LayoutInflater _inflater;
    private final List<String> _content;

    public ErrorMessageAdapter(Context context, ArrayList<String> content) {
        _inflater = LayoutInflater.from(context);
        _content = content;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = _inflater.inflate(R.layout.layout_error_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String errorMessage = _content.get(position);
        holder.errorText.setText(errorMessage);
    }

    @Override
    public int getItemCount() {
        return _content.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView errorText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            errorText = itemView.findViewById(R.id.text_error);
        }
    }
}
