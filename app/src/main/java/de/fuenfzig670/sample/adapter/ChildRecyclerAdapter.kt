package de.fuenfzig670.sample.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.UserRanking


class ChildRecyclerAdapter(
    rankings: List<UserRanking>?,
    company: Company,
    context: Context

) : RecyclerView.Adapter<ChildRecyclerAdapter.ViewHolder>() {
    private val _rankings: MutableList<UserRanking> = ArrayList()
    private val _company: Company
    private val _context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.layout_child_ranking_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val userRanking = getItem(position)
        if (userRanking != null) {
            holder.ranking.text =
                _context.resources.getString(R.string.ranking_format, userRanking.Ranking)
            holder.nickname.text = userRanking.Nickname
            holder.steps.text = userRanking.Steps
            holder.trash.visibility = View.GONE
            if (!_company.eco_challenge) {
                holder.trash.visibility = View.GONE
            } else {
                holder.trash.visibility = View.VISIBLE
                if (userRanking.eco == null || userRanking.eco == "") {
                    holder.trash.text = ""
                } else {
                    holder.trash.text = userRanking.eco + " l"
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return _rankings.size
    }

    private fun getItem(id: Int): UserRanking {
        return _rankings[id]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ranking: TextView = itemView.findViewById(R.id.text_ranking)
        var nickname: TextView = itemView.findViewById(R.id.text_nickname)
        var steps: TextView = itemView.findViewById(R.id.text_steps)
        var trash: TextView = itemView.findViewById(R.id.text_trash)
    }

    init {
        _rankings.addAll(rankings!!)
        _context = context
        _company = company
    }
}
