package de.fuenfzig670.sample.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.RecycledViewPool
import de.fuenfzig670.dasfitnesswunder.R
import de.fuenfzig670.sample.helpers.GeneralHelper
import de.fuenfzig670.sample.model.Company
import de.fuenfzig670.sample.model.GroupRanking
import de.fuenfzig670.sample.model.UserRanking


class RankingsRecyclerViewAdapter(
    private val _context: Context,
    private val _rankings: MutableList<out UserRanking> = arrayListOf(),
    private val rankingActivity: Boolean,
    private val _company: Company,
    private val hasGroupHeader: Boolean
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val _inflater: LayoutInflater = LayoutInflater.from(_context)
    private val viewPool = RecycledViewPool()

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        if (rankingActivity) {
            val userModel = GeneralHelper.GetUser(_context)
            if (userModel != null && item.Id == userModel.id && !hasGroupHeader) {
                return 1
            }
        }
        if (item.isGroupHeader) {
            return 2
        }
        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0, 1 -> {
                val view = if (viewType == 0) {
                    _inflater.inflate(R.layout.layout_ranking_content, parent, false)
                } else {
                    _inflater.inflate(R.layout.layout_ranking_content_self, parent, false)
                }
                ViewHolder(view)
            }
            else -> {
                val view = _inflater.inflate(R.layout.layout_ranking_content_group, parent, false)
                GroupViewHolder(view)
            }
        }
    }

    @SuppressLint("ResourceAsColor", "ObjectAnimatorBinding")
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val userRanking = getItem(position)
        when (getItemViewType(position)) {
            0, 1 -> {
                handleDefaultRanking(viewHolder as ViewHolder, userRanking, position)
            }
            else -> {
                handleGroupEntryRanking(viewHolder as GroupViewHolder, userRanking)
            }
        }
    }

    private fun handleGroupEntryRanking(groupViewHolder: GroupViewHolder, userRanking: UserRanking) {
        groupViewHolder.groupHeaderText.text = userRanking.Nickname
    }

    private fun handleDefaultRanking(holder: ViewHolder, userRanking: UserRanking, position: Int) {
        val isgroupRanking = userRanking is GroupRanking
        val layoutManager = LinearLayoutManager(holder.childRecyclerView?.context, LinearLayoutManager.VERTICAL, false)
        holder.ranking.text = _context.resources.getString(R.string.ranking_format, userRanking.Ranking)
        val genderString = userRanking.GenderString()
        if (genderString == "") {
            holder.gender.visibility = View.GONE
        } else {
            holder.gender.setImageDrawable(
                AppCompatResources.getDrawable(_context, userRanking.GenderDrawableId())
            )
        }
        holder.nickname.text = if (isgroupRanking) (userRanking as GroupRanking).grouptitle else userRanking.Nickname
        if (!_company.eco_challenge) {
            holder.trashChild?.visibility = View.GONE
            holder.trash.visibility = View.GONE
        } else {
            holder.trash.visibility = View.VISIBLE
            holder.trashChild?.visibility = View.VISIBLE
            if (userRanking.eco == null || userRanking.eco == "") {
                holder.trash.text = ""
            } else {
                holder.trash.text = userRanking.eco + " l"
            }
        }
        holder.steps.text = userRanking.Steps
        holder.ranking.setCompoundDrawablesWithIntrinsicBounds(0, 0, userRanking.tendencyIcon(), 0)
        holder.expandedLayout?.visibility = View.GONE
        if (isgroupRanking) {
            holder.ranking.setCompoundDrawablesWithIntrinsicBounds(0, 0, (userRanking as GroupRanking).tendencyIcon(), 0)
            val isVisible: Boolean = userRanking.isVisibility
            if (userRanking.users.size == 0) {
                holder.no_users?.visibility = View.VISIBLE
            } else {
                holder.no_users?.visibility = View.GONE
            }
            val icon = if (isVisible) R.drawable.ic_baseline_expand_less_24 else R.drawable.ic_baseline_expand_more_24
            //if(!userRanking.isUsers) icon = 0
            holder.nickname.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0)
            holder.expandedLayout?.visibility = if (isVisible) View.VISIBLE else View.GONE

            layoutManager.initialPrefetchItemCount = userRanking.users.size
            val childItemAdapter = ChildRecyclerAdapter(userRanking.users, _company, _context)

            holder.childRecyclerView?.layoutManager = layoutManager
            holder.childRecyclerView?.adapter = childItemAdapter
            holder.childRecyclerView?.setRecycledViewPool(viewPool)
            holder.layoutRanking.setOnClickListener {
                userRanking.isVisibility = !userRanking.isVisibility
                notifyItemChanged(position)
            }
        }
    }

    override fun getItemCount(): Int {
        return _rankings.size
    }

    fun getItem(id: Int): UserRanking {
        return _rankings[id]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ranking: TextView = itemView.findViewById(R.id.text_ranking)
        var gender: ImageView = itemView.findViewById(R.id.text_gender)
        var nickname: TextView = itemView.findViewById(R.id.text_nickname)
        var steps: TextView = itemView.findViewById(R.id.text_steps)
        var trash: TextView = itemView.findViewById(R.id.text_trash)
        var trashChild: ImageView? = itemView.findViewById(R.id.text_trash_child)
        var expandedLayout: ConstraintLayout? = itemView.findViewById(R.id.expandedLayout)
        var layoutRanking: ConstraintLayout = itemView.findViewById(R.id.layout_ranking)
        var no_users: TextView? = itemView.findViewById(R.id.keine_teilnehmer)
        var childRecyclerView: RecyclerView? = itemView.findViewById<RecyclerView>(R.id.child_rankings_body).also { childRecyclerView = it }
    }

    class GroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var groupHeaderText: TextView = itemView.findViewById(R.id.text_group_name)
    }
}
