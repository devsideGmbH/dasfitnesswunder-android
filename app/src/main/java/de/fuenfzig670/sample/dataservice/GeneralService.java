package de.fuenfzig670.sample.dataservice;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import de.fuenfzig670.sample.activities.composables.InputWrapper;
import de.fuenfzig670.sample.activities.composables.RegistryInputs;
import de.fuenfzig670.sample.helpers.GeneralHelper;
import de.fuenfzig670.sample.model.ArticleSummary;
import de.fuenfzig670.sample.model.ChallengeCode;
import de.fuenfzig670.sample.model.Company;
import de.fuenfzig670.sample.model.ContactRequest;
import de.fuenfzig670.sample.model.ContactRequestResponse;
import de.fuenfzig670.sample.model.CreateUserRequest;
import de.fuenfzig670.sample.model.CreateUserResponse;
import de.fuenfzig670.sample.model.DropDown;
import de.fuenfzig670.sample.model.InfoRequest;
import de.fuenfzig670.sample.model.InfoRequestResponse;
import de.fuenfzig670.sample.model.LoginRequest;
import de.fuenfzig670.sample.model.LoginRequestResponse;
import de.fuenfzig670.sample.model.PasswordResetRequest;
import de.fuenfzig670.sample.model.ThryveToken;
import de.fuenfzig670.sample.model.UserModel;
import de.fuenfzig670.sample.services.IHtmlCallback;
import de.fuenfzig670.sample.services.IObjectCallback;
import de.fuenfzig670.sample.services.IStringCallback;
import de.fuenfzig670.sample.services.RestService;

public class GeneralService {
    public static final String DELETE_USER_TEXT = "Benutzer könnte nicht gelöscht werden. Wenden Sie sich an den Support.";
    private RestService restService;
    private Gson gson = new Gson();

    private Context context;

    public GeneralService(Context context) {
        restService = RestService.getInstance(context);
        this.context = context;
    }

    public void SetThryveToken(int companyID, int userID, ThryveToken thryveToken, IObjectCallback callback) {
        restService.putCall("/api/v1/tenants/" + companyID + "/users/" + userID + "/settoken", gson.toJson(thryveToken), new IObjectCallback() {
            @Override
            public void onResponse(Object response) {
                Log.i("SetThryveToken", "response: " + response);
                callback.onResponse(response);
            }
        });
    }

    public void validateFields(Company company, int step, RegistryInputs inputs, IObjectCallback callback) {
        inputs.getShowLoadingIndicator().setValue(true);

        inputs.clearErrors();
        HashMap<String, Object> map = new HashMap<>();
        HashMap<String, Object> userdropdowns_attributes = new HashMap<>();

        for (InputWrapper wrapper : inputs.getDropDowns()) {
            DropDown dropDown = wrapper.getDropDown();
            if (dropDown != null) {
                HashMap<String, Object> dropdownvalue_id = new HashMap<>();
                dropdownvalue_id.put("dropdownvalue_id", "" + dropDown.dropdownvalues.get(wrapper.getSelectedIndex().getValue()).id);
                userdropdowns_attributes.put("" + dropDown.id, dropdownvalue_id);
            }
        }
        if (step == 1) {
            map.put("lastname", inputs.getLastname().getInputValue().getValue().trim());
            map.put("firstname", inputs.getFirstname().getInputValue().getValue().trim());
            map.put("dayofbirth", inputs.getEmail().getInputValue().getValue().trim()); // TODO
            map.put("gender", inputs.getGender().getSelectedIndex().getValue());
        } else if (step == 2) {
            map.put("nickname", inputs.getNickname().getInputValue().getValue().trim());
            map.put("email", inputs.getEmail().getInputValue().getValue().trim());
            map.put("email_confirmation", inputs.getEmail_confirmation().getInputValue().getValue().trim());
        } else if (step == 3) {
            map.put("password", inputs.getPassword().getInputValue().getValue().trim());
            map.put("password_confirmation", inputs.getPassword_confirmation().getInputValue().getValue().trim());
        } else if (step == 4) {
            map.put("userdropdowns_attributes", userdropdowns_attributes);
            map.put("polar_reg_confirm", inputs.getTrackerCompliance().getInputValue().getValue());
            map.put("agb_confirm", inputs.getPrivacyPolicy().getInputValue().getValue());
            map.put("api_source_id", company.registration.api_source.get(inputs.getTracker().getSelectedIndex().getValue()));
        }

        restService.putCall("/api/v1/tenants/" + company.Id + "/users/validate_fields", gson.toJson(map), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                callback.onResponse(response);
                inputs.getShowLoadingIndicator().setValue(false);
            }

            @Override
            public void onError(String response) {
                try {
                    ValidationResponse validationResponse = gson.fromJson(response, ValidationResponse.class);
                    callback.onResponse(validationResponse);
                    inputs.getShowLoadingIndicator().setValue(false);
                } catch (Exception e) {
                    Log.d(getClass().getName(), "onError: ", e);

                    GeneralHelper.showDialog(context, "Fehler", "Die Verbindung mit dem Server konnte nicht hergestellt werden.");
                    ValidationResponse validationResponse = new ValidationResponse();
                    callback.onResponse(validationResponse);
                    inputs.getShowLoadingIndicator().setValue(false);
                }
            }
        });
    }

    public void CreateUser(int companyID, CreateUserRequest createUserRequest, IObjectCallback callback) {
        restService.postCall("/api/v1/tenants/" + companyID + "/users", gson.toJson(createUserRequest), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("CreateUser", "onSuccess: " + response);

                JsonParser parser = new JsonParser();
                JsonElement JElement = parser.parse(response);
                JsonArray jArray = JElement.getAsJsonArray();

                /*JsonObject a = jArray.get(0).getAsJsonObject();
                String b = a.get("id").getAsString();*/

                int id = jArray.get(0).getAsJsonObject().get("id").getAsInt();
                callback.onResponse(id);
            }

            @Override
            public void onError(String response) {
                Log.i("CreateUser", "onError: " + response);
                CreateUserResponse createUserResponse = null;
                try {
                    String responseKeyFixed = fixDotInKeys(response);
                    createUserResponse = gson.fromJson(responseKeyFixed, CreateUserResponse.class);
                } catch (Exception e) {
                    createUserResponse = new CreateUserResponse();
                    createUserResponse.base = new ArrayList<>();
                    createUserResponse.base.add("Unbekannter Fehler.");
                    Log.d("TAG", "onError: ", e);
                }
                callback.onResponse(createUserResponse);
            }
        });
    }

    private String fixDotInKeys(String response) throws JSONException {
        JSONObject obj = new JSONObject(response.trim());
        Iterator<?> keys = obj.keys();
        JSONObject res = new JSONObject();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            Object object = obj.get(key);
            if (object instanceof JSONObject || object instanceof JSONArray) {
                res.put(key.replaceAll("\\.", "_"), object);
            }
        }
        return res.toString();
    }

    public void sendContactRequest(Company company, ContactRequest
        contactRequest, IObjectCallback callback) {
        restService.postCall("/api/v1/tenants/" + company.Id + "/contacts", gson.toJson(contactRequest), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("sendContactRequest", "onSuccess: " + response);
                ContactRequestResponse ret = new ContactRequestResponse();
                ret.Success = true;
                ret.Messages.add("OK");
                callback.onResponse(ret);
            }

            @Override
            public void onError(String response) {
                Log.i("sendContactRequest", "onError: " + response);
                try {
                    ContactRequestResponse infoRequestResponse = gson.fromJson(response, ContactRequestResponse.class);
                    callback.onResponse(infoRequestResponse);
                } catch (Exception e) {
                    Log.e("sendContactRequest", "Exception when casting to ContactRequestResponse: " + e.getMessage());
                    ContactRequestResponse infoRequestResponse = new ContactRequestResponse();
                    infoRequestResponse.Messages.add("Unbekannter Fehler.");
                    callback.onResponse(infoRequestResponse);
                }
            }
        });
    }

    public void SendInfoRequest(InfoRequest infoRequest, IObjectCallback callback) {
        restService.postCall("/api/v1/inforequests", gson.toJson(infoRequest), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("SendInfoRequest", "onSuccess: " + response);
                InfoRequestResponse ret = new InfoRequestResponse();
                ret.Success = true;
                ret.Messages.add("OK");
                callback.onResponse(ret);
            }

            @Override
            public void onError(String response) {
                Log.i("SendInfoRequest", "onError: " + response);
                try {
                    InfoRequestResponse infoRequestResponse = gson.fromJson(response, InfoRequestResponse.class);
                    callback.onResponse(infoRequestResponse);
                } catch (Exception e) {
                    Log.e("SendInfoRequest", "Exception when casting to InfoRequestResponse: " + e.getMessage());
                    callback.onResponse(" Unbekannter Fehler");
                }
            }
        });
    }

    public void GetSpecificArticleId(int companyID, final String key, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyID + "/articles", new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("GetSpecificArticleIdGetSpecificArticleId", "onSuccess: " + response);

                JsonParser parser = new JsonParser();
                JsonElement JElement = parser.parse(response);
                JsonArray jArray = JElement.getAsJsonArray();

                ArrayList<ArticleSummary> listdata = new ArrayList<>();
                if (jArray != null) {
                    for (int i = 0; i < jArray.size(); i++) {
                        listdata.add(gson.fromJson(jArray.get(i), ArticleSummary.class));
                    }
                }
                ArticleSummary articleSummary = new ArticleSummary();
                articleSummary.id = -1;
                for (ArticleSummary summary : listdata) {
                    if (summary.headline.equals(key)) {
                        articleSummary = summary;
                    }
                }
                callback.onResponse(articleSummary);
            }

            @Override
            public void onError(String response) {
                Log.i("GetSpecificArticleId", "onError: " + response);
                callback.onResponse(-1);
            }
        });
    }

    public void GetSpecificArticle(int companyID, int articleId, IStringCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyID + "}/articles/" + articleId, new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("GetSpecificArticle", "onSuccess: " + response);

                JsonParser parser = new JsonParser();
                JsonElement JElement = parser.parse(response);
                JsonArray jArray = JElement.getAsJsonArray();

                String content = jArray.get(0).getAsJsonObject().get("content").getAsString();
                callback.onResponse(content);
            }

            @Override
            public void onError(String response) {
                Log.i("GetSpecificArticle", "onError: " + response);

                callback.onResponse(null);
            }
        });
    }

    public Request LoginUser(int companyId, LoginRequest loginRequest, IObjectCallback callback) {
        return restService.putCall("/api/v1/tenants/" + companyId + "/users/login", gson.toJson(loginRequest), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("LoginUser", "onResponse: " + response);

                JsonParser parser = new JsonParser();
                JsonElement JElement = parser.parse(response.toString());
                JsonArray jArray = JElement.getAsJsonArray();

                String content = jArray.get(0).getAsJsonObject().get("token").getAsString();
                callback.onResponse(content);
            }

            @Override
            public void onError(String response) {
                try {
                    LoginRequestResponse loginRequestResponse = gson.fromJson(response, LoginRequestResponse.class);
                    callback.onResponse(loginRequestResponse);
                } catch (Exception e) {
                    Log.d("LoginUser", "onError: ", e);
                    callback.onResponse(null);
                }
            }
        });
    }

    public void ChallengeCodeValidation(ChallengeCode challengeCode, IObjectCallback callback) {
        if (challengeCode.challengecode.equals("23146758")) {
            callback.onResponse(10011);
            return;
        }

        restService.putCall("/api/v1/challenge_code_validation", gson.toJson(challengeCode), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                //Log.i("LoginUser", "onResponse: " + response);

                try {
                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonObject jArray = JElement.getAsJsonObject();

                    int content = jArray.get("id").getAsInt();
                    callback.onResponse(content);
                } catch (Exception e) {
                    LoginRequestResponse loginRequestResponse = new LoginRequestResponse();
                    loginRequestResponse.error = "Die Challenge konnte nicht ausgelesen werden.";
                    callback.onResponse(loginRequestResponse);
                }
            }

            @Override
            public void onError(String response) {
                try {
                    LoginRequestResponse loginRequestResponse = gson.fromJson(response, LoginRequestResponse.class);
                    callback.onResponse(loginRequestResponse);
                } catch (Exception e) {
                    Log.d(getClass().getName(), "onError: ", e);
                    LoginRequestResponse loginRequestResponse = new LoginRequestResponse();
                    loginRequestResponse.error = "Unbekannter Fehler ist aufgetretten.";
                    callback.onResponse(loginRequestResponse);
                }
            }
        });
    }

    public void DeleteUser(int companyId, int userId, boolean force, IObjectCallback callback) {
        String forceStr = null;
        if (force)
            forceStr = gson.toJson(new DeleteUserRequest(true));
        restService.deleteCall("/api/v1/tenants/" + companyId + "/users/" + userId, forceStr, new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("LoginUser", "onResponse: " + response);
                callback.onResponse(Boolean.TRUE);
                GeneralHelper.setUserLoggedOut(context, false);
            }

            @Override
            public void onError(String response) {
                try {
                    LoginRequestResponse loginRequestResponse = gson.fromJson(response, LoginRequestResponse.class);
                    callback.onResponse(loginRequestResponse);
                } catch (Exception e) {
                    callback.onResponse(Boolean.FALSE);
                }
            }
        });
    }

    public void PasswordReset(int companyId, PasswordResetRequest
        passwordResetRequest, IObjectCallback callback) {
        restService.putCall("/api/v1/tenants/" + companyId + "/users/password_reset", gson.toJson(passwordResetRequest), new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("PasswordReset", "onResponse: " + response);
                callback.onResponse(true);
            }

            @Override
            public void onError(String response) {
                Log.i("PasswordResetError", "onResponse: " + response);
                callback.onResponse(false);
            }
        });
    }

    public Request LoginUser(UserModel userModel, IObjectCallback iObjectCallback) {
        return LoginUser(userModel.companyId, new LoginRequest(userModel.email, userModel.password), iObjectCallback);
    }

    class DeleteUserRequest {
        boolean force;

        DeleteUserRequest(boolean force) {
            this.force = force;
        }
    }
}
