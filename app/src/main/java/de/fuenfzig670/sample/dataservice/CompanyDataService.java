package de.fuenfzig670.sample.dataservice;

import android.content.Context;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import de.fuenfzig670.sample.model.Company;
import de.fuenfzig670.sample.model.CompanySmall;
import de.fuenfzig670.sample.model.GroupRanking;
import de.fuenfzig670.sample.model.RankingCategory;
import de.fuenfzig670.sample.model.UserModel;
import de.fuenfzig670.sample.model.UserModelBase;
import de.fuenfzig670.sample.model.UserRanking;
import de.fuenfzig670.sample.services.IHtmlCallback;
import de.fuenfzig670.sample.services.IObjectCallback;
import de.fuenfzig670.sample.services.RestService;

import java.util.ArrayList;

public class CompanyDataService {
    private final static String TAG = CompanyDataService.class.getName();

    private final RestService restService;
    /*    private Gson gson = new GsonBuilder().setFieldNamingStrategy(field -> {
            String fieldName = FieldNamingPolicy.UPPER_CAMEL_CASE.translateName(field);
            return fieldName.replaceAll("_", "");
        }).create();*/
    private final Gson gson = new GsonBuilder()
            .setFieldNamingStrategy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

    public CompanyDataService(Context context) {
        restService = RestService.getInstance(context);
    }

    public void GetAll(IObjectCallback callback) {
        restService.getCall("/api/v1/tenants", new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    Log.i("GetAll", "onSuccess: " + response);

                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonArray jArray = JElement.getAsJsonArray();

                    ArrayList<CompanySmall> listdata = new ArrayList<>();
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            listdata.add(gson.fromJson(jArray.get(i), CompanySmall.class));
                        }
                    }

                    callback.onResponse(listdata);
                } catch (Exception e) {
                    Log.i("GetAll", "Exception: " + e);
                    callback.onResponse(null);
                }
            }

            @Override
            public void onError(String response) {
                Log.i("GetAll", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetSpecific(int id, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + id, new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Log.i("GetSpecific", "onSuccess: " + response);

                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonArray jArray = JElement.getAsJsonArray();

                    ArrayList<Company> listdata = new ArrayList<>();
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            Company c = gson.fromJson(jArray.get(i), Company.class);
                            listdata.add(c);
                        }
                    }

                    Company company = listdata.get(0);
                    if (company.isRestricted() && !company.hasUserLogin()) {
                        company.ProfilUrl = null;
                    }

                    callback.onResponse(company);
                } catch (Exception e) {
                    Log.i("GetSpecific", "Exception: " + e);
                    callback.onResponse(null);
                }
            }

            @Override
            public void onError(String response) {
                Log.i("GetSpecific", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetAllRankings(int companyId, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyId + "/rankings", new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    Log.i("GetAllRankings", "onSuccess: " + response);

                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonArray jArray = JElement.getAsJsonArray();

                    ArrayList<RankingCategory> listdata = new ArrayList<>();
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            listdata.add(gson.fromJson(jArray.get(i), RankingCategory.class));
                        }
                    }

                    callback.onResponse(listdata);
                } catch (Exception e) {
                    Log.i("GetAllRankings", "Exception: " + e);
                    callback.onResponse(null);
                }
            }

            @Override
            public void onError(String response) {
                Log.i("GetAllRankings", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetSpecificRanking(int companyId, int rankingId, boolean showGender, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyId + "/rankings/" + rankingId, new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    Log.i("GetSpecificRanking", "onSuccess: " + response);

                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonArray jArray = JElement.getAsJsonArray();

                    ArrayList<UserRanking> listdata = new ArrayList<>();
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            UserRanking userRanking = gson.fromJson(jArray.get(i), UserRanking.class);
//                        userRanking.eco = "2." + i;
                            userRanking.ShowGender = showGender;
                            listdata.add(userRanking);
                        }
                    }

                    callback.onResponse(listdata);
                } catch (Exception e) {
                    Log.i("GetSpecificRanking", "Exception: " + e);
                    callback.onResponse(null);
                }

            }

            @Override
            public void onError(String response) {
                Log.i("GetSpecificRanking", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetDynamicRankings(int companyId, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyId + "/dropdowns", new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("GetDynamicRankings", "onSuccess: " + response);
                try {

                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonArray jArray = JElement.getAsJsonArray();

                    ArrayList<RankingCategory> listdata = new ArrayList<>();
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            listdata.add(gson.fromJson(jArray.get(i), RankingCategory.class));
                        }
                    }
                    callback.onResponse(listdata);
                } catch (Exception e) {
                    Log.i("GetDynamicRankings", "Exception: " + e);
                    callback.onResponse(null);
                }
            }

            @Override
            public void onError(String response) {
                Log.i("GetDynamicRankings", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetSpecificDynamicRanking(int companyId, int dropdownId, Boolean showGender, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyId + "/dropdowns/" + dropdownId + "/with_users", new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("GetSpecificDynamicRanki", "onSuccess: " + response);
                try {

                    JsonParser parser = new JsonParser();
                    JsonElement JElement = parser.parse(response);
                    JsonArray jArray = JElement.getAsJsonArray();

                    ArrayList<GroupRanking> listdata = new ArrayList<>();
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            GroupRanking groupRanking = gson.fromJson(jArray.get(i), GroupRanking.class);
                            //userRanking.ShowGender = showGender;
                            listdata.add(groupRanking);
                        }
                    }
                    Log.i("GroupRanking", "onSuccess: " + listdata);
                    callback.onResponse(listdata);
                } catch (Exception e) {
                    Log.i("GetSpecificDynamicRanking", "Exception: " + e);
                    callback.onResponse(null);
                }

            }

            @Override
            public void onError(String response) {
                Log.i("GetSpecificDynamicRanki", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetSpecificUser(int userId, int companyId, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyId  + "/users/" + userId, new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("GetUserId", "onSuccess: " + response);

                JsonParser parser = new JsonParser();
                JsonElement JElement = parser.parse(response);
                JsonArray jArray = JElement.getAsJsonArray();

                UserModel res = null;
                try {
                   res = gson.fromJson(jArray.get(0), UserModel.class);
                }
                catch (Exception e) {
                    Log.d(TAG, "GetUserId: ", e);
                }

                callback.onResponse(res);
            }

            @Override
            public void onError(String response) {
                Log.i("GetUserId", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }

    public void GetUserId(String email, int companyId, IObjectCallback callback) {
        restService.getCall("/api/v1/tenants/" + companyId + "/users", new IHtmlCallback() {
            @Override
            public void onSuccess(String response) {
                Log.i("GetUserId", "onSuccess: " + response);

                JsonParser parser = new JsonParser();
                JsonElement JElement = parser.parse(response);
                JsonArray jArray = JElement.getAsJsonArray();

                UserModelBase res = null;
                try {
                    if (jArray != null) {
                        for (int i = 0; i < jArray.size(); i++) {
                            UserModelBase userModelBase = gson.fromJson(jArray.get(i), UserModelBase.class);
                            if (email.toLowerCase().equals(userModelBase.email.toLowerCase())) {
                                res = userModelBase;
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.d(TAG, "GetUserId: ", e);
                }

                callback.onResponse(res);
            }

            @Override
            public void onError(String response) {
                Log.i("GetUserId", "onError: " + response);
                callback.onResponse(null);
            }
        });
    }
}
