package de.fuenfzig670.sample.dataservice

class ValidationResponse {
    var lastname: ArrayList<String>? = null
    var firstname: ArrayList<String>? = null
    var dayofbirth: ArrayList<String>? = null
    var gender: ArrayList<String>? = null
    var nickname: ArrayList<String>? = null
    var email: ArrayList<String>? = null
    var email_confirmation: ArrayList<String>? = null
    var password: ArrayList<String>? = null
    var password_confirmation: ArrayList<String>? = null
    var userdropdowns_attributes: ArrayList<String>? = null
    var polar_reg_confirm: ArrayList<String>? = null
    var agb_confirm: ArrayList<String>? = null
    var api_source_id: ArrayList<String>? = null
}
