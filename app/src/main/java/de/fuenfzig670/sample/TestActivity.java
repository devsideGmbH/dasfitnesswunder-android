package de.fuenfzig670.sample;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import de.fuenfzig670.dasfitnesswunder.R;
import de.fuenfzig670.sample.activities.BaseActivity;

public class TestActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
    }

    public void onImpressumClick(View view) {
        Toast.makeText(this, "Hello There!", Toast.LENGTH_SHORT).show();
    }

}
